 jQuery(document).ready(function ($) {
            var $window = $(window);

            function checkWidth() {
                var windowsize = $window.width();
                if (windowsize > 767) {
                    $('a[href*="account/login"]').click(function (e) {
                        e.preventDefault();
                        $("#so_sociallogin").modal('show');
                    });
                }
            }

            checkWidth();
            $(window).resize(checkWidth);
        });


 var total_item = 3;
        $(".sohomeslider-inner-1").owlCarousel2({
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            autoplay: false,
            autoplayTimeout: 5000,
            autoplaySpeed: 1000,
            smartSpeed: 500,
            autoplayHoverPause: true,
            startPosition: 0,
            mouseDrag: true,
            touchDrag: true,
            dots: true,
            autoWidth: false,
            dotClass: "owl2-dot",
            dotsClass: "owl2-dots",
            loop: true,
            navText: ["Next", "Prev"],
            navClass: ["owl2-prev", "owl2-next"],
            responsive: {
                0: {
                    items: 1,
                    nav: total_item <= 1 ? false : ((false ) ? true : false),
                },
                480: {
                    items: 1,
                    nav: total_item <= 1 ? false : ((false ) ? true : false),
                },
                768: {
                    items: 1,
                    nav: total_item <= 1 ? false : ((false ) ? true : false),
                },
                992: {
                    items: 1,
                    nav: total_item <= 1 ? false : ((false ) ? true : false),
                },
                1200: {
                    items: 1,
                    nav: total_item <= 1 ? false : ((false ) ? true : false),
                }
            },
        });


//<![CDATA[
        jQuery(document).ready(function ($) {
            ;(function (element) {
                var $element = $(element),
                    $extraslider = $(".blog-external", $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none';

                this_item = $extraslider.find('div.media');
                this_item.find('div.item:eq(0)').addClass('head-button');
                this_item.find('div.item:eq(0) .media-heading').addClass('head-item');
                this_item.find('div.item:eq(0) .media-left').addClass('so-block');
                this_item.find('div.item:eq(0) .media-content').addClass('so-block');
                $extraslider.on("initialized.owl.carousel2", function () {
                    var $item_active = $(".owl2-item.active", $element);
                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $(".owl2-item", $element);
                        $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                    }

                    $(".owl2-controls", $element).insertBefore($extraslider);
                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));
                });

                $extraslider.owlCarousel2({
                    margin: 30,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: false,
                    autoplayTimeout: 0,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    rtl: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 2},
                        768: {items: 3},
                        992: {items: 3},
                        1200: {items: 3},
                    },
                    dotClass: "owl2-dot",
                    dotsClass: "owl2-dots",
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ["&#171;", "&#187;"],
                    navClass: ["owl2-prev", "owl2-next"]
                });

                $extraslider.on("translate.owl.carousel2", function (e) {

                    //var $item_active = $(".owl2-item.active", $element);
                    //_UngetAnimate($item_active);
                    //_getAnimate($item_active);
                });

                $extraslider.on("translated.owl.carousel2", function (e) {

                    var $item_active = $(".owl2-item.active", $element);
                    var $item = $(".owl2-item", $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    } else {
                        $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                    }
                });

                function _getAnimate($el) {
                    if (_effect == "none") return;
                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                    $extraslider.removeClass("extra-animate");
                    $el.each(function (i) {
                        var $_el = $(this);
                        $(this).css({
                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                            "-o-animation": _effect + " " + _duration + "ms ease both",
                            "animation": _effect + " " + _duration + "ms ease both",
                            "-webkit-animation-delay": +i * _delay + "ms",
                            "-moz-animation-delay": +i * _delay + "ms",
                            "-o-animation-delay": +i * _delay + "ms",
                            "animation-delay": +i * _delay + "ms",
                            "opacity": 1
                        }).animate({
                            opacity: 1
                        });

                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            "animation": "",
                            "-webkit-animation": "",
                            "-moz-animation": "",
                            "-o-animation": "",
                            "opacity": 1
                        });
                    });
                }
            })("#so_latest_blog_95_6472416921504444938");
        });
        //]]>



//<![CDATA[
        jQuery(document).ready(function ($) {
            ;
            (function (element) {
                var $element = $(element),
                    $extraslider = $('.extraslider-inner', $element),
                    $featureslider = $('.product-feature', $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none';

                $extraslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.extraslider-inner .owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));
                    $('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');
                });

                $extraslider.owlCarousel2({
                    rtl: false,
                    margin: 0,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 1},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $extraslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    var $item = $('.extraslider-inner .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });
                /*feature product*/
                $featureslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));
                    $('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');
                });

                $featureslider.owlCarousel2({
                    rtl: false,
                    margin: 0,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 1},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $featureslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    var $item = $('.product-feature .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });

                function _getAnimate($el) {
                    if (_effect == 'none') return;
                    $extraslider.removeClass('extra-animate');
                    $el.each(function (i) {
                        var $_el = $(this);
                        $(this).css({
                            '-webkit-animation': _effect + ' ' + _duration + "ms ease both",
                            '-moz-animation': _effect + ' ' + _duration + "ms ease both",
                            '-o-animation': _effect + ' ' + _duration + "ms ease both",
                            'animation': _effect + ' ' + _duration + "ms ease both",
                            '-webkit-animation-delay': +i * _delay + 'ms',
                            '-moz-animation-delay': +i * _delay + 'ms',
                            '-o-animation-delay': +i * _delay + 'ms',
                            'animation-delay': +i * _delay + 'ms',
                            'opacity': 1
                        }).animate({
                            opacity: 1
                        });

                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            'animation': '',
                            '-webkit-animation': '',
                            '-moz-animation': '',
                            '-o-animation': '',
                            'opacity': 1
                        });
                    });
                }

                data = new Date(2013, 10, 26, 12, 00, 00);

                function CountDown(date, id) {
                    dateNow = new Date();
                    amount = date.getTime() - dateNow.getTime();
                    if (amount < 0 && $('#' + id).length) {
                        $('.' + id).html("Now!");
                    } else {
                        days = 0;
                        hours = 0;
                        mins = 0;
                        secs = 0;
                        out = "";
                        amount = Math.floor(amount / 1000);
                        days = Math.floor(amount / 86400);
                        amount = amount % 86400;
                        hours = Math.floor(amount / 3600);
                        amount = amount % 3600;
                        mins = Math.floor(amount / 60);
                        amount = amount % 60;
                        secs = Math.floor(amount);
                        if (days != 0) {
                            out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "Day" : "Days") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        } else if (hours != 0) {
                            out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else if (days == 0 && hours == 0) {
                            out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else {
                            out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        }

                        $('.' + id).html(out);

                        setTimeout(function () {
                            CountDown(date, id);
                        }, 1000);
                    }
                }
            });
        });
        //]]>
window.location.href="#slide-1";










//<![CDATA[
        jQuery(document).ready(function ($) {
            (function (element) {
                var $element = $(element),
                    $extraslider = $(".extraslider-inner", $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none ';

                $extraslider.on("initialized.owl.carousel2", function () {
                    var $item_active = $(".owl2-item.active", $element);
                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $(".owl2-item", $element);
                        $item.css({
                            "opacity": 1,
                            "filter": "alpha(opacity = 100)"
                        });
                    }


                    $(".owl2-controls", $element).insertBefore($extraslider);
                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                });

                $extraslider.owlCarousel2({
                    rtl: false,
                    margin: 0,
                    slideBy: 1,
                    autoplay: 0,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 0,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: 1,
                    touchDrag: 1,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 1},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: "owl2-dot",
                    dotsClass: "owl2-dots",
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ["&#171 ", "&#187 "],
                    navClass: ["owl2-prev", "owl2-next"]

                });

                $extraslider.on("translate.owl.carousel2", function (e) {


                    var $item_active = $(".owl2-item.active", $element);
                    _UngetAnimate($item_active);
                    _getAnimate($item_active);
                });

                $extraslider.on("translated.owl.carousel2", function (e) {


                    var $item_active = $(".owl2-item.active", $element);
                    var $item = $(".owl2-item", $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    } else {

                        $item.css({
                            "opacity": 1,
                            "filter": "alpha(opacity = 100)"
                        });

                    }
                });

                function _getAnimate($el) {
                    if (_effect == "none") return;
                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                    $extraslider.removeClass("extra-animate");
                    $el.each(function (i) {
                        var $_el = $(this);
                        $(this).css({
                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                            "-o-animation": _effect + " " + _duration + "ms ease both",
                            "animation": _effect + " " + _duration + "ms ease both",
                            "-webkit-animation-delay": +i * _delay + "ms",
                            "-moz-animation-delay": +i * _delay + "ms",
                            "-o-animation-delay": +i * _delay + "ms",
                            "animation-delay": +i * _delay + "ms",
                            "opacity": 1
                        }).animate({
                            opacity: 1
                        });

                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            "animation": "",
                            "-webkit-animation": "",
                            "-moz-animation": "",
                            "-o-animation": "",
                            "opacity": 1
                        });
                    });
                }

            })("#so_extra_slider_20014185151504444938 ");
        });
        //]]>
//<![CDATA[
        jQuery(document).ready(function ($) {
            ;
            (function (element) {
                var $element = $(element),
                    $extraslider = $('.extraslider-inner', $element),
                    $featureslider = $('.product-feature', $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none';

                $extraslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.extraslider-inner .owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));
                    $('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');
                });

                $extraslider.owlCarousel2({
                    rtl: false,
                    margin: 30,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 2},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $extraslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    var $item = $('.extraslider-inner .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });
                /*feature product*/
                $featureslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));
                    $('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');
                });

                $featureslider.owlCarousel2({
                    rtl: false,
                    margin: 30,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 1},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $featureslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    var $item = $('.product-feature .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });

                function _getAnimate($el) {
                    if (_effect == 'none') return;
                    $extraslider.removeClass('extra-animate');
                    $el.each(function (i) {
                        var $_el = $(this);
                        $(this).css({
                            '-webkit-animation': _effect + ' ' + _duration + "ms ease both",
                            '-moz-animation': _effect + ' ' + _duration + "ms ease both",
                            '-o-animation': _effect + ' ' + _duration + "ms ease both",
                            'animation': _effect + ' ' + _duration + "ms ease both",
                            '-webkit-animation-delay': +i * _delay + 'ms',
                            '-moz-animation-delay': +i * _delay + 'ms',
                            '-o-animation-delay': +i * _delay + 'ms',
                            'animation-delay': +i * _delay + 'ms',
                            'opacity': 1
                        }).animate({
                            opacity: 1
                        });

                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            'animation': '',
                            '-webkit-animation': '',
                            '-moz-animation': '',
                            '-o-animation': '',
                            'opacity': 1
                        });
                    });
                }

                data = new Date(2013, 10, 26, 12, 00, 00);

                function CountDown(date, id) {
                    dateNow = new Date();
                    amount = date.getTime() - dateNow.getTime();
                    if (amount < 0 && $('#' + id).length) {
                        $('.' + id).html("Now!");
                    } else {
                        days = 0;
                        hours = 0;
                        mins = 0;
                        secs = 0;
                        out = "";
                        amount = Math.floor(amount / 1000);
                        days = Math.floor(amount / 86400);
                        amount = amount % 86400;
                        hours = Math.floor(amount / 3600);
                        amount = amount % 3600;
                        mins = Math.floor(amount / 60);
                        amount = amount % 60;
                        secs = Math.floor(amount);
                        if (days != 0) {
                            out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "Day" : "Days") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        } else if (hours != 0) {
                            out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else if (days == 0 && hours == 0) {
                            out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else {
                            out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        }


                        $('.' + id).html(out);

                        setTimeout(function () {
                            CountDown(date, id);
                        }, 1000);
                    }
                }

                if (listdeal2.length > 0) {
                    for (var i = 0; i < listdeal2.length; i++) {
                        var arr = listdeal2[i].split("|");
                        if (arr[1].length) {
                            var data = new Date(arr[1]);
                            CountDown(data, arr[0]);
                        }
                    }
                }
            })('#so_deals_150415776409032017132217');
        });
        //]]>
//<![CDATA[
        jQuery(document).ready(function ($) {
            (function (element) {
                var $element = $(element),
                    $extraslider = $(".extraslider-inner", $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none ';

                $extraslider.on("initialized.owl.carousel2", function () {
                    var $item_active = $(".owl2-item.active", $element);
                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $(".owl2-item", $element);
                        $item.css({
                            "opacity": 1,
                            "filter": "alpha(opacity = 100)"
                        });
                    }


                    $(".owl2-controls", $element).insertBefore($extraslider);
                    $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

                });

                $extraslider.owlCarousel2({
                    rtl: false,
                    margin: 30,
                    slideBy: 1,
                    autoplay: 0,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 0,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: 1,
                    touchDrag: 1,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 2},
                        768: {items: 3},
                        992: {items: 3},
                        1200: {items: 4}
                    },
                    dotClass: "owl2-dot",
                    dotsClass: "owl2-dots",
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: true,
                    navSpeed: 500,
                    navText: ["&#171 ", "&#187 "],
                    navClass: ["owl2-prev", "owl2-next"]

                });

                $extraslider.on("translate.owl.carousel2", function (e) {

                    var $item_active = $(".owl2-item.active", $element);
                    _UngetAnimate($item_active);
                    _getAnimate($item_active);
                });
                $extraslider.on("translated.owl.carousel2", function (e) {

                    var $item_active = $(".owl2-item.active", $element);
                    var $item = $(".owl2-item", $element);
                    _UngetAnimate($item);
                    if ($item_active.length > 1 && _effect != "none") {
                        _getAnimate($item_active);
                    } else {
                        $item.css({
                            "opacity": 1,
                            "filter": "alpha(opacity = 100)"
                        });
                    }
                });

                function _getAnimate($el) {
                    if (_effect == "none") return;
                    //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                    $extraslider.removeClass("extra-animate");
                    $el.each(function (i) {
                        var $_el = $(this);
                        var i = i + 1;
                        $(this).css({
                            "-webkit-animation": _effect + " " + _duration + "ms ease both",
                            "-moz-animation": _effect + " " + _duration + "ms ease both",
                            "-o-animation": _effect + " " + _duration + "ms ease both",
                            "animation": _effect + " " + _duration + "ms ease both",
                            "-webkit-animation-delay": +i * _delay + "ms",
                            "-moz-animation-delay": +i * _delay + "ms",
                            "-o-animation-delay": +i * _delay + "ms",
                            "animation-delay": +i * _delay + "ms",

                        }).animate({});
                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            "animation": "",
                            "-webkit-animation": "",
                            "-moz-animation": "",
                            "-o-animation": "",
                        });
                    });
                }
            })("#so_extra_slider_1062702841504444937 ");
        });
        //]]>

//<![CDATA[
        jQuery(document).ready(function ($) {
            ;
            (function (element) {
                var $element = $(element),
                    $extraslider = $('.extraslider-inner', $element),
                    $featureslider = $('.product-feature', $element),
                    _delay = 500,
                    _duration = 800,
                    _effect = 'none';

                $extraslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.extraslider-inner .owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));
                    $('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');
                });

                $extraslider.owlCarousel2({
                    rtl: false,
                    margin: 30,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 2},
                        992: {items: 2},
                        1200: {items: 2}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: false,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $extraslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.extraslider-inner .owl2-item.active', $element);
                    var $item = $('.extraslider-inner .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });
                /*feature product*/
                $featureslider.on('initialized.owl.carousel2', function () {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    }
                    else {
                        var $item = $('.owl2-item', $element);
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                    $('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));
                    $('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');
                });

                $featureslider.owlCarousel2({
                    rtl: false,
                    margin: 30,
                    slideBy: 1,
                    autoplay: false,
                    autoplayHoverPause: 0,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    startPosition: 0,
                    mouseDrag: true,
                    touchDrag: true,
                    autoWidth: false,
                    responsive: {
                        0: {items: 1},
                        480: {items: 1},
                        768: {items: 1},
                        992: {items: 1},
                        1200: {items: 1}
                    },
                    dotClass: 'owl2-dot',
                    dotsClass: 'owl2-dots',
                    dots: false,
                    dotsSpeed: 500,
                    nav: true,
                    loop: false,
                    navSpeed: 500,
                    navText: ['&#171;', '&#187;'],
                    navClass: ['owl2-prev', 'owl2-next']
                });

                $featureslider.on('translated.owl.carousel2', function (e) {
                    var $item_active = $('.product-feature .owl2-item.active', $element);
                    var $item = $('.product-feature .owl2-item', $element);

                    _UngetAnimate($item);

                    if ($item_active.length > 1 && _effect != 'none') {
                        _getAnimate($item_active);
                    } else {
                        $item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
                    }
                });

                function _getAnimate($el) {
                    if (_effect == 'none') return;
                    $extraslider.removeClass('extra-animate');
                    $el.each(function (i) {
                        var $_el = $(this);
                        $(this).css({
                            '-webkit-animation': _effect + ' ' + _duration + "ms ease both",
                            '-moz-animation': _effect + ' ' + _duration + "ms ease both",
                            '-o-animation': _effect + ' ' + _duration + "ms ease both",
                            'animation': _effect + ' ' + _duration + "ms ease both",
                            '-webkit-animation-delay': +i * _delay + 'ms',
                            '-moz-animation-delay': +i * _delay + 'ms',
                            '-o-animation-delay': +i * _delay + 'ms',
                            'animation-delay': +i * _delay + 'ms',
                            'opacity': 1
                        }).animate({
                            opacity: 1
                        });

                        if (i == $el.size() - 1) {
                            $extraslider.addClass("extra-animate");
                        }
                    });
                }

                function _UngetAnimate($el) {
                    $el.each(function (i) {
                        $(this).css({
                            'animation': '',
                            '-webkit-animation': '',
                            '-moz-animation': '',
                            '-o-animation': '',
                            'opacity': 1
                        });
                    });
                }

                data = new Date(2013, 10, 26, 12, 00, 00);

                function CountDown(date, id) {
                    dateNow = new Date();
                    amount = date.getTime() - dateNow.getTime();
                    if (amount < 0 && $('#' + id).length) {
                        $('.' + id).html("Now!");
                    } else {
                        days = 0;
                        hours = 0;
                        mins = 0;
                        secs = 0;
                        out = "";
                        amount = Math.floor(amount / 1000);
                        days = Math.floor(amount / 86400);
                        amount = amount % 86400;
                        hours = Math.floor(amount / 3600);
                        amount = amount % 3600;
                        mins = Math.floor(amount / 60);
                        amount = amount % 60;
                        secs = Math.floor(amount);
                        if (days != 0) {
                            out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "Day" : "Days") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        } else if (hours != 0) {
                            out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
                        }
                        if (days == 0 && hours != 0) {
                            out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else if (days == 0 && hours == 0) {
                            out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        } else {
                            out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
                            out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
                            out = out.substr(0, out.length - 2);
                        }

                        $('.' + id).html(out);

                        setTimeout(function () {
                            CountDown(date, id);
                        }, 1000);
                    }
                }

                if (listdeal1.length > 0) {
                    for (var i = 0; i < listdeal1.length; i++) {
                        var arr = listdeal1[i].split("|");
                        if (arr[1].length) {
                            var data = new Date(arr[1]);
                            CountDown(data, arr[0]);
                        }
                    }
                }
            })('#so_deals_83259092109032017132217');
        });
        //]]>









        $(document).ready(function () {
            $('a[href="http://opencart.opencartworks.com/themes/so_oneshop/"]').each(function () {
                $(this).parents('.with-sub-menu').addClass('sub-active');
            });
        });





        (function ($) {
            $.fn.Soautocomplete = function (option) {
                return this.each(function () {
                    this.timer = null;
                    this.items = new Array();

                    $.extend(this, option);

                    $(this).attr('autocomplete', 'off');

                    // Focus
                    $(this).on('focus', function () {
                        this.request();
                    });

                    // Blur
                    $(this).on('blur', function () {
                        setTimeout(function (object) {
                            object.hide();
                        }, 200, this);
                    });

                    // Keydown
                    $(this).on('keydown', function (event) {
                        switch (event.keyCode) {
                            case 27: // escape
                                this.hide();
                                break;
                            default:
                                this.request();
                                break;
                        }
                    });

                    // Click
                    this.click = function (event) {
                        event.preventDefault();

                        value = $(event.target).parent().attr('data-value');

                        if (value && this.items[value]) {
                            this.select(this.items[value]);
                        }
                    }

                    // Show
                    this.show = function () {
                        var pos = $(this).position();

                        $(this).siblings('ul.dropdown-menu').css({
                            top: pos.top + $(this).outerHeight(),
                            left: pos.left
                        });

                        $(this).siblings('ul.dropdown-menu').show();
                    }

                    // Hide
                    this.hide = function () {
                        $(this).siblings('ul.dropdown-menu').hide();
                    }

                    // Request
                    this.request = function () {
                        clearTimeout(this.timer);

                        this.timer = setTimeout(function (object) {
                            object.source($(object).val(), $.proxy(object.response, object));
                        }, 200, this);
                    }

                    // Response
                    this.response = function (json) {
                        html = '';

                        if (json.length) {
                            for (i = 0; i < json.length; i++) {
                                this.items[json[i]['value']] = json[i];
                            }

                            for (i = 0; i < json.length; i++) {
                                if (!json[i]['category']) {
                                    html += '<li class="media" data-value="' + json[i]['value'] + '" title="' + json[i]['label'] + '">';
                                    if (json[i]['image'] && json[i]['show_image'] && json[i]['show_image'] == 1) {
                                        html += '   <a class="media-left" href="' + json[i]['link'] + '"><img class="pull-left" src="' + json[i]['image'] + '"></a>';
                                    }

                                    html += '<div class="media-body">';
                                    html += '<a href="' + json[i]['link'] + '" title="' + json[i]['label'] + '"><span>' + json[i]['cate_name'] + json[i]['label'] + '</span></a>';
                                    if (json[i]['price'] && json[i]['show_price'] && json[i]['show_price'] == 1) {
                                        html += '   <div class="box-price">';
                                        if (!json[i]['special']) {
                                            html += '<span class="price">' + json[i]['price'] + '</span>';
                                            ;
                                        } else {
                                            html += '</span><span class="price-new">' + json[i]['special'] + '</span>' + '<span class="price-old" style="text-decoration:line-through;">' + json[i]['price'];
                                        }

                                        html += '   </div>';
                                    }
                                    html += '</div></li>';
                                    html += '<li class="clearfix"></li>';
                                }
                            }

                            // Get all the ones with a categories
                            var category = new Array();

                            for (i = 0; i < json.length; i++) {
                                if (json[i]['category']) {
                                    if (!category[json[i]['category']]) {
                                        category[json[i]['category']] = new Array();
                                        category[json[i]['category']]['name'] = json[i]['category'];
                                        category[json[i]['category']]['item'] = new Array();
                                    }

                                    category[json[i]['category']]['item'].push(json[i]);
                                }
                            }

                            for (i in category) {
                                html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                                for (j = 0; j < category[i]['item'].length; j++) {
                                    html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                                }
                            }
                        }

                        if (html) {
                            this.show();
                        } else {
                            this.hide();
                        }

                        $(this).siblings('ul.dropdown-menu').html(html);
                    }

                    $(this).after('<ul class="dropdown-menu"></ul>');

                });
            }
        })(window.jQuery);
        $(document).ready(function () {
            var selector = '#search1';
            var total = 0;
            var showimage = 1;
            var showprice = 1;
            var character = 3;
            var height = 70;
            var width = 70;
            $(selector).find('input[name=\'search\']').Soautocomplete({
                delay: 500,
                source: function (request, response) {
                    var category_id = $(".select_category select[name=\"category_id\"]").first().val();
                    if (typeof(category_id) == 'undefined')
                        category_id = 0;
                    var limit = 5;
                    if (request.length >= character) {
                        $.ajax({
                            url: 'index.php?route=extension/module/so_searchpro/autocomplete&filter_category_id=' + category_id + '&limit=' + limit + '&width=' + width + '&height=' + height + '&filter_name=' + encodeURIComponent(request),
                            dataType: 'json',
                            success: function (json) {
                                response($.map(json, function (item) {
                                    total = 0;
                                    if (item.total) {
                                        total = item.total;
                                    }

                                    return {
                                        price: item.price,
                                        special: item.special,
                                        tax: item.tax,
                                        label: item.name,
                                        cate_name: (item.category_name) ? item.category_name + ' > ' : '',
                                        image: item.image,
                                        link: item.link,
                                        minimum: item.minimum,
                                        show_price: showprice,
                                        show_image: showimage,
                                        value: item.product_id,
                                    }
                                }));
                            }
                        });
                    }
                },
            });
        });