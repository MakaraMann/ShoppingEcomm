<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@getHome')->name('home');
Route::get('product_detail/{id}','HomeController@getProductDetail')->name('get_product_detail');
Route::get('product/parent/{parent_cat_id}','HomeController@getParentProduct')->name('get_product_by_parent');
Route::get('product/child/{child_cat_id}','HomeController@getChildProduct')->name('get_product_by_child');
Route::get('view_cart', 'CartController@index')->name('view_cart');
Route::get('cart/add_item/{id}', 'CartController@addItem')->name('add_cart');
Route::get('cart/remove_item/{id}', 'CartController@removeItem')->name('remove_item');
Route::get('cart/update/{id}', 'CartController@updateItem')->name('cart_update');
Route::get('cart/check_out', 'CartController@checkOut')->name('check_out');

//-------------------------------\\ My account //-------------------------------------\\
	Route::get('login_form','UserController@login')->name('login');
	Route::post('post/login','Auth\AuthController@postLogin')->name('store_login');
    Route::get('get_register', 'Auth\AuthController@webRegister')->name('get_register');
    Route::post('web-register', 'Auth\AuthController@webRegisterPost');
    Route::get('user/activation/{token}', 'Auth\AuthController@userActivation');

Route::group(['namespace'=>'Admin'],function(){
	Route::get('cpanel','AdminController@getAdminLogin')->name('admin_login');
	Route::post('post_admin_login','AdminController@postAdminLogin')->name('post_admin_login');
	Route::group(['middleware' => 'admin'], function() {
		Route::get('admin_logout','AdminController@adminLogout')->name('admin_logout');
		Route::get('dashboard','AdminController@getDashboard')->name('dashboard');

		//----------------------------------\\ Route Admin        //--------------------------\\
		Route::get('all_admin','AdminController@getAdmin')->name('all_admin');
		Route::get('add_admin','AdminController@getAddAdmin')->name('add_admin');
		Route::post('post_admin','AdminController@postAdmin')->name('post_admin');
		Route::get('edit_admin/{id}','AdminController@getEditAdmin')->name('edit_admin');
		Route::get('delete_admin/{id}','AdminController@deleteAdmin')->name('delete_admin');
		Route::post('update_admin/{id}','AdminController@updateAdmin')->name('update_admin');


		//-----------------------------------\\ Route Sponsors //----------------------------\\

		Route::get('all_sponsor','SponsorController@getSponsor')->name('all_sponsor');
		Route::get('add_sponsor','SponsorController@getAddSponsor')->name('add_sponsor');
		Route::post('post_sponsor','SponsorController@postSponsor')->name('post_sponsor');
		Route::get('edit_sponsor/{id}','SponsorController@getEditSponsor')->name('edit_sponsor');
		Route::get('delete_sponsor/{id}','SponsorController@deleteSponsor')->name('delete_sponsor');
		Route::post('update_sponsor/{id}','SponsorController@updateSponsor')->name('update_sponsor');

        //-----------------------------------\\ Route Sponsors //----------------------------\\

        Route::get('all_supplier','SupplierController@getSupplier')->name('all_supplier');

		//-----------------------------------\\ Route Main Categories //----------------------------\\

		Route::get('all_main_categories','MainCategoriesController@getMainCategories')->name('all_main_categories');
		Route::get('add_main_categories','MainCategoriesController@getAddMainCategories')->name('add_main_categories');
		Route::post('post_main_categories','MainCategoriesController@postMainCategories')->name('post_main_categories');
		Route::get('edit_main_categories/{id}','MainCategoriesController@getEditMainCategories')->name('edit_main_categories');
		Route::post('delete_main_categories/{id}','MainCategoriesController@deleteMainCategories')->name('delete_main_categories');

		//-----------------------------------\\ Route Parent Categories //----------------------------\\

		Route::get('all_parent_categories/{main_cat_id}','ParentCategoriesController@getParentCategories')->name('all_parent_categories');
		Route::get('add_parent_categorie/{main_cat_id}','ParentCategoriesController@getAddParentCategories')->name('add_parent_categories');
		Route::post('post_parent_categories/{main_cat_id}','ParentCategoriesController@postParentCategories')->name('post_parent_categories');
		Route::get('edit_parent_categories/{id}','ParentCategoriesController@getEditParentCategories')->name('edit_parent_categories');
		Route::post('delete_parent_categories/{id}','ParentCategoriesController@deleteParentCategories')->name('delete_parent_categories');


		//-------------------------//Route Child Categories//-----------------------------//
		Route::get('all_child_categories/{main_cat_id}/{parent_cat_id}','ChildCategoriesController@getChildCategories')->name('all_child_categories');
		Route::get('add_child_categories/{main_cat_id}/{parent_cat_id}','ChildCategoriesController@addChildCategories')->name('add_child_categories');
		Route::post('post_child_categories/{main_cat_id}/{parent_cat_id}','ChildCategoriesController@postChildCategories')->name('post_child_categories');
		Route::post('delete_child_categories/{id}','ChildCategoriesController@deleteChildCategories')->name('delete_child_categories');
		Route::get('edit_child_categories/{id}','ChildCategoriesController@editChildCategories')->name('edit_child_categories');



		//-------------------------//Route Product//-----------------------------//
		Route::get('all_product/{main_cat_id}/{parent_cat_id}/{child_cat_id}','ProductController@getProduct')->name('all_product');
		Route::get('add_product/{main_cat_id}/{parent_cat_id}/{child_cat_id}','ProductController@addProduct')->name('add_product');
		Route::post('post_product/{main_cat_id}/{parent_cat_id}/{child_cat_id}','ProductController@postProduct')->name('post_product');
		Route::post('delete_product/{id}','ProductController@deleteProduct')->name('delete_product');
		Route::get('edit_product/{id}','ProductController@editProduct')->name('edit_product');
	});
});
Route::get('/bb', function (\Illuminate\Http\Request $request) {
    session(['lan' => $request->lan]);
    return redirect('/');
});
