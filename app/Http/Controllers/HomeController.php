<?php

namespace App\Http\Controllers;
use App\Products;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class HomeController extends BaseController
{
    public function getHome(){
    	$products=Products::orderBy('id','desc')->where('status','enable')->get();
        $cartItems = Cart::content();
        return view('frontent.home.index', compact('cartItems'))->with('products', $products);
    }

    public function getProductDetail($id){
        $product=Products::find($id);
        $key=0;
    	return view('frontent.detail.index')->with('product',$product)->with('key',$key);
    }

    public function getParentProduct($parent_cat_id){
    	$products=Products::where('parent_cat_id',$parent_cat_id)->get();
    	return view('frontent.products.index')->with('products',$products);
    }

    public function getChildProduct($child_cat_id){
    	$products=Products::where('child_cat_id',$child_cat_id)->get();
    	return view('frontent.products.index')->with('products',$products);
    }
}
