<?php

namespace App\Http\Controllers;

use App\Products;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    public function index()
    {
        $cartItems = Cart::content();
        return view('frontent.checkout.cart', compact('cartItems'));
    }

    public function addItem($id)
    {
        $product = Products::find($id);
        Cart::add($product->id, json_decode($product->product_name)->en, 1, $product->product_price);
        return back();
    }

    public function removeItem($id)
    {
        Cart::remove($id);
        return back();
    }

    public function updateItem(Request $request, $id)
    {
        Cart::update($id, $request->quantity);
        $cartItems = Cart::content();
        return $cartItems;
    }

    public function checkOut(Request $request)
    {
        return view('frontent.checkout.checkout');
    }
}
