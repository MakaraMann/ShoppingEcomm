<?php  
namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use App\Products;
use App\Photos;
use App\MainCategories;
use App\ParentCategories;
use App\ChildCategories;
use File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ProductController extends Controller
{
	public function getProduct($main_cat_id,$parent_cat_id,$child_cat_id){
        $products=Products::where('main_cat_id',$main_cat_id)->where('parent_cat_id',$parent_cat_id)->where('child_cat_id',$child_cat_id)->get();
        $main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
        $child_category_name=ChildCategories::find($child_cat_id);
		return view('backent.products.index')->with('products',$products)->with('child_cat_id',$child_cat_id)->with('parent_cat_id',$parent_cat_id)->with('main_cat_id',$main_cat_id)->with('parent_category_name',$parent_category_name)->with('main_category_name',$main_category_name)->with('child_category_name',$child_category_name);
	}
	public function addProduct($main_cat_id,$parent_cat_id,$child_cat_id){
		$main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
        $child_category_name=ChildCategories::find($child_cat_id);
		return view('backent.products.form',['product'=>null])->with('photos',null)->with('parent_cat_id',$parent_cat_id)->with('main_cat_id',$main_cat_id)->with('parent_category_name',$parent_category_name)->with('main_category_name',$main_category_name)->with('child_category_name',$child_category_name)->with('child_cat_id',$child_cat_id);
	}
	public function postProduct(Request $request,$main_cat_id,$parent_cat_id,$child_cat_id){
    	$this->validate($request,[
    		'p_name'=>'required',
            'p_stock'=>'required',
            'p_price'=>'required',
            'p_code'=>'required',
            'description'=>'required'
        ]);
        $id=$request->id;
        $tz='GMT+7';
        $tim=Carbon::now($tz);
        $tomorrow = Carbon::now($tz)->addDay();
        $lastWeek = Carbon::now($tz)->subWeek();
        $today = Carbon::now($tz)->today();
        $c_date=$lastWeek->Format('d F Y');
        $time=$tim->Format('h:i A');
        if ($id == 0) { 
            $this->validate($request,[
            'p_name'=>'required',
            'p_stock'=>'required',
            'p_price'=>'required',
            'p_code'=>'required',
            'description'=>'required',
            'p_images'=>'required'
        ]);
            $product=new Products();
            $images = $request->file('p_images');
            $input= time().'.'.$images->getClientOriginalName();
            $filename='web/images/product/'.$input;
            $images->move('web/images/product', $input);
            $product->product_name = json_encode($request->p_name);
            $product->product_code = json_encode($request->p_code);
            $product->parent_cat_id=$parent_cat_id;
            $product->main_cat_id=$main_cat_id;
            $product->child_cat_id=$child_cat_id;
            $product->product_stock_in=$request->input('p_stock');
            $product->product_price=$request->input('p_price');
            $product->option="admin";
            $product->product_description=json_encode($request->description);
            $product->publish_time=$time;
            $product->publish_date=$c_date;
            $product->image=$filename;

            if ($product->save()) {
                $images = $request->file('product_gallery');
                foreach( $images as $image){
                    $input= time().'.'.$image->getClientOriginalName();
                    $filename='web/images/product_gallery/'.$input;
                    $image->move('web/images/product_gallery', $input);
                    $photo = new Photos();
                    $photo->image=$filename;
                    $photo->product_id =$product->id;
                    $photo->save();
                }
              return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('success','You add your product is successfully!');
            }else{
                return Redirect::route('add_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('fails','You add your product is not successfully. Please add it again?');
            }
        }else{
            $product=Products::find($id);
            if ($request->hasFile('p_images')) {
                $oldimage=$product->image;
                File::delete($oldimage);
                $newimage=$request->file('p_images');
                $input= time().'.'.$newimage->getClientOriginalName();
                $filename='web/images/product/'.$input;
                $newimage->move('web/images/product', $input);

                $product->product_name = json_encode($request->p_name);
	            $product->product_code = json_encode($request->p_code);
	            $product->product_stock_in=$request->input('p_stock');
            	$product->product_price=$request->input('p_price');
	            $product->product_description=json_encode($request->description);
                $product->publish_time=$time;
                $product->image=$filename;
                $product->publish_date=$c_date;
                if ($product->save()) {
                    return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('success','You update your product is successfully!');
                }else{
                    return Redirect::route('edit_product',$id)->with('fails','You update your product is not successfully. Please update it again?');
                }
            }elseif($request->hasFile('product_gallery')){
                $photos=Photos::where('product_id',$id)->get();
                foreach($photos as $ph){
                    $ph_id=$ph->id;
                    $photos=Photos::find($ph_id);
                    $phImage=$photos->image;
                    $del=File::delete($phImage);
                    $photos->delete();
                }

                $product->product_name = json_encode($request->p_name);
                $product->product_code = json_encode($request->p_code);
                $product->product_stock_in=$request->input('p_stock');
                $product->product_price=$request->input('p_price');
                $product->product_description=json_encode($request->description);
                $product->publish_time=$time;
                $product->publish_date=$c_date;
                if ($product->save()) {
                    $images = $request->file('product_gallery');
                    foreach( $images as $image){
                        $input= time().'.'.$image->getClientOriginalName();
                        $filename='web/images/product_gallery/'.$input;
                        $image->move('web/images/product_gallery', $input);
                        $photo = new Photos();
                        $photo->image=$filename;
                        $photo->product_id =$product->id;
                        $photo->save();
                    }
                    return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('success','You update your product is successfully!');
                }else{
                    return Redirect::route('edit_product',$id)->with('fails','You update your product is not successfully. Please update it again?');
                }
            }else{
                $product->product_name = json_encode($request->p_name);
	            $product->product_code = json_encode($request->p_code);
	            $product->product_stock_in=$request->input('p_stock');
            	$product->product_price=$request->input('p_price');
	            $product->product_description=json_encode($request->description);
                $product->publish_time=$time;
                $product->publish_date=$c_date;
                if ($product->save()) {
                    return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('success','You update your product is successfully!');
                }else{
                    return Redirect::route('edit_product',$id)->with('fails','You update your product is not successfully. Please update it again?');
                }
            }
        }
    }
    public function deleteProduct(Request $request,$id){
	    $status=$request->input('status');
        $photos=Photos::where('product_id',$id)->get();
        foreach($photos as $ph){
            $ph_id=$ph->id;
            $photos=Photos::find($ph_id);
            $photos->status=$status;
            $photos->save();
//            $phImage=$photos->image;
//            File::delete($phImage);
//            $photos->delete();
        }
        $product=Products::find($id);
        $parent_cat_id=$product->parent_cat_id;
        $main_cat_id=$product->main_cat_id;
        $child_cat_id=$product->child_cat_id;
        $product->status=$status;
        if ($product->save()) {
            return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('success','You delete your product is successfully!');
        }else{
            return Redirect::route('all_product',[$product->main_cat_id,$product->parent_cat_id,$product->child_cat_id])->with('fails','Sorry, you delete your product is not successfully!');
        }
    }
    public function editProduct($id){
        $product=Products::find($id);
        $main_cat_id=$product->main_cat_id;
        $parent_cat_id=$product->parent_cat_id;
        $child_cat_id=$product->child_cat_id;
        $photos=Photos::where('product_id',$id)->get();
        $main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
        $child_category_name=ChildCategories::find($child_cat_id);
        return view('backent.products.form',['product'=>$product])->with('parent_cat_id',$parent_cat_id)->with('photos',$photos)->with('main_cat_id',$main_cat_id)->with('child_cat_id',$child_cat_id)->with('main_category_name',$main_category_name)->with('parent_category_name',$parent_category_name)->with('child_category_name',$child_category_name);
    }
}