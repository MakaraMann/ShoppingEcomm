<?php

namespace App\Http\Controllers\Admin;
use Response;
use App\MainCategories;
use App\ParentCategories;
use App\ChildCategories;
use File;
use App\Photos;
use App\Products;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers;

class MainCategoriesController extends Controller
{
    public function getMainCategories(){
    	$main_categories=MainCategories::all();
    	$key=1;
    	return view('backent.main_categories.index')->with('main_categories',$main_categories)->with('key',$key);
    }
    public function getAddMainCategories(){
		return view('backent.main_categories.add',['main_categories'=>null]);
	}
	public function postMainCategories(Request $request){
    	$this->validate($request, [
    		'main_cateories_name'=>'required'        
        ]);
        $id=$request->id;
        if ($id == 0) {
            $category=new MainCategories();
            $category->main_cat_name = json_encode($request->main_cateories_name);
            if ($category->save()) {
              return Redirect::route('all_main_categories')->with('success','You add main categories is successfully');
            }else{
                return Redirect::route('add_main_categories')->with('fails','You add main categories is not successfully');
            }
        }else{
            $category=MainCategories::find($id);
            $category->main_cat_name=json_encode($request->main_cateories_name);
            if ($category->save()) {
                return Redirect::route('all_main_categories')->with('success','You update categories is successfully');
            }else{
                return Redirect::route('edit_main_categories',$id)->with('success','You update categories is not successfully');
            }
        }
    }
    public function deleteMainCategories(Request $request,$id){
	    $status=$request->input('status');
        $main_category=MainCategories::find($id);
        $products=Products::where('main_cat_id',$id)->get();
        foreach ($products as $product) {
            $photos=Photos::where('product_id',$product->id)->get();
            foreach($photos as $ph){
                $ph_id=$ph->id;
                $photos=Photo::find($ph_id);
                $photos->status=$status;
                $photos->save();
//                $phImage=$photos->image;
//                $del=File::delete($phImage);
//                $photos->delete();
            }
            $pro=Products::find($product->id);
            $product->delete();
        }
        $child_category=ChildCategories::where('main_cat_id',$id)->get();
        foreach($child_category as $c_category){
            $c_category->status=$status;
            $c_category->save();
        }
        $parent_category=ParentCategories::where('main_cat_id',$id)->get();
        foreach($parent_category as $p_category){
            $p_category->status=$status;
            $p_category-save();
        }
        $main_category->status=$status;
        if ($main_category->save()) {
            return Redirect::route('all_main_categories')->with('success','You  '.$status.'  your parent categories is successfully!');;
        }else{
            return Redirect::route('all_main_categories')->with('fails','Sorry, You  '.$status.'  parent categories is not successfully');;
        }
        return Response::json($message);
    }
    public function getEditMainCategories($id){
        $main_categories=MainCategories::find($id);
        return view('backent.main_categories.add',['main_categories'=>$main_categories]);
    }
}
