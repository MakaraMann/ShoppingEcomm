<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Http\Requests;
use File;
use Hash;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers;
class AdminController extends Controller
{
    public function getDashboard(){
    	return view('backent.dashboard.index');
    }
    public function getAdminLogin(){
        return view('backent.login');
    }
    public function adminLogout(){
        Auth::logout();
        return Redirect::route('admin_login')->with('success','You logout is successfully!');
    }
    public function postAdminLogin(Request $request){
        $this->validate($request,[
            'username'=>'required',
            'password'=>'required'
        ]);
        $remember = (Input::has('remember')) ? true : false;
        $auth = Auth::attempt(array(
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'role'=>'admin'
        ),$remember);
        if ($auth) {
            return Redirect::route('dashboard')->with('success','Welcome to admin system of DyMaFashion');
        }else{
            return Redirect::route('admin_login')->with('fails','You login is not successfully!,Please login again?');
        }
    }
    public function getAdmin(){
    	$admin = User::all()->first();
		$admins=User::whereBetween('id', array(2, 10000))->get();
		$key=1;
		return view('backent.users.index')->with('admins',$admins)->with('admin',$admin)->with('key',$key);
    }
    public function getAddAdmin(){
    	return view('backent.users.add');
    }
    public function postAdmin(Request $request){
    	$this->validate($request,[
    		'username'=>'required',
    		'email'=>'required|email|unique:users',
    		'profile'=>'required',
    		'pass'=>'Required|AlphaNum|Between:4,8|Confirmed',
    		'c_pass'=>'Required|AlphaNum|Between:4,8'
        ]);
        $image = $request->file('profile');
        $input= time().'.'.$image->getClientOriginalExtension();
        $filename='web/images/profiles/'.$input;
        $image->move('web/images/profiles', $input);

        $admin=new User();
        $admin->username = $request->input('username');
        $admin->profile = $filename;
        $admin->email=$request->input('email');
        $admin->password=Hash::make($request->input('c_pass'));
        $admin->role='admin';
        if ($admin->save()) {
          return Redirect::route('add_admin')->with('success','You add information of user admin is successfully?');
        }else{
            return Redirect::route('add_admin')->with('fails','Please add information of user admin again?');
        }
    }
    public function getEditAdmin($id){
    	$admin=User::find($id);
        return view('backent.users.edit',['admin'=>$admin]);
    }
    public function updateAdmin(Request $request,$id){
        $admin=User::find($id);
        $oldPassword=$request->input('o_pass');
        $newPassword=$request->input('n_pass');

        if ($request->hasFile('profile')) {
            $oldImage=$admin->image;
            File::delete($oldImage);

            $image = $request->file('profile');
            $input= time().'.'.$image->getClientOriginalExtension();
            $filename='web/images/profiles/'.$input;
            $image->move('web/images/profiles', $input);

            if (Input::has('n_pass')) {
                if (Hash::check($oldPassword,$admin->password)){
                    $this->validate($request,[
                        'n_pass'=>'required',
                        'c_pass'=>'required|same:n_pass'
                    ]);
                    $admin->username = $request->input('username');
                    $admin->profile = $filename;
                    $admin->email=$request->input('email');
                    $admin->password=Hash::make($request->input('c_pass'));
                    if ($admin->save()) {
                        return Redirect::route('all_admin')->with('success','You update information of user admin is successfully!');
                    }else{
                        return Redirect::route('edit_admin',$id)->with('fails','Please update information of user admin again?');
                    }
                }else{
                    return Redirect::route('edit_admin',$id)->with('fails','Old password not mutch!.Please enter your old password again?');
                }
            }else{
                $admin->username = $request->input('username');
                $admin->profile = $filename;
                $admin->email=$request->input('email');
                if ($admin->save()) {
                    return Redirect::route('all_admin')->with('success','You update information of user admin is successfully!');
                }else{
                   return Redirect::route('edit_admin',$id)->with('fails','Please update information of user admin again?');
                }
            } 
        }else{
           if (Input::has('n_pass')) {
                if (Hash::check($oldPassword,$admin->password)){
                    $this->validate($request,[
                        'n_pass'=>'required',
                        'c_pass'=>'required|same:n_pass'
                    ]);
                    $admin->username = $request->input('username');
                    $admin->email=$request->input('email');
                    $admin->password=Hash::make($request->input('c_pass'));
                    if ($admin->save()) {
                        return Redirect::route('all_admin')->with('success','You update information of user admin is successfully?');
                    }else{
                        return Redirect::route('edit_admin',$id)->with('fails','Please update information of user admin again?');
                    }
                }else{
                    return Redirect::route('edit_admin',$id)->with('fails','Old password not mutch!.Please enter your old password again?');
                }
            }else{
                $admin->username = $request->input('username');
                $admin->email=$request->input('email');
                if ($admin->save()) {
                    return Redirect::route('all_admin')->with('success','You update information of user admin is successfully?');
                }else{
                    return Redirect::route('edit_admin',$id)->with('fails','Please update information of user admin again?');
                }
            }
        }
    }
    public function deleteAdmin($id){
        $admin=User::find($id);
        $oldImage=$admin->profile;
        File::delete($oldImage);
        if ($admin->delete()) {
            return Redirect::route('all_admin')->with('success','You delete information of user admin is successfully!');
        }else{
            return Redirect::route('all_admin')->with('fails','You delete information of user admin is not successfully!');
        }
	}
}
