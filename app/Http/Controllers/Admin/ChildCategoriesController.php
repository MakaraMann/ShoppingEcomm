<?php

namespace App\Http\Controllers\Admin;

use App\ParentCategories;
use App\ChildCategories;
use App\Products;
use App\Photos;
use File;
use App\MainCategories;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers;

class ChildCategoriesController extends Controller
{
    public function getChildCategories($main_cat_id,$parent_cat_id){
    	$child_categories=ChildCategories::where('main_cat_id',$main_cat_id)->where('parent_cat_id',$parent_cat_id)->get();
    	$key=1;
        $main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
    	return view('backent.child_category.index')->with('child_categories',$child_categories)->with('key',$key)->with('main_cat_id',$main_cat_id)->with('parent_cat_id',$parent_cat_id)->with('main_category_name',$main_category_name)->with('parent_category_name',$parent_category_name);
    }
    public function addChildCategories($main_cat_id,$parent_cat_id){
        $main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
		return view('backent.child_category.add',['child_category'=>null])->with('main_cat_id',$main_cat_id)->with('parent_cat_id',$parent_cat_id)->with('main_category_name',$main_category_name)->with('parent_category_name',$parent_category_name);
	}
	public function postChildCategories(Request $request,$main_cat_id,$parent_cat_id){
    	$this->validate($request,[
    		'child_cateories_name'=>'required'        
        ]);

        $id=$request->id;
        if ($id == 0) {
            $child_category=new ChildCategories();
            $child_category->child_cat_name = json_encode($request->child_cateories_name);
            $child_category->main_cat_id = $main_cat_id;
            $child_category->parent_cat_id=$parent_cat_id;
            if ($child_category->save()) {
              return Redirect::route('all_child_categories',[$main_cat_id,$parent_cat_id])->with('success','You add child categories is successfully');
            }else{
                return Redirect::route('add_child_categories',[$main_cat_id,$parent_cat_id])->with('fails','You add child categories is not successfully');
            }
        }else{
            $child_category=ChildCategories::find($id);
            $child_category->child_cat_name=json_encode($request->child_cateories_name);
            if ($child_category->save()) {
                return Redirect::route('all_child_categories',[$child_category->main_cat_id,$child_category->parent_cat_id])->with('success','You update child categories is successfully');
            }else{
                return Redirect::route('edit_parent_categories',[$child_category->main_cat_id,$child_category->parent_cat_id])->with('success','You update child categories is not successfully');
            }
        }
    }
    public function deleteChildCategories(Request $request,$id){
        $child_category=ChildCategories::find($id);
        $status=$request->input('status');
        $main_category_id=$child_category->main_cat_id;
        $parent_category_id=$child_category->parent_cat_id;

        $products=Products::where('child_cat_id',$id)->get();
        foreach($products as $product) {
            $pro=Products::find($product->id);
            $photos=Photos::where('product_id',$product->id)->get();
            foreach($photos as $ph){
                $ph_id=$ph->id;
                $photo=photos::find($ph_id);
                $photo->status=$status;
                $photo->save();
//                $phImage=$photo->image;
//                $del=File::delete($phImage);
//                $photo->delete();
            }
            $pro->delete();
        }
        $child_category->status=$status;
        if ($child_category->save()) {
            return Redirect::route('all_child_categories',[$main_category_id,$parent_category_id])->with('success','You  '.$status.'  your child categories is successfully!');
        }else{
            return Redirect::route('all_child_categories',[$main_category_id,$parent_category_id])->with('fails','Sorry, You  '.$status.'  child categories is not successfully');
        }
        
    }
    public function editChildCategories($id){
        $child_category=ChildCategories::find($id);
        $parent_cat_id=ChildCategories::where('id',$id)->pluck('parent_cat_id');
        $main_cat_id=ParentCategories::where('id',$parent_cat_id)->pluck('main_cat_id');
        $main_category_name=MainCategories::find($main_cat_id);
        $parent_category_name=ParentCategories::find($parent_cat_id);
        return view('backent.child_category.add',['child_category'=>$child_category])->with('main_cat_id',$main_category_name->id)->with('parent_cat_id',$parent_category_name->id)->with('main_category_name',$main_category_name)->with('parent_category_name',$parent_category_name);
    }
}
