<?php

namespace App\Http\Controllers\Admin;

use App\ParentCategories;
use App\ChildCategories;
use App\Products;
use App\Photos;
use File;
use App\MainCategories;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers;

class ParentCategoriesController extends Controller
{
    public function getParentCategories($main_cat_id){
    	$parent_categories=ParentCategories::where('main_cat_id',$main_cat_id)->get();
    	$key=1;
        $main_category_name=MainCategories::find($main_cat_id);
    	return view('backent.parent_category.index')->with('parent_categories',$parent_categories)->with('key',$key)->with('main_cat_id',$main_cat_id)->with('main_category_name',$main_category_name);
    }
    public function getAddParentCategories($main_cat_id){
        $main_category_name=MainCategories::find($main_cat_id);
		return view('backent.parent_category.add',['parent_category'=>null])->with('main_cat_id',$main_cat_id)->with('main_category_name',$main_category_name);
	}
	public function postParentCategories(Request $request,$main_cat_id){
    	$this->validate($request,[
    		'parent_cateories_name'=>'required'        
        ]);

        $id=$request->id;
        if ($id == 0) {
            $parent_category=new ParentCategories();
            $parent_category->parent_cat_name = json_encode($request->parent_cateories_name);
            $parent_category->main_cat_id = $main_cat_id;
            if ($parent_category->save()) {
              return Redirect::route('all_parent_categories',$main_cat_id)->with('success','You add parent categories is successfully');
            }else{
                return Redirect::route('add_parent_categories',$main_cat_id)->with('fails','You add parent categories is not successfully');
            }
        }else{
            $parent_category=ParentCategories::find($id);
            $parent_category->parent_cat_name=json_encode($request->parent_cateories_name);
            if ($parent_category->save()) {
                return Redirect::route('all_parent_categories',$parent_category->main_cat_id)->with('success','You update parent categories is successfully');
            }else{
                return Redirect::route('edit_parent_categories',$parent_category->main_cat_id)->with('success','You update parent categories is not successfully');
            }
        }
    }
    public function deleteParentCategories(Request $request,$id){
        $parent_category=ParentCategories::find($id);
        $status=$request->input('status');
        $main_category_id=$parent_category->main_cat_id;

        $products=Products::where('parent_cat_id',$id)->get();
        foreach($products as $product) {
            $pro=Products::find($product->id);
            $photos=Photos::where('product_id',$product->id)->get();
            foreach($photos as $ph){
                $ph_id=$ph->id;
                $photo=photos::find($ph_id);
                $photo->status=$status;
                $photo->save();
//                $phImage=$photo->image;
//                $del=File::delete($phImage);
//                $photo->delete();
            }
            $pro->delete();
        }
        $child_category=ChildCategories::where('parent_cat_id',$id)->get();
        foreach($child_category as $c_category){
            $c_category->status=$status;
            $c_category->save();
        }
        $parent_category->status=$status;
        if ($parent_category->save()) {
            return Redirect::route('all_parent_categories',$main_category_id)->with('success','You  '.$status.'  your parent categories is successfully!');
        }else{
            return Redirect::route('all_parent_categories',$main_category_id)->with('fails','Sorry, You  '.$status.'  parent categories is not successfully');
        }
        
    }
    public function getEditParentCategories($id){
        $parent_category=ParentCategories::find($id);
        $main_cat_id=ParentCategories::where('id',$id)->pluck('main_cat_id');
        $main_category_name=MainCategories::find($main_cat_id);
        return view('backent.parent_category.add',['parent_category'=>$parent_category])->with('main_cat_id',$main_cat_id)->with('main_category_name',$main_category_name);
    }
}
