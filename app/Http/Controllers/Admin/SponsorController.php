<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use File;
use Hash;
use App\Sponsor;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers;
class SponsorController extends Controller
{
    public function getSponsor(){
        $sponsors = Sponsor::orderBy('id','=','desc')->get();
        $key=1;
        return view('backent.sponsor.index')->with('sponsors',$sponsors)->with('key',$key);
    }
    public function getAddSponsor(){
        return view('backent.sponsor.add');
    }
    public function postSponsor(Request $request){
        $h=$request->input('height');
        $w=$request->input('width');
        $this->validate($request,[
            'width'=>'required',
            'height'=>'required',
            'price'=>'required',
            'image'=>'required|dimensions:width='.$w.',height='.$h.'',
        ]);
        
        $images=$request->file('image');
        $input= time().'.'.$images->getClientOriginalName();
        $filename='web/images/sponsor/'.$input;
        $images->move('web/images/sponsor', $input);

        $sponsor = new Sponsor();
        $sponsor->width=$request->input('width');
        $sponsor->height=$request->input('height');
        $sponsor->price=$request->input('price');
        $sponsor->image=$filename;
        if ($sponsor->save()) {
            return Redirect::route('all_sponsor')->with('success','Sponsor has created');
        }else{
            return Redirect::route('add_sponsor')->with('success','Sponsor has not created');
        }
    }
    public function getEditSponsor($id){
        $sponsor=Sponsor::find($id);
        return view('backent.sponsor.edit')->with('sponsor',$sponsor);
    }
    public function updateSponsor(Request $request, $id){
        $sponsor=Sponsor::find($id);
        if ($request->hasFile('image')) {
            $h=$request->input('height');
            $w=$request->input('width');
            $this->validate($request,[
                'width'=>'required',
                'height'=>'required',
                'price'=>'required',
                'image'=>'required|dimensions:width='.$w.',height='.$h.'',
            ]);
            $phImage=$sponsor->image;
            $del=File::delete($phImage);

            $images=$request->file('image');
            $input= time().'.'.$images->getClientOriginalName();
            $filename='web/images/sponsor/'.$input;
            $images->move('web/images/sponsor', $input);

            $sponsor->price = $request->input('price');
            $sponsor->width = $request->input('width');
            $sponsor->height=$request->input('height');
            $sponsor->image=$filename;
            if ($sponsor->save()) {
                return Redirect::route('all_sponsor')->with('success','Sponsor has created');
            }else{
                return Redirect::route('edit_sponsor',$id)->with('success','Sponsor has not created');
            }
        }else if(Input::has('width')){
            return "OK";
            $h=$request->input('height');
            $w=$request->input('width');
            $this->validate($request,[
                'width'=>'required',
                'height'=>'required',
                'price'=>'required',
                'image'=>'required|dimensions:width='.$w.',height='.$h.'',
            ]);
            $sponsor->price = $request->input('price');
            $sponsor->width = $request->input('width');
            $sponsor->height= $request->input('height');
            if ($sponsor->save()) {
                return Redirect::route('all_sponsor')->with('success','Sponsor has created');
            }else{
                return Redirect::route('edit_sponsor',$id)->with('success','Sponsor has not created');
            }
        }else{
            return "Fails";
            $sponsor->price = $request->input('price');
            if ($sponsor->save()) {
                return Redirect::route('all_sponsor')->with('success','Sponsor has created');
            }else{
                return Redirect::route('edit_sponsor',$id)->with('success','Sponsor has not created');
            } 
        }
    }
}
