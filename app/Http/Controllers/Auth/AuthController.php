<?php  
namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Hash;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Illuminate\Http\Request;

class AuthController extends Controller
{


    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function postLogin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $auth = Auth::attempt(array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'is_activated'=>'1'
        ));
        if ($auth) {
            return "Dashboard";
        }
        else{
            return Redirect::route('login')->with('warning',"First please active your account.");
        }
    }
    public function webRegister()
    {
        return view('frontent.account.register');
    }

    public function webRegisterPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:password_confirmation',
            'password_confirmation' => 'required',
            'role'=>'required',
            'CaptchaCode' => 'required|valid_captcha'
        ]);

        $contactName=$request->input('name');
        $contactEmail=$request->input('email');

        $user= new User();
        $user->username=$request->input('name');
        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password_confirmation'));
        $user->captcha=$request->input('CaptchaCode');
        if ($user->save()) {
            Mail::send('welcome',['name'=>$contactName, 'email'=>$contactEmail,'link'=>$user->captcha], function($message) use ($user) {
                $message->to($user['email']);
                $message->subject('Site - Activation Code');
            });

            return redirect()->to('login_form')
                ->with('success',"We sent activation code. Please check your mail.");
        }else{

        }
    }
    public function userActivation($token)
    {
        $users = User::where('captcha','=',$token)->where('is_activated','=','0');
        if($users->count()){
          $users = $users->first();
          $users->is_activated ='1';
          $users->captcha = "";
          if($users->save()){
            return redirect()->to('login_form')
                ->with('success',"Your account has been activated.");
          }else{
            return redirect()->to('login_form')
                ->with('warning',"Your account activate not success. Please activate again?");
          }
        }
    }
}