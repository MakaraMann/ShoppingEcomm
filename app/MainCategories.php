<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class MainCategories extends Model
{
    public function parent_category(){
    	return $this->hasMany('App\ParentCategories','main_cat_id');
    }
    public function child_category(){
    	return $this->hasMany('App\ChildCategories','child_cat_id');
    }
    public function product(){
    	return $this->hasMay('App\Products','main_cat_id');
    }
}
