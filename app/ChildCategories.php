<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ChildCategories extends Model
{
    public function category(){
    	return $this->belongsTo('App\MainCategories','main_cat_id');
    }
    public function parent_category(){
    	return $this->belongsTo('App\ParentCategories','parent_cat_id');
    }
    public function product(){
    	return $this->hasMany('App\Products','child_cat_id');
    }
}
