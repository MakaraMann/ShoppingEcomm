<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Products extends Model
{
    public function parent_categories(){
    	return $this->belongsTo('App\ParentCategories','parent_cat_id');
    }
    public function category(){
        return $this->belongsTo('App\Categories','main_cat_id');
    }
    public function child_category(){
        return $this->belongsTo('App\ChildCategories','child_cat_id');
    }
    public function photo(){
    	return $this->hasMany('App\Photos','product_id');
    }
    public function user(){
    	return $this->belongsTo('App\User','user_id');
    }
}

