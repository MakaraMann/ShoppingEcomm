<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ParentCategories extends Model
{
    public function main_category(){
    	return $this->belongsTo('App\MainCategories','main_cat_id');
    }
    public function parent_category(){
    	return $this->hasMany('App\ParentCategories','parent_cat_id');
    }
}
