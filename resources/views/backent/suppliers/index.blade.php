@extends('backent.layout.app')
@section('head')
    @parent
    <title>List suppliers</title>

    <link href="{{asset('assets/plugins/datatables/dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <section class="content-header" style="padding-top: 30px">
        <div class="header-title">
            <ol class="breadcrumb">
                <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
                <li>
                    <a href="#" class="btn btn-success btn-xs" style="color: white">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Create Supplier </a>
                </li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            @if(Session::has('success'))
                <p class="alert alert-success" style="color: white;">{{ Session::get('success') }}  </p>
            @elseif(Session::has('fails'))
                <p class="alert alert-danger" style="color: white;">{{ Session::get('fails') }}  </p>
            @endif
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>List Suppliers</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div >
                            <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr >
                                        <th style="text-align: center;">NAMES</th>
                                        <th style="text-align: center;">CHILD CATEGORIES</th>
                                        <th style="text-align: center;">CODES</th>
                                        <th style="text-align: center;">IMAGES</th>
                                        <th style="text-align: center;">PRICES</th>
                                        <th style="text-align: center;">STOCK IN</th>
                                        <th style="text-align: center;">STOCK OUT</th>
                                        <th style="text-align: center;">OPTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
    <!-- jquery-ui -->
    <script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
    <!-- Pace js -->
    <script src="{{asset('admin/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
    <!-- AdminBD frame -->

    <script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/datatables/dataTables.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            "use strict"; // Start of use strict

            $('#dataTableExample1').DataTable({
                "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
                "lengthMenu": [[6, 25, 50, -1], [6, 25, 50, "All"]],
                "iDisplayLength": 6
            });

            $("#dataTableExample2").DataTable({
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[6, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'excel', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ]
            });

        });
    </script>
@endsection