@extends('backent.layout.app')
@section('head')
	@parent
		<title>All Admin</title>
		
        <link href="{{asset('assets/plugins/datatables/dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
<section class="content-header" style="padding-top: 20px">
    <div class="header-title">
        <ol class="breadcrumb">
            <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
            <li class="active">All Admin</li>
            <li>
            	<a href="{{URL::route('add_admin')}}" class="btn btn-success btn-xs" style="color: white">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    Create Admin </a>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
        @if(Session::has('success'))
        <p class="alert alert-success" style="color: white;">{{ Session::get('success') }}  </p>
        @elseif(Session::has('fails'))
        <p class="alert alert-danger" style="color: white;">{{ Session::get('fails') }}  </p>
        @endif
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>User Admin Management</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div >
                        <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr >
                                    <th style="text-align: center;">No</th>
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Profile</th>
                                    <th style="text-align: center;">Email</th>
                                    <th style="text-align: center;">Role</th>
                                    <th style="text-align: center;">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $key++}}</td>
                                    <td>{{ $admin->username}}</td>
                                    <td style="width: 10%"><img src="{{ asset($admin->profile)}}" alt="" style="width: 100%" class="circle-img"></td>
                                    <td>{{$admin->email}}</td>
                                    <td>{{$admin->role}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{ URL::route('edit_admin',$admin->id)}}" class="btn btn-sm btn-labeled btn-success"><span ><i class="fa fa-pencil" aria-hidden="true"></i></span> Update</a>
                                    </td>
                                </tr>
                                @foreach( $admins as $admin)
                                <tr>
                                    <td>{{ $key++}}</td>
                                    <td>{{ $admin->username}}</td>
                                    <td style="width: 10%"><img src="{{ asset($admin->profile)}}" alt="" style="width: 100%"></td>
                                    <td>{{$admin->email}}</td>
                                    <td>{{$admin->role}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{ URL::route('edit_admin',$admin->id)}}" class="btn btn-sm btn-labeled btn-success"><span ><i class="fa fa-pencil" aria-hidden="true"></i></span> Edit</a>

                                        <a href="{{ URL::route('delete_admin',$admin->id)}}" style="background: #ce1d1d;border: 1px solid #ce1d1d" class="btn btn-sm btn-labeled btn-success"><span ><i class="fa fa-trash-o" aria-hidden="true"></i></span> Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui --> 
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{asset('admin/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- AdminBD frame -->

<script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/datatables/dataTables.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {

        "use strict"; // Start of use strict

        $('#dataTableExample1').DataTable({
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "lengthMenu": [[6, 25, 50, -1], [6, 25, 50, "All"]],
            "iDisplayLength": 6
        });

        $("#dataTableExample2").DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[6, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });

    });
</script>
@endsection