@extends('backent.layout.app')
@section('head')
	@parent
		<title>Edit user admin</title>
		<style>
		input[type="file"] {
		  display: block;
		}
		.imageThumb {
		  max-height: 75px;
		  border: 2px solid;
		  padding: 1px;
		  cursor: pointer;
		}
		.pip {
		  display: inline-block;
		  margin: 10px 10px 0 0;
		}
		.remove {
		  display: block;
		  background: #444;
		  border: 1px solid black;
		  color: white;
		  text-align: center;
		  cursor: pointer;
		}
		.remove:hover {
		  background: white;
		  color: black;
		}
	</style>
@endsection
@section('content')
<section class="content-header" style="padding-top: 20px">
    <div class="header-title">
        <ol class="breadcrumb">
            <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
            <li class="active">Edit user admin</li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
	    <form action="{{URL::route('update_admin',$admin->id)}}" method="post" class="wizard-pane form-bordered" id="wizard-account" novalidate="novalidate" style="display: block;" enctype="multipart/form-data">
			{!! csrf_field() !!}
	        <div class="panel panel-bd lobidrag lobipanel lobipanel-sortable" data-inner-id="fk3VAeV1Nm" data-index="0">
	            <div class="panel-heading ui-sortable-handle">
	                <div class="panel-title" style="max-width: calc(100% - 180px);padding-bottom: 20px;padding-top: 10px;text-align: center;">
	                    <h4>USER ADMIN MANAGEMENTS</h4>
	                </div>
	            	<div class="panel-body">
	            	    <div class="col-xs-12 col-sm-12 col-md-12 m-b-20" style="border: 1px solid #e0e0e0;padding-top: 20px;padding-bottom: 20px">
		                	<div class="col-md-8">
		                        <div class="form-group no-padding-t no-border-t panel-padding-h">
		                            <div class="has-feedback">
		                                <label for="#"><i class="fa fa-pencil" aria-hidden="true"></i> Name <i style="color: red">*</i></label>
		                                <input type="text" class="form-control" name="username" placeholder="Enter username..." value="{{$admin->username}}">
		                            </div>
		                        </div>
		                        <div class="form-group no-padding-t no-border-t panel-padding-h">
		                            <div class="has-feedback">
		                                <label for="#"><i class="fa fa-pencil" aria-hidden="true"></i> Email Address <i style="color: red">*</i></label>
		                                <input type="email" class="form-control" name="email" placeholder="Enter email address..." value="{{$admin->email}}">
		                            </div>
		                        </div>
		                        <div class="form-group no-padding-t no-border-t panel-padding-h">
		                            <div class="has-feedback">
		                                <label for="#"><i class="fa fa-pencil" aria-hidden="true"></i>Old Password <i style="color: red">*</i></label>
		                                <input type="password" class="form-control" name="o_pass" placeholder="Enter old password...">
		                            </div>
		                        </div>
		                        <div class="form-group no-padding-t no-border-t panel-padding-h">
		                            <div class="has-feedback">
		                                <label for="#"><i class="fa fa-pencil" aria-hidden="true"></i> Password <i style="color: red">*</i></label>
		                                <input type="password" class="form-control" name="n_pass" placeholder="Enter new password...">
		                            </div>
		                        </div>
		                        <div class="form-group no-padding-t no-border-t panel-padding-h">
		                            <div class="has-feedback">
		                                <label for="#"><i class="fa fa-pencil" aria-hidden="true"></i> Confirm Password <i style="color: red">*</i></label>
		                                <input type="password" class="form-control" name="c_pass"  placeholder="Enter confirm password...">
		                            </div>
		                        </div>
		                	</div>
		                	<div class="col-md-4">
		                		<div class="col-md-12" style="border: 1px solid #e0e0e0;text-align: center;padding: 5px;margin-top: 22px">
					        		<h4><b>Upload New Profile </b></h4>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
									<div class="field" align="left">
									  <input type="file" id="files" name="profile" />
									</div>
					        	</div>
					        	<div class="col-md-12" style="border-right: 1px solid #e0e0e0;border-left: 1px solid #e0e0e0;border-top: 1px solid #e0e0e0;text-align: center;padding: 5px;margin-top: 40px">
					        		<h4><b>Old Profile</b></h4>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
                                    <div style="padding: 10px">
                                    	<img src="{{asset($admin->profile)}}" alt="" style="
                                            width: 100%">
                                    </div>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;text-align: center;padding: 5px;margin-top: 10px">
					        		<h4><b>Publish <i style="color: red">*</i></b></h4>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
									<button type="submit" class="btn btn-success m-r-2" style="border-radius: 1px"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
									<a href="{{ URL::previous() }}" class="btn btn-warning" style="border-radius: 1px"><i class="fa fa-fast-backward" aria-hidden="true"></i> Cancel</a>
					        	</div>
		                	</div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui --> 
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script>
	$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Hide</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          // $("<img></img>", {
          //   class: "imageThumb",
          //   src: e.target.result,
          //   title: file.name + " | Click to remove"
          // }).insertAfter("#files").click(function(){$(this).remove();});
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
</script>
@endsection