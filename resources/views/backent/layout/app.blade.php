<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from admin.bdtask.com/BdtaskAdmin_v1.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Mar 2017 06:50:58 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ecommerce Project</title>
        <link rel="shortcut icon" href="admin/assets/dist/img/ico/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="admin/assets/dist/img/ico/apple-touch-icon-57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="admin/assets/dist/img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="admin/assets/dist/img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="admin/assets/dist/img/ico/apple-touch-icon-144-precomposed.png">
        <link href="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/plugins/pace/flash.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/plugins/toastr/toastr.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/plugins/emojionearea/emojionearea.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/plugins/monthly/monthly.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/dist/css/styleBD.css')}}" rel="stylesheet" type="text/css"/>


        @yield('head')
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <header class="main-header"> 
                <a href="index-2.html" class="logo">
                    <span class="logo-mini">
                        EADMIN
                    </span>
                    <span class="logo-lg">
                        <h3><b>EADMIN</b></h3>
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="pe-7s-keypad"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown dropdown-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="pe-7s-settings"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="profile.html"><i class="pe-7s-users"></i> {{ Auth::user()->username}}</a></li>
                                    <li><a href="{{URL::route('admin_logout')}}"><i class="pe-7s-key"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <div class="sidebar">
                    <div class="user-panel text-center">
                        <div class="image">
                            <img src="{{URL::to('admin/assets/dist/img/user2-160x160.png')}}" class="img-circle" alt="User Image">
                        </div>
                        <div class="info">
                            <p>Naeem Khan</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> <!-- /.search form -->
                    <!-- sidebar menu -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="active">
                            <a href="{{URL::route('dashboard')}}"><i class="ti-home"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                </span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Categories Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{URL::route('all_main_categories')}}">All Main Categories</a></li>
                                <li><a href="{{URL::route('add_main_categories')}}">New Main Categories</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i> <span>Sponsors Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{URL::route('all_sponsor')}}">All Sponsors</a></li>
                                <li><a href="{{URL::route('add_sponsor')}}">Create Sponsor</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="">
                                <i class="hvr-buzz-out fa fa-user-circle"></i><span>Admin Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{URL::route('all_admin')}}">All Admin</a></li>
                                <li><a href="{{URL::route('add_admin')}}">New Admin</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="">
                                <i class="fa fa-wheelchair-alt" aria-hidden="true"></i><span>Supplier Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{URL::route('all_supplier')}}">All Suppliers</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="">
                                <i class="fa fa-male" aria-hidden="true"></i><span>Customer Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="buttons.html">All Customers</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cog" aria-hidden="true"></i> <span>Settings</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="maps_data.html">Data Maps</a></li>
                                <li><a href="maps_jvector.html">Jvector Maps</a></li>
                                <li><a href="maps_google.html">Google map</a></li>
                                <li><a href="maps_snazzy.html">Snazzy Map</a></li>
                            </ul>
                        </li>
                      
                    </ul>
                </div> <!-- /.sidebar -->
            </aside>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                
                <!-- Main content -->
                @yield('content')
                 <!-- /.content -->
            </div> <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> <b>Version</b> 1.0</div>
                <strong>Copyright &copy; 2016-2017 <a href="#">bdtask</a>.</strong> All rights reserved. <i class="fa fa-heart color-green"></i>
            </footer>
        </div>
        
        <!-- Bootstrap -->
        @yield('script')
        <!-- lobipanel -->
        <!-- SlimScroll -->
        <script src="{{asset('admin/assets/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/plugins/fastclick/fastclick.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/dist/js/frame.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/plugins/toastr/toastr.min.js')}}" type="text/javascript"></script>
        <!-- Sparkline js -->
        <script src="{{asset('admin/assets/plugins/sparkline/sparkline.min.js')}}" type="text/javascript"></script>
        <!-- Data maps js -->
        <script src="{{asset('admin/assets/plugins/datamaps/d3.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/plugins/datamaps/topojson.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/plugins/datamaps/datamaps.all.min.js')}}" type="text/javascript"></script>

        
        <!-- Counter js -->
        <script src="{{asset('admin/assets/plugins/counterup/waypoints.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
        <!-- Emojionearea -->
        <script src="{{asset('admin/assets/plugins/emojionearea/emojionearea.min.js')}}" type="text/javascript"></script>
        <!-- Monthly js -->
        <script src="{{asset('admin/assets/plugins/monthly/monthly.js')}}" type="text/javascript"></script>


        <!-- End Page Lavel Plugins
        =====================================================================-->
        <!-- Start Theme label Script
        =====================================================================-->
        <!-- Dashboard js -->
        <script src="{{asset('admin/assets/dist/js/dashboard.js')}}" type="text/javascript"></script>


        
        <!-- End Theme label Script
        =====================================================================-->
        <script>
            $(document).ready(function () {

                "use strict"; // Start of use strict

             

                //counter
                $('.count-number').counterUp({
                    delay: 10,
                    time: 5000
                });

                //data maps
                var basic_choropleth = new Datamap({
                    element: document.getElementById("map1"),
                    projection: 'mercator',
                    fills: {
                        defaultFill: "#37a000",
                        authorHasTraveledTo: "#fa0fa0"
                    },
                    data: {
                        USA: {fillKey: "authorHasTraveledTo"},
                        JPN: {fillKey: "authorHasTraveledTo"},
                        ITA: {fillKey: "authorHasTraveledTo"},
                        CRI: {fillKey: "authorHasTraveledTo"},
                        KOR: {fillKey: "authorHasTraveledTo"},
                        DEU: {fillKey: "authorHasTraveledTo"}
                    }
                });

                var colors = d3.scale.category10();

                window.setInterval(function () {
                    basic_choropleth.updateChoropleth({
                        USA: colors(Math.random() * 10),
                        RUS: colors(Math.random() * 100),
                        AUS: {fillKey: 'authorHasTraveledTo'},
                        BRA: colors(Math.random() * 50),
                        CAN: colors(Math.random() * 50),
                        ZAF: colors(Math.random() * 50),
                        IND: colors(Math.random() * 50)
                    });
                }, 2000);

                //Chat list
                $('.chat_list').slimScroll({
                    size: '3px',
                    height: '305px'
                });

                // Message
                $('.message_inner').slimScroll({
                    size: '3px',
                    height: '320px'
//                    position: 'left'
                });

                //emojionearea
                $(".emojionearea").emojioneArea({
                    pickerPosition: "top",
                    tonesStyle: "radio"
                });

                //monthly calender
                $('#m_calendar').monthly({
                    mode: 'event',
                    //jsonUrl: 'events.json',
                    //dataType: 'json'
                    xmlUrl: 'events.xml'
                });

                //Sparklines Charts
                $('.sparkline1').sparkline([4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 7, 4, 3, 1, 5, 7, 6, 6, 5, 5, 4, 4, 3, 3, 4, 4, 5], {
                    type: 'bar',
                    barColor: '#37a000',
                    height: '35',
                    barWidth: '3',
                    barSpacing: 2
                });

                $(".sparkline2").sparkline([-8, 2, 4, 3, 5, 4, 3, 5, 5, 6, 3, 9, 7, 3, 5, 6, 9, 5, 6, 7, 2, 3, 9, 6, 6, 7, 8, 10, 15, 16, 17, 15], {
                    type: 'line',
                    height: '35',
                    width: '100%',
                    lineColor: '#37a000',
                    fillColor: '#fff'
                });

                $(".sparkline3").sparkline([2, 5, 3, 7, 5, 10, 3, 6, 5, 7], {
                    type: 'line',
                    height: '35',
                    width: '100%',
                    lineColor: '#37a000',
                    fillColor: '#fff'
                });

                $(".sparkline4").sparkline([10, 34, 13, 33, 35, 24, 32, 24, 52, 35], {
                    type: 'line',
                    height: '35',
                    width: '100%',
                    lineColor: '#37a000',
                    fillColor: 'rgba(55, 160, 0, 0.7)'
                });

                $(".sparkline5").sparkline([4, 2], {
                    type: 'pie',
                    sliceColors: ['#37a000', '#2c3136']
                });

                $(".sparkline6").sparkline([3, 2], {
                    type: 'pie',
                    sliceColors: ['#37a000', '#2c3136']
                });

                $(".sparkline7").sparkline([4, 1], {
                    type: 'pie',
                    sliceColors: ['#37a000', '#2c3136']
                });

                $(".sparkline8").sparkline([1, 3], {
                    type: 'pie',
                    sliceColors: ['#37a000', '#2c3136']
                });

                $(".sparkline9").sparkline([3, 5], {
                    type: 'pie',
                    sliceColors: ['#37a000', '#2c3136']
                });

            });
        </script>
        
        <script src="{{asset('//cdn.ckeditor.com/4.4.3/standard/ckeditor.js')}}"></script>
        <script type="text/javascript">
          $(function() {
                CKEDITOR.replace('editor1');
              });
        </script>
        <script>
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();   
          });
        </script>

    </body>

<!-- Mirrored from admin.bdtask.com/BdtaskAdmin_v1.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Mar 2017 06:52:30 GMT -->
</html>