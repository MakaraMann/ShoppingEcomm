<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Login</title>
        <link rel="shortcut icon" href="admin/assets/dist/img/ico/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="admin/assets/dist/img/ico/apple-touch-icon-57-precomposed.png">
        <link href="{{asset('admin/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <style>
            .row .col-md-offset-4{
                margin-top: 220px;
            }
            .panel {
                background-color: rgba(177, 29, 29, 0.21) !important;
            }
            .btn-success {
                color: #fff;
                background-color: #005075;
                border-color: #4cae4c;
            }
            .btn-success:hover{
                background: #005080 !important;
            }
            .panel-body {
                padding-top: 0px !important;
                padding-left: 44px !important;
                padding-right: 44px !important;
                padding-bottom: 44px !important;
            }
            .panel-default {
                border-color: 0px solid red !important;
            }
            .panel {
                 border: 0px solid transparent !important;
            }
            .panel-title{
                font-size: 25px !important;
            }
            .panel-default>.panel-heading {
                color: #fcf8e3 !important;
                font-size: 25px;
                background-color: transparent !important;
                border-color: transparent !important;
            } 
            .panel-heading{
                padding-top: 40px !important;
                padding-bottom: 30px !important;
            }
            .form-control {
                display: block;
                width: 100%;
                height: 38px;
                padding: 19px 12px !important;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            }
            .error{
                font-size: 12px;
            }
        </style>
    </head>
    <body style="background: url('web/image/background_admin.jpg');min-height: 500px">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center;">
                            <h3 class="panel-title"><b>ADMIN LOGIN</b></h3>
                        </div>
                        <div class="panel-body form-container">
                            <form accept-charset="UTF-8" role="form" action="{{URL::route('post_admin_login')}}" method="post" id="form1">
                                {!! csrf_field() !!}
                                <fieldset>
                                    <!-- @if(count($errors))
                                        <div class="form-group">
                                             <div class="alert alert-danger">
                                            <ul >
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                             </div>
                                        </div>
                                    @endif -->
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <input class="form-control" placeholder="Username" name="username" type="text" style="border-radius: 0px;border:0px;" value="{{ old('username') }}" id="username">
                                        @if ($errors->has('username'))
                                            <span class="help-block" style="color: 
                                            white;font-family: sans-serif">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input class="form-control" placeholder="Password" name="password" type="password" style="border-radius: 0px;border:0px;" value="{{ old('password') }}" id="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block" style="color: 
                                            white;font-family: sans-serif">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="checkbox">
                                        <label style="color: white">
                                            <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                        </label>
                                    </div>
                                    <button class="btn btn-lg btn-success btn-block" type="submit" style="border-radius: 0px;border:0px">Login</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

        <script>
        $(function (){
         $("#form1").validate({
        // Specify the validation rules
        rules: {
            username: {
                required: true,
                minlength: 3,
                maxlength: 6,
            },
            password: {
                required: true,
                maxlength: 8,
                minlength: 3
            }
        },
        
        // Specify the validation error messages
        messages: {
            username: {
               required: "*Please enter a valid username*",
               minlength: "*Your username must be at least 3 characters long*",
               maxlength: "*Your username must be at maxlength 6 characters*"
            },
            password: {
                required: "*Please provide a password*",
                minlength: "*Your password must be at least 3 characters long*",
                maxlength: "*Your password must be at maxlength 6 characters*"
            },
            submitHandler: function (form) { // for demo
             $('#username').focus();
             $('#submit').click(function () {
                 event.preventDefault(); // prevent PageReLoad                                
                 var ValEmail = $('#username').val() === 'admin@admin.com'; // Email Value
                 alert("Email" + ValEmail);
                 var ValPassword = $('#password').val() === 'admin1234'; // Password Value
                 if (ValEmail === true && ValPassword === true) { // if ValEmail & Val ValPass are as above
                     alert('valid!'); // alert valid!
                     window.location.href = "home_page.html";
                     // go to home.html
                 }
                 else {
                     alert('not valid!'); // alert not valid!
                 }
             });
         }
        }

        
    });

  });
  
        </script>
    </body>
</html>