@php
    $id=isset($parent_category)?$parent_category->id:0;
    $parent_category_name=isset($parent_category)?$parent_category->parent_cat_name:'';
@endphp
@extends('backent.layout.app')
@section('head')
	@parent
		<title>Create Parent Categories</title>
@endsection
@section('content')
<section class="content-header" style="padding-top: 20px">
    <div class="header-title">
        <ol class="breadcrumb">
            <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
            <li><a href="{{URL::route('all_main_categories')}}"><i class="pe-7s-home"></i> {{json_decode($main_category_name->main_cat_name)->en}} </a></li>
            <li class="active">Create Parent Category</li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
	    <form data-toggle="validator" method="post" action="{{URL::route('post_parent_categories',$main_cat_id)}}">
			{!! csrf_field() !!}
			<input type="hidden" name="id" value="{{$id}}">
	        <div class="panel panel-bd lobidrag lobipanel lobipanel-sortable" data-inner-id="fk3VAeV1Nm" data-index="0">
	            <div class="panel-heading ui-sortable-handle">
	                <div class="panel-title" style="max-width: calc(100% - 180px);padding-bottom: 20px;padding-top: 10px;text-align: center;">
	                    <h4>Create New Sub Category</h4>
	                </div>
	            	<div class="panel-body">
	            	    <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
	                        <!-- Nav tabs -->
	                        <ul class="nav nav-tabs">
	                            <li class="active"><a href="#eng" data-toggle="tab" aria-expanded="true">English</a></li>
	                            <li class=""><a href="#kh" data-toggle="tab" aria-expanded="false">Khmer</a></li>
	                        </ul>
	                        <!-- Tab panels -->
	                        @if($parent_category == null)
		                        <div class="tab-content">
		                            <div class="tab-pane fade active in" id="eng">
		                                <div class="panel-body">
		                                    <dov class="row">
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    	<div class="col-md-6">
							                        <div class="form-group">
							                            <label for="inputName" class="control-label">Parent Categories <i style="color: red">*</i></label>
							                            <input type="text" class="form-control" name="parent_cateories_name[en]" placeholder="Parent categories name">
							                        </div>
						                    	</div>
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    </dov>
		                                </div>
		                            </div>
		                            <div class="tab-pane fade" id="kh">
		                                <div class="panel-body">
		                                    <dov class="row">
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    	<div class="col-md-6">
							                        <div class="form-group">
							                            <label for="inputName" class="control-label">ប្រភេទបន្ទាប់ <i style="color: red">*</i></label>
							                            <input type="text" class="form-control" name="parent_cateories_name[kh]" placeholder=" បញ្ចូលឈ្មោះប្រភេទបន្ទាប់">
							                        </div>
						                    	</div>
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    </dov>
		                                </div>
		                            </div>
		                        </div>
	                        @else
		                        <div class="tab-content">
		                            <div class="tab-pane fade active in" id="eng">
		                                <div class="panel-body">
		                                    <dov class="row">
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    	<div class="col-md-6">
							                        <div class="form-group">
							                            <label for="inputName" class="control-label">Parent Categories <i style="color: red">*</i></label>
							                            <input type="text" class="form-control" name="parent_cateories_name[en]" placeholder="Parent categories name" value="{{json_decode($parent_category_name)->en}}">
							                        </div>
						                    	</div>
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    </dov>
		                                </div>
		                            </div>
		                            <div class="tab-pane fade" id="kh">
		                                <div class="panel-body">
		                                    <dov class="row">
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    	<div class="col-md-6">
							                        <div class="form-group">
							                            <label for="inputName" class="control-label"> ប្រភេទបន្ទាប់ <i style="color: red">*</i></label>
							                            <input type="text" class="form-control" name="parent_cateories_name[kh]" placeholder=" បញ្ចូលឈ្មោះប្រភេទបន្ទាប់" value="{{json_decode($parent_category_name)->kh}}">
							                        </div>
						                    	</div>
						                    	<div class="col-md-3">
						                    		
						                    	</div>
						                    </dov>
		                                </div>
		                            </div>
		                        </div>
	                        @endif
	                    </div>
	                    <div class="col-md-12">
							@if($parent_category == null)
								<button type="submit" class="btn btn-success m-r-2" style="border-radius: 1px"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
								<a href="{{ URL::previous() }}" class="btn btn-warning" style="border-radius: 1px"><i class="fa fa-fast-backward" aria-hidden="true"></i> Cancel</a>
							@else
								<button type="submit" class="btn btn-success m-r-2" style="border-radius: 1px"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
								<a href="{{ URL::previous() }}" class="btn btn-warning" style="border-radius: 1px"><i class="fa fa-fast-backward" aria-hidden="true"></i> Cancel</a>
							@endif
			        	</div>
	                </div>
	            </div>
	        </div>
	    </form>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui --> 
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
@endsection