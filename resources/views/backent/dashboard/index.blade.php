@extends('backent.layout.app')
@section('head')
	@parent
		<title>Deshboard</title>
@endsection
@section('content')
<section class="content-header">
    <div class="header-title">
        <ol class="breadcrumb">
            <li class="active"><i class="pe-7s-home"></i> Dashboard</li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2><span class="count-number">206</span> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> +28%</span></h2>
                        <div class="small">% New Sessions</div>
                        <div class="sparkline1 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2><span class="count-number">321</span> <span class="slight"><i class="fa fa-play fa-rotate-90 c-white"> </i> +10%</span> </h2>
                        <div class="small">Total visitors</div>
                        <div class="sparkline2 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2><span class="count-number">789</span> <span class="slight"><i class="fa fa-play fa-rotate-270 text-warning"> </i> +29%</span></h2>
                        <div class="small">Total users</div>
                        <div class="sparkline3 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2><span class="count-number">171</span><span class="slight"><i class="fa fa-play fa-rotate-90 c-white"> </i> +24%</span></h2>
                        <div class="small">Bounce Rate</div>
                        <div class="sparkline4 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- datamap -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 ">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Top 5 countries Azimuth users</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="map1"></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Messages</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="message_inner">
                        <div class="message_widgets">
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Naeem Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a> 
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar2.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Sala Uddin</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status away pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar3.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Mozammel Hoque</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status busy pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar4.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Tanzil Ahmed</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status offline pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar5.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Amir Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="admin/assets/dist/img/avatar.png" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Salman Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
@endsection