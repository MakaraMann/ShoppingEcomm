@php
    $id=isset($product)?$product->id:0;
    $p_name=isset($product)?$product->product_name:'';
    $p_stock=isset($product)?$product->product_stock_in:'';
    $p_price=isset($product)?$product->product_price:'';
    $p_code=isset($product)?$product->product_code:'';
    $p_discount=isset($product)?$product->product_discount:'';
    $p_description=isset($product)?$product->product_description:'';
@endphp
@extends('backent.layout.app')
@section('head')
	@parent
		<title>Create Product</title>
	<link rel="stylesheet" href="{{asset('web/dist/imageuploadify.min.css')}}">
	<style>
		input[type="file"] {
		  display: block;
		}
		.imageThumb {
		  max-height: 75px;
		  border: 2px solid;
		  padding: 1px;
		  cursor: pointer;
		}
		.pip {
		  display: inline-block;
		  margin: 10px 10px 0 0;
		}
		.remove {
		  display: block;
		  background: #444;
		  border: 1px solid black;
		  color: white;
		  text-align: center;
		  cursor: pointer;
		}
		.remove:hover {
		  background: white;
		  color: black;
		}
		 #upload_button {
			 display: inline-block;
		 }
		#upload_button input[type=file] {
			display:none;
		}
		.imageuploadify-container img{
			width: 20px;
			height: 20px;
		}
		.imageuploadify-message{
			display: none;
		}
	</style>
@endsection
@section('content')
<section class="content-header" style="padding-top: 20px">
    <div class="header-title">
        <ol class="breadcrumb">
            <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
            <li><a href="{{URL::route('all_main_categories')}}"><i class="pe-7s-home"></i> {{json_decode(App\MainCategories::find($main_cat_id)->main_cat_name)->en}}</a></li>
            <li><a href="{{URL::route('all_parent_categories',$main_cat_id)}}"><i class="pe-7s-home"></i> {{json_decode(App\ParentCategories::find($parent_cat_id)->parent_cat_name)->en}}</a></li>
            <li><a href="{{URL::route('all_child_categories',array($main_cat_id,$child_cat_id))}}"><i class="pe-7s-home"></i> {{json_decode(App\ChildCategories::find($child_cat_id)->child_cat_name)->en}}</a></li>
            <li class="active">Create New Product</li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="panel panel-bd lobidrag lobipanel lobipanel-sortable" data-inner-id="fk3VAeV1Nm" data-index="0">
        	<div class="panel-heading ui-sortable-handle">
        	    <div class="panel-title" style="max-width: calc(100% - 180px);padding-bottom: 20px;padding-top: 10px;text-align: center;">
                    <h4>Create New Product</h4>
                </div>

                <form data-toggle="validator" method="post" action="{{URL::route('post_product',array($main_cat_id,$parent_cat_id,$child_cat_id))}}" enctype="multipart/form-data">
					{!! csrf_field() !!}
				    <input type="hidden" name="id" value="{{$id}}">
			        <div class="col-md-12">
			        	<div class="col-md-8" style="padding-right: 2px;padding-left: 2px">
			            	<!-- Nav tabs -->
	                        <ul class="nav nav-tabs">
	                            <li class="active"><a href="#eng" data-toggle="tab" aria-expanded="true">English</a></li>
	                            <li class=""><a href="#kh" data-toggle="tab" aria-expanded="false">Khmer</a></li>
	                        </ul>
	                        <!-- Tab panels -->
	                        @if($product == null)
		                        <div class="tab-content">
		                            <div class="tab-pane fade active in" id="eng">
		                                <div class="panel-body">
		                                    <div class="col-md-12">
						                    	<div class="form-group">
						                            <label for="inputName" class="control-label">Product Name <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_name[en]" placeholder="Enter product name">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">Product Code <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_code[en]" placeholder="Enter product code">
						                        </div>
						                        
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">Product Description <i style="color: red">*</i></label>
						                           <textarea class="ckeditor"  style="width: 100%;min-height: 200px" name="description[en]"></textarea>
						                        </div>
						                    </div>
		                                </div>
		                            </div>
		                            <div class="tab-pane fade" id="kh">
		                                <div class="panel-body">
		                                    <div class="col-md-12">
						                    	<div class="form-group">
						                            <label for="inputName" class="control-label">ឈ្មោះផលិតផល <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_name[kh]" placeholder="បញ្ចូលឈ្មោះផលិតផល">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">លេខកូដផលិតផល <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_code[kh]" placeholder="បញ្ចូលលេខកូដផលិតផល">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">ពណ៏នាពីផលិតផល<i style="color: red">*</i></label>
						                           <textarea class="ckeditor" style="width: 100%;min-height: 200px" name="description[kh]"></textarea>
						                        </div>
						                    </div>
		                                </div>
		                            </div>
		                            <div style="padding: 5px;margin-top: 10px;margin-bottom: 20px">
		                            	<div class="form-group">
				                            <label for="inputName" class="control-label">Product Stock in <i style="color: red">*</i></label>
				                            <input type="text" class="form-control" name="p_stock" placeholder="Enter product stock">
				                        </div>
										<div class="form-group">
											<label for="inputName" class="control-label">Product Discount <i style="color: red">*</i></label>
											<input type="text" class="form-control" name="p_discount" placeholder="Enter product discount" value="{{$p_discount}}">
										</div>
				                        <div class="form-group">
				                            <label for="inputName" class="control-label">Product Price <i style="color: red">*</i></label>
				                            <input type="text" class="form-control" name="p_price" placeholder="Enter product price">
				                        </div>
		                            </div>
		                        </div>
	                        @else
		                        <div class="tab-content">
		                            <div class="tab-pane fade active in" id="eng">
		                                <div class="panel-body">
		                                    <div class="col-md-12">
						                    	<div class="form-group">
						                            <label for="inputName" class="control-label">Product Name <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_name[en]" placeholder="Enter product name" value="{{json_decode($p_name)->en}}">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">Product Code <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_code[en]" placeholder="Enter product code" value="{{json_decode($p_code)->en}}">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">Product Description <i style="color: red">*</i></label>
						                           <textarea class="ckeditor" value="{{$p_description}}" style="width: 100%;min-height: 200px" name="description[en]" >{{json_decode($p_description)->en}}</textarea>
						                        </div>
						                        
						                    </div>
		                                </div>
		                            </div>
		                            <div class="tab-pane fade" id="kh">
		                                <div class="panel-body">
		                                    <div class="col-md-12">
						                    	<div class="form-group">
						                            <label for="inputName" class="control-label">ឈ្មោះផលិតផល <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_name[kh]" placeholder="បញ្ចូលឈ្មោះផលិតផល" value="{{json_decode($p_name)->kh}}">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">លេខកូដផលិតផល <i style="color: red">*</i></label>
						                            <input type="text" class="form-control" name="p_code[kh]" placeholder="បញ្ចូលលេខកូដផលិតផល" value="{{json_decode($p_code)->kh}}">
						                        </div>
						                        <div class="form-group">
						                            <label for="inputName" class="control-label">ពណ៏នាពីផលិតផល<i style="color: red">*</i></label>
						                           <textarea class="ckeditor" value="{{$p_description}}" style="width: 100%;min-height: 200px" name="description[kh]">{{json_decode($p_description)->kh}}</textarea>
						                        </div>
						                    </div>
		                                </div>
		                            </div>
		                            <div style="padding: 5px;margin-top: 10px;margin-bottom: 20px">
		                            	<div class="form-group">
				                            <label for="inputName" class="control-label">Product Stock in <i style="color: red">*</i></label>
				                            <input type="text" class="form-control" name="p_stock" placeholder="Enter product stock" value="{{$p_stock}}">
				                        </div>
										<div class="form-group">
											<label for="inputName" class="control-label">Product Discount <i style="color: red">*</i></label>
											<input type="text" class="form-control" name="p_discount" placeholder="Enter product discount" value="{{$p_discount}}">
										</div>
				                        <div class="form-group">
				                            <label for="inputName" class="control-label">Product Price <i style="color: red">*</i></label>
				                            <input type="text" class="form-control" name="p_price" placeholder="Enter product price" value="{{$p_price}}">
				                        </div>
		                            </div>
		                        </div>
	                        @endif
				        </div>
				        <div class="col-md-4" style="padding-right: 2px;padding-left: 2px">
				        	<div class="col-md-12" style="border: 1px solid #e0e0e0;text-align: center;padding: 5px;margin-top: 40px">
				        		<h4><b>Publish</b></h4>
				        	</div>
				        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
								@if($product == null)
									<button type="submit" class="btn btn-primary m-r-2" style="border-radius: 1px"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
									<a href="{{ URL::previous() }}" class="btn btn-warning" style="border-radius: 1px"><i class="fa fa-fast-backward" aria-hidden="true"></i> Cancel</a>
								@else
									<button type="submit" class="btn btn-danger m-r-2" style="border-radius: 1px"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
									<a href="{{ URL::previous() }}" class="btn btn-warning" style="border-radius: 1px"><i class="fa fa-fast-backward" aria-hidden="true"></i> Cancel</a>
								@endif
				        	</div>
                            @if($photos == null)
	                        <div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 5px;margin-top: 40px">
				        		<h4><b>Product image (Size should be 600 x 600 pixels)</b></h4>
				        	</div>
				        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
								<div class="field" align="left">
								  <input type="file" id="files" name="p_images"  />
								</div>
				        	</div>
							<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 5px;margin-top: 40px">
								<h4><b>Product Gallery (Size should be 600 x 600 pixels)</b></h4>
							</div>
							<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
								<div class="field" align="left">
									<input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple name="product_gallery">
								</div>
							</div>
	                        @else
	                            <div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 5px;margin-top: 40px">
					        		<h4><b><h4><b>New Product image (Size should be 600 x 600 pixels)</b></h4></b></h4>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
									<div class="field" align="left">
									  <input type="file" id="files" name="p_images[]" multiple />
									</div>
					        	</div>
					        	<div class="col-md-12" style="border-right: 1px solid #e0e0e0;border-left: 1px solid #e0e0e0;border-top: 1px solid #e0e0e0;text-align: center;padding: 5px;margin-top: 40px">
					        		<h4><b>Old Image</b></h4>
					        	</div>
					        	<div class="col-md-12" style="border: 1px solid #e0e0e0;padding: 30px">
									@foreach($photos as $ph)
                                        <div class="col-md-6" style="padding-right: 1px;padding-left: 1px">
                                            <img src="{{asset($ph->image)}}" alt="" style="
                                            width: 100%">
                                        </div>
                                    @endforeach
					        	</div>
	                        @endif
				        </div>
			        </div>
			    </form>
		    </div>
	    </div>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui --> 
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('web/dist/imageuploadify.min.js')}}"></script>
<script>
	$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"/>" +
            "<br/><span class=\"remove\">Hide</span>" +/
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          // $("<img></img>", {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       8nnnnjjnjn nnn
          //   class: "imageThumb",
          //   src: e.target.result,
          //   title: file.name + " | Click to remove"
          // }).insertAfter("#files").click(function(){$(this).remove();});
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="file"]').imageuploadify();
    })
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
@endsection