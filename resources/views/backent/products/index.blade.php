@extends('backent.layout.app')
@section('head')
	@parent
		<title>All Products</title>
		
        <link href="{{asset('assets/plugins/datatables/dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
<section class="content-header" style="padding-top: 30px">
    <div class="header-title">
        <ol class="breadcrumb">
            <li><a href="{{URL::route('dashboard')}}"><i class="pe-7s-home"></i> Dashboard</a></li>
            <li><a href="{{URL::route('all_main_categories')}}"><i class="pe-7s-home"></i> {{json_decode(App\MainCategories::find($main_cat_id)->main_cat_name)->en}}</a></li>
            <li><a href="{{URL::route('all_parent_categories',$main_cat_id)}}"><i class="pe-7s-home"></i> {{json_decode(App\ParentCategories::find($parent_cat_id)->parent_cat_name)->en}}</a></li>
            <li><a href="{{URL::route('all_child_categories',array($main_cat_id,$child_cat_id))}}"><i class="pe-7s-home"></i> {{json_decode(App\ChildCategories::find($child_cat_id)->child_cat_name)->en}}</a></li>
            <li>
                <a href="{{URL::route('add_product',array($main_cat_id,$parent_cat_id,$child_cat_id))}}" class="btn btn-success btn-xs" style="color: white">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    Create Product </a>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="row">
        @if(Session::has('success'))
        <p class="alert alert-success" style="color: white;">{{ Session::get('success') }}  </p>
        @elseif(Session::has('fails'))
        <p class="alert alert-danger" style="color: white;">{{ Session::get('fails') }}  </p>
        @endif
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>{{json_decode($child_category_name->child_cat_name)->en}}</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div >
                        <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr >
                                    <th style="text-align: center;">NAMES</th>
                                    <th style="text-align: center;">CHILD CATEGORIES</th>
                                    <th style="text-align: center;">CODES</th>
                                    <th style="text-align: center;">IMAGES</th>
                                    <th style="text-align: center;">PRICES</th>
                                    <th style="text-align: center;">STOCK IN</th>
                                    <th style="text-align: center;">STOCK OUT</th>
                                    <th style="text-align: center;">OPTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td style="width: 10%">{{json_decode($product->product_name)->en}}</td>
                                        <td style="width: 15%">{{json_decode($product->child_category->child_cat_name)->en}}</td>
                                        <td style="width: 10%">{{json_decode($product->product_code)->en}}</td>
                                        <td style="width: 4%"><img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}" alt="" width="100%"></td>
                                        <td style="width: 10%">{{$product->product_price}}</td>
                                        <td style="width: 10%">{{$product->product_stock_in}}</td>
                                        <td style="width: 10%">{{$product->product_stock_out}}</td>
                                        <td style="width: 15%;text-align: center;">
                                            <form action="{{URL::route('delete_product',$product->id)}}" method="post">
                                                {!! csrf_field() !!}

                                                @if($product->status =='disabled')
                                                    <input type="text" name="status" value="enable" class="hidden">
                                                    <button type="submit" class="btn btn-danger btn-xs my_delete" style="border:none;border-radius: none"><i class="fa fa-trash" aria-hidden="true"></i> Enable</button>
                                                @else($product->status =='enable')
                                                    <a href="{{URL::route('edit_product',$product->id)}}" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square" aria-hidden="true"></i> Edit</a>
                                                    <input type="text" name="status" value="disabled" class="hidden">
                                                    <button type="submit" class="btn btn-danger btn-xs my_delete" style="border:none;border-radius: none"><i class="fa fa-trash" aria-hidden="true"></i> Disabled</button>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('admin/assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui --> 
<script src="{{asset('admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{asset('admin/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- AdminBD frame -->

<script src="{{asset('admin/assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/datatables/dataTables.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {

        "use strict"; // Start of use strict

        $('#dataTableExample1').DataTable({
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "lengthMenu": [[6, 25, 50, -1], [6, 25, 50, "All"]],
            "iDisplayLength": 6
        });

        $("#dataTableExample2").DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[6, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'excel', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });

    });
</script>
@endsection