@extends('frontent.layout.app')
@section('head')
    @parent
    <link rel="stylesheet" href="{{asset('web/css/login.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/savanihd/multi-select-autocomplete/master/style.css"/>
@endsection
@section('content')
    <div class="container" style="margin-bottom: 40px">
        <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">
        <nav class="woocommerce-breadcrumb">
            <a href="https://angkorstores.com/">Home</a>
            <span class="delimiter"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            Register
        </nav>
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <article id="post-8" class="hentry">
                    <div class="entry-content">
                        <div class="woocommerce">
                            <div class="customer-login-form">
                                <span class="or-text">or</span>
                                <div class="col2-set" id="customer_login">
                                    <div class="col-1">
                                        <h2>Register</h2>
                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/web-register') }}">
                                            {!! csrf_field() !!}
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email address">

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="password" placeholder="Enter password">

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Confirm Password</label>

                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="password_confirmation"  placeholder="Enter password confirm">

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label" style="padding-top: 0px !important;">Supplier</label>
                                                <div class="col-md-1">
                                                    <input type="radio" class="form-control" name="role" value="supplier" style="width: 15px;height: 15px;cursor: pointer">

                                                </div>
                                                <label class="col-md-1 control-label" style="padding-top: 0px !important;">Buyer</label>
                                                <div class="col-md-6">
                                                    <input type="radio" class="form-control" name="role" value="buyer" style="width: 15px;height: 15px;cursor: pointer">
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('CaptchaCode') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Captcha</label>

                                                <div class="col-md-6">
                                                    {!! captcha_image_html('ContactCaptcha') !!}
                                                    <input class="form-control" type="text" id="CaptchaCode" name="CaptchaCode" style="margin-top:5px;" placeholder="Captcha number">

                                                    @if ($errors->has('CaptchaCode'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('CaptchaCode') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <br/>
                                                    <button type="submit" class="btn btn-primary" style="border-radius: 20px;background: #f44336">
                                                        <i class="fa fa-btn fa-user"></i>Register
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- .col-1 -->
                                    <div class="col-2">
                                        <h2>Authentication</h2>
                                        <form method="post" class="register">
                                            <p class="before-register-text">
                                                Register with us for future convenience:
                                            </p>
                                            <p class="form-row">
                                                <a href="{{URL::route('login')}}" name="register" style="color:blue ">
                                                    Login Now</a>
                                            </p>
                                            <div class="register-benefits">
                                                <h3>Register and save time!</h3>
                                                <h3>Register with us for future convenience:</h3>
                                                <ul>
                                                    <li>  Fast and easy check out</li>
                                                    <li> Easy access to your order history and status</li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div><!-- .col-2 -->
                                </div><!-- .col2-set -->
                            </div><!-- /.customer-login-form -->
                        </div><!-- .woocommerce -->
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main><!-- #main -->
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script type="text/javascript">
        $(function(){
            var availableTags = [
                "Laravel",
                "Bootstrap",
                "Server",
                "JavaScript",
                "JQuery",
                "Perl",
                "PHP",
                "Python",
                "Ruby",
                "API",
                "Scheme"
            ];
            $('#myAutocompleteMultiple').autocomplete({
                source: availableTags,
                multiselect: true
            });
        });
    </script>
@endsection