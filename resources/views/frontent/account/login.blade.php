@extends('frontent.layout.app')
@section('head')
    @parent
    <title>Login</title>
    <link rel="stylesheet" href="{{asset('web/css/login.css')}}">
@endsection
@section('content')
    <div class="container" style="margin-bottom: 40px">
        <nav class="woocommerce-breadcrumb">
            <a href="https://angkorstores.com/">Home</a>
            <span class="delimiter"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            Login
        </nav>
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <article id="post-8" class="hentry">
                    <div class="entry-content">
                        <div class="woocommerce">
                            <div class="customer-login-form">
                                <span class="or-text">or</span>
                                <div class="col2-set" id="customer_login">
                                    <div class="col-1">
                                        <h2>Login</h2>
                                        <form action="{{URL::route('store_login')}}" method="post" accept-charset="utf-8">
                                            {!! csrf_field() !!}
                                            <p class="before-login-text">
                                                Welcome back! Sign in to your account
                                            </p>
                                            @if ($message = Session::get('success'))
                                                <div class="alert alert-success">
                                                    <p>{{ $message }}</p>
                                                </div>
                                            @endif

                                            @if ($message = Session::get('warning'))
                                                <div class="alert alert-warning">
                                                    <p>{{ $message }}</p>
                                                </div>
                                            @endif
                                            <p class="form-row form-row-wide has-error{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="username">E-mail <span class="required">*</span></label>
                                                <input type="text" name="email" value="{{ old('email') }}"
                                                       class="form-control form-control-solid placeholder-no-fix"
                                                       id="username" placeholder="Email" autocomplete="off" >
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </p>
                                            <p class="form-row form-row-wide has-error{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Password <span class="required">*</span></label>
                                                <input type="password" name="password" value="{{ old('password') }}" class="form-control form-control-solid placeholder-no-fix" id="password" placeholder="Password" autocomplete="off">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </p>
                                            <p class="form-row">
                                                <input class="button" type="submit" value="Login" name="login">
                                            </p>
                                            <p class="lost_password">
                                                <!-- <a href="login-and-register.html">Lost your password?</a> -->
                                                <a href="https://angkorstores.com/admin/register/reset_pwd">Lost your
                                                    password?</a>
                                            </p>
                                        </form>
                                    </div><!-- .col-1 -->
                                    <div class="col-2">
                                        <h2>Register</h2>
                                        <form method="post" class="register">
                                            <p class="before-register-text">
                                                Create a new account
                                            </p>
                                            <p class="form-row">
                                                <a href="{{URL::route('get_register')}}" style="color:blue ">Register
                                                    Now</a>
                                            </p>
                                            <div class="register-benefits">
                                                <h3>Sign up today and you will be able to :</h3>
                                                <ul>
                                                    <li> Speed your way through checkout</li>
                                                    <li> Track your orders easily</li>
                                                    <li> Keep a record of all your purchases</li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div><!-- .col-2 -->
                                </div><!-- .col2-set -->
                            </div><!-- /.customer-login-form -->
                        </div><!-- .woocommerce -->
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main><!-- #main -->
        </div>
    </div>
@endsection