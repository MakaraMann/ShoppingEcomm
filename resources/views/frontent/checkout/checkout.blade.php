@extends('frontent.layout.app')
@section('head')
    <link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}">

    @parent
        <title>CheckOut</title>
    <link rel="stylesheet" href="{{asset('web/css/checkout.css')}}">
@endsection

@section('content')
    <div class="container" style="margin-bottom: 60px">
        <div class="row" style="padding-top:20px;padding-bottom: 20px">
            <ol>
                <li style="float: left;padding-left: 20px"><a href="{{URL::route('home')}}">HOME</a></li>
                <li style="float: left;padding-left: 20px;font-size: 16px;padding-top:3px">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </li>
                <li style="float: left;padding-left: 20px">CHECKOUT</li>
            </ol>
        </div>
        <div class="row page type-page status-publish hentry">
            <header class="entry-header"><h1 itemprop="name" class="entry-title">Checkout</h1></header>
        </div>
        <div class="row">
            <div class="woocommerce-info">Returning customer? <a href="#" class="showlogin">Click here to login</a></div>
        </div>
        <form action="#" class="checkout woocommerce-checkout" method="post" name="checkout">
            <div class="row" style="margin-top: 10px">
                <div class="col-sm-7">
                    <h3>Shipping information</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">First Name</label>
                                    <input type="text" name="billing_first_name" value="" class="form-control" id="first_name" placeholder="Enter your first name">
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" name="billing_last_name" value="" class="form-control" id="last_name" placeholder="Enter your last name">
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Email Address</label>
                                    <input type="text" name="billing_email" value="" class="form-control" id="billing_email" placeholder="Enter your emai address">
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Mobile Phone</label>
                                    <input type="text" name="billing_phone" value="" class="form-control" id="billing_phone" placeholder="Enter your phone number">
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Country/Region</label>
                                    <select name="country" class="form-control">
                                        <option selected="selected" value="">--Please Select--</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="US">United States</option>
                                        <option value="ES">Spain</option>
                                        <option value="FR">France</option>
                                        <option value="UK">United Kingdom</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IL">Israel</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="CA">Canada</option>
                                        <option value="IT">Italy</option>
                                        <option value="CL">Chile</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="PL">Poland</option>
                                        <option value="AU">Australia</option>
                                        <option value="DE">Germany</option>
                                        <option value="BE">Belgium</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">State/Province/Region:</label>
                                    <select name="province" class="form-control">
                                        <option selected="selected" value="">--Please Select--</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="US">United States</option>
                                        <option value="ES">Spain</option>
                                        <option value="FR">France</option>
                                        <option value="UK">United Kingdom</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IL">Israel</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="CA">Canada</option>
                                        <option value="IT">Italy</option>
                                        <option value="CL">Chile</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="PL">Poland</option>
                                        <option value="AU">Australia</option>
                                        <option value="DE">Germany</option>
                                        <option value="BE">Belgium</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Address</label>
                                    <input type="text" name="billing_address_1" value="" class="form-control" id="billing_address_1" placeholder="Enter your address">
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">City</label>
                                    <p id="calc_shipping_state_field" class="form-row form-row-wide validate-required">
                                        <input type="text" name="billing_city" value="" class="form-control" id="billing_city" placeholder="Enter your city">
                                    </p>
                                    <font color="red"><font color="red"></font></font>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="postal-code">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Postcode / ZIP</label>
                                    <input type="text" name="billing_postcode" value="" class="form-control" placeholder="Enter your postcode / zip">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5" style="background:#f5f5f5;padding-bottom: 20px" id="checkout_sidebar">
                    <h3 id="order_review_heading" style="padding: 10px;">Your order</h3>
                    <div class="woocommerce-checkout-review-order" id="order_review">
                        <table class="w3-table w3-bordered">
                            <tr>
                                <td style="padding-top: 10px;padding-bottom: 10px"><b>Product</b></td>
                                <td style="padding-top: 10px;padding-bottom: 10px"><b>Total</b></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;padding-bottom: 10px"><b>Sub Total</b></td>
                                <td style="padding-top: 10px;padding-bottom: 10px">0.00</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;padding-bottom: 10px"><b>Shipping</b></td>
                                <td style="padding-top: 10px;padding-bottom: 10px">No Shipping</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;padding-bottom: 10px"><b>Total</b></td>
                                <td style="padding-top: 10px;padding-bottom: 10px">0.00</td>
                            </tr>
                        </table>
                        <div class="woocommerce-checkout-payment" id="payment">
                            <ul class="wc_payment_methods payment_methods methods">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td width="30px"><input style="margin: 14px 0 12px 0;" type="radio" value="1" name="payment_method" class="payment_method_cheque"></td>
                                            <td><label style="margin-top: 1.1em;" for="payment_method_cheque">Wing</label></td>
                                            <td><img src="https://angkorstores.com/images/wing.png" height="30" style="margin-top:0.4em"></td>
                                        </tr>
                                        <tr>
                                            <td width="30px"><input style="margin: 14px 0 12px 0;" type="radio" value="2" name="payment_method" class="payment_method_cheque"></td>
                                            <td><label style="margin-top: 1.1em;" for="payment_method_cheque">CanaPay</label></td>
                                            <td><img src="https://angkorstores.com/images/visa&amp;master.jpg" height="30" style="margin-top: 0.4em"></td>
                                        </tr>
                                    </tbody>
                                </table>														<font color="red"><font color="red"></font></font>
                            </ul>
                            <div class="form-row place-order" style="margin-top: 15px">
                                <p class="form-row terms wc-terms-and-conditions">
                                    <input type="checkbox" value="terms" id="terms" name="terms" class="input-checkbox">
                                    <label class="checkbox" for="terms">I’ve read and accepted the <a target="_blank" href="https://angkorstores.com/p/terms-con">terms &amp; conditions</a> <span class="required">*</span></label>
                                    <font color="red"><font color="red"></font></font>
                                    <input type="hidden" value="1" name="terms-field">
                                </p>
                                <input type="submit" name="place_order" id="btn-order" data-value="Place order" value="Place order" class="button alt">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection