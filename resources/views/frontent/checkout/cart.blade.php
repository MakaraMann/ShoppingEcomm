@extends('frontent.layout.app')
@section('head')
    @parent
        <title>Cart</title>
    <link rel="stylesheet" href="{{asset('web/css/cart.css')}}">
    @endsection
@section('content')

    <div class="container">
        <br>
        <div class="row" style="padding-top:20px;padding-bottom: 20px">
            <ol>
                <li style="float: left;padding-left: 20px"><a href="{{URL::route('home')}}">HOME</a></li>
                <li style="float: left;padding-left: 20px;font-size: 16px;padding-top:3px">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </li>
                <li style="float: left;padding-left: 20px">LIST CARTS</li>
            </ol>
        </div>
        <div class="row">
            <div id="content" class="col-sm-12 table-responsive" style="padding-left: 0px;padding-right: 0px">
                <div style="padding: 20px">
                    <h1 style="text-align: center">Your cart</h1>
                </div>
                <table class="table table-striped table-responsive table-cart">
                    <thead>
                    <tr>
                        <th style="width:30%">Product Image</th>
                        <th style="width:15%">Price</th>
                        <th style="width:20%">Quantity</th>
                        <th style="width:15%">Unit Price</th>
                        <th style="width:20%">Total</th>
                    </tr>
                    </thead>
                    <tbody class="shopping">
                        <tr>
                        <td><a href="menu_single.html">
                                <img src="http://ppcoffeecorner.com/images/items/1482227574.jpg" alt=""> Lor Bak Png滷肉飯 </a>
                        </td>
                        <td>3 $</td>
                        <td><!-- input group minus & plus-->
                            <div class="input-group plus-minus">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="minus" data-price="3" data-field="quant[1]" data-rowidy="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding: 7px"><span class="fa fa-minus"></span>
                                    </button>
                                </span>
                                <input type="number" name="quant[1]" class="form-control input-number" value="1" min="1" max="999" id="qty[1]" style="background: white;border: 1px solid #e8e8e8;">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-price="3" data-type="plus" data-field="quant[1]" data-rowidx="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding:7px;">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </td>
                        <td>
                            <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"></a>
                        </td>
                        <td>
                            <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                        <tr>
                            <td><a href="menu_single.html">
                                    <img src="http://ppcoffeecorner.com/images/items/1482227574.jpg" alt=""> Lor Bak Png滷肉飯 </a>
                            </td>
                            <td>3 $</td>
                            <td><!-- input group minus & plus-->
                                <div class="input-group plus-minus">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="minus" data-price="3" data-field="quant[1]" data-rowidy="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding: 7px"><span class="fa fa-minus"></span>
                                    </button>
                                </span>
                                    <input type="number" name="quant[1]" class="form-control input-number" value="1" min="1" max="999" id="qty[1]" style="background: white;border: 1px solid #e8e8e8;">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-price="3" data-type="plus" data-field="quant[1]" data-rowidx="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding:7px;">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </span>
                                </div>
                            </td>
                            <td>
                                <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"></a>
                            </td>
                            <td>
                                <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="menu_single.html">
                                    <img src="http://ppcoffeecorner.com/images/items/1482227574.jpg" alt=""> Lor Bak Png滷肉飯 </a>
                            </td>
                            <td>3 $</td>
                            <td><!-- input group minus & plus-->
                                <div class="input-group plus-minus">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="minus" data-price="3" data-field="quant[1]" data-rowidy="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding: 7px"><span class="fa fa-minus"></span>
                                    </button>
                                </span>
                                    <input type="number" name="quant[1]" class="form-control input-number" value="1" min="1" max="999" id="qty[1]" style="background: white;border: 1px solid #e8e8e8;">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-price="3" data-type="plus" data-field="quant[1]" data-rowidx="24d10c92e09359a294262a28a614d054" data-ids="qty[1]" data-tt="total1" style="padding:7px;">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </span>
                                </div>
                            </td>
                            <td>
                                <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"></a>
                            </td>
                            <td>
                                <span class="total1">3 $ </span><a class="pull-right c-del-f" href="#" data-rowidz="24d10c92e09359a294262a28a614d054"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <table class="table table-bordered">
                        <tbody id="total1">
                            <tr>
                                <td class="text-left"><strong>Sub-Total:</strong></td>
                                <td class="text-left">${{Cart::subtotal()}}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><strong>Shipping charge:</strong></td>
                                <td class="text-left">Free</td>
                            </tr>
                            <tr>
                                <td class="text-left"><strong>VAT (20%):</strong></td>
                                <td class="text-left">${{Cart::tax()}}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><strong>Total:</strong></td>
                                <td class="text-left">${{Cart::total()}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="buttons clearfix">
                <div class="pull-left"><a
                            href="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/index.php?route=common/home"
                            class="btn btn-default">Continue Shopping</a></div>
                <div class="pull-right"><a
                            href="{{ route('check_out') }}"
                            class="btn btn-primary">Checkout</a></div>
            </div>
        </div>
    </div>
    </div>
    <br>
@endsection