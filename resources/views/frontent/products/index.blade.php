@extends('frontent.layout.app')
@section('head')
    @parent
    <title>Products</title>
@endsection
@section('content')
<div class="container product-listing content-main" style="margin-top: 30px;margin-bottom: 30px">
    <div class="row">
        <div id="content" class="col-md-9 col-sm-12 col-xs-12">
            <div class="products-category">
                <h3 class="title-category ">Digital &amp; Electronics</h3>
                <div class="products-category">
                    <div class="product-filter filters-panel" style="background: #e0e0e0;padding: 5px">
                        <div class="row">
                            <div class="short-by-show form-inline col-md-10 col-sm-12">
                                <div class="form-group short-by">
                                    <label class="control-label" for="input-sort">Sort By:</label>
                                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                                        <option value="#"
                                                selected="selected">Default
                                        </option>
                                        <option value="#">
                                            Name (A - Z)
                                        </option>
                                        <option value="#">
                                            Name (Z - A)
                                        </option>
                                        <option value="#">
                                            Price (Low &gt; High)
                                        </option>
                                        <option value="#">
                                            Price (High &gt; Low)
                                        </option>
                                        <option value="#">
                                            Rating (Highest)
                                        </option>
                                        <option value="#">
                                            Rating (Lowest)
                                        </option>
                                        <option value="#">
                                            Model (A - Z)
                                        </option>
                                        <option value="#">
                                            Model (Z - A)
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-limit">Show:</label>
                                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                                        <option value="#" selected="selected">15
                                        </option>
                                        <option value="#"> 25
                                        </option>
                                        <option value="#"> 50
                                        </option>
                                        <option value="#"> 75
                                        </option>
                                        <option value="#"> 100
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group product-compare"><a href="#" id="compare-total" class="btn btn-default">Product Compare (0)</a></div>
                            </div>
                        </div>
                    </div>
                    @if($products == '[]')
                        <div style="vertical-align: bottom;text-align: center">
                            <h2><b>Sorry, this category is not yet available!</b></h2>
                        </div>
                    @else
                        <div class="products-list row grid">
                            @if(session('lan') == 'kh')
                                @foreach($products as $product)
                                    <div class="product-layout  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container  second_img  ">
                                                    <a href="#"
                                                       title=" Porkchop tongue cupim capicola ">
                                                        <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}"
                                                             title=" Porkchop tongue cupim capicola " class="img-1 img-responsive">
                                                        <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}" alt=" Porkchop tongue cupim capicola " title=" Porkchop tongue cupim capicola " class="img-2 img-responsive">
                                                    </a>
                                                </div>
                                                <div class="countdown_box">
                                                    <div class="countdown_inner">
                                                    </div>
                                                </div>
                                                <div class="box-label">
                                                    <span class="label-product label-sale">-36%</span>
                                                </div>
                                                <div class="button-group">
                                                    <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('29', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                    <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('29');"><i class="fa fa-heart"></i></button>
                                                    <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                    <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4 style="text-align: center;">
                                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/index.php?route=product/product&amp;path=33&amp;product_id=29 ">{{json_decode($product->product_name)->kh}}</a>
                                                    </h4>
                                                    <div class="price">
                                                        <span class="price-new">${{$product->product_price}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else(session('lan') == 'en')
                                @foreach($products as $product)
                                    <div class="product-layout  col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container  second_img  ">
                                                    <a href="{{URL::route('get_product_detail',$product->id)}}"
                                                       title=" Porkchop tongue cupim capicola ">
                                                        <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}"
                                                             title=" Porkchop tongue cupim capicola " class="img-1 img-responsive">
                                                        <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}" alt=" Porkchop tongue cupim capicola " title=" Porkchop tongue cupim capicola " class="img-2 img-responsive">
                                                    </a>
                                                </div>
                                                <div class="countdown_box">
                                                    <div class="countdown_inner">
                                                    </div>
                                                </div>
                                                <div class="box-label">
                                                    <span class="label-product label-sale">-36%</span>
                                                </div>
                                                <div class="button-group">
                                                    <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('29', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                    <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('29');"><i class="fa fa-heart"></i></button>
                                                    <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                    <a href="{{URL::route('get_product_detail',$product->id)}}" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4 style="text-align: center;">
                                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/index.php?route=product/product&amp;path=33&amp;product_id=29 ">{{json_decode($product->product_name)->en}}</a>
                                                    </h4>
                                                    <div class="price">
                                                        <span class="price-new">${{$product->product_price}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif
                </div>
            </div>
            <div class="row" style="padding-top:20px;padding-bottom:20px">
                <div class="content-product-bottom clearfix">
                    <div class="clearfix module related-horizontal col-xs-12">
                        <h3 class="modtitle"><span>Recommended Products</span></h3>
                        <div class="products-category">
                            <div class="related-products products-list grid contentslider" data-rtl="no"
                                 data-autoplay="no" data-pagination="no" data-delay="4" data-speed="0.6"
                                 data-margin="30" data-items_column0="4" data-items_column1="4"
                                 data-items_column2="2"
                                 data-items_column3="1" data-items_column4="1" data-arrows="yes" data-lazyload="yes"
                                 data-loop="no" data-hoverpause="yes">
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="#"
                                                   title=" Porkchop tongue cupim capicola ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/17-270x270.jpg " title=" Porkchop tongue cupim capicola " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-29"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('29', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('29');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="#"> Porkchop tongue cupim capicola </a></h4>
                                                <div class="price">
                                                    <span class="price-new">$337.99 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="#"
                                                   title="Alcatra sunt quispare ribs nulla ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/2-270x270.jpg " title="Alcatra sunt quispare ribs nulla " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-30"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                                <span class="label-product label-sale"> -20% </span>
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('30', '1');"><i  class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button"  title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4> <a href="#">Alcatra sunt quispare ribs nulla </a></h4>
                                                <div class="price">
                                                    <span class="price-new">$98.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="#"
                                                   title="Mapicola ullamco tempor jowlball ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/16-270x270.jpg " title="Mapicola ullamco tempor jowlball " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-31"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                                <span class="label-product label-sale"> -13% </span>
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('31', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('31');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4> <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=31 ">Mapicola  ullamco tempor jowlball </a></h4>
                                                <div class="price">
                                                    <span class="price-new">$86.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="#"
                                                   title="Fatback turducken burgdoggen porche ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/13-270x270.jpg " title="Fatback turducken burgdoggen porche " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-32"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('32', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('32');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4>
                                                    <a href="#">Fatback turducken burgdoggen porche </a>
                                                </h4>
                                                <div class="price">
                                                    <span class="price-new">$122.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="#"
                                                   title="Doner nisi estse strip steasgk ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/12-270x270.jpg " title="Doner nisi estse strip steasgk " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-34"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                        <span class="label-product label-sale">
                                             -20%
                                        </span>
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('34', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('34');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=34 ">Doner nisi estse strip steasgk </a>
                                                </h4>
                                                <div class="price">
                                                    <span class="price-new">$98.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=42 "
                                                   title=" Sausage aliqua prosciutto deserunt ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/1-270x270.jpg " title=" Sausage aliqua prosciutto deserunt " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-42"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                        <span class="label-product label-sale">
                                             -10%
                                        </span>
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('42', '2');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4> <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=42 "> Sausage aliqua prosciutto deserunt </a>
                                                </h4>
                                                <div class="price">
                                                    <span class="price-new">$110.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=43 "
                                                   title="Frankfer tri-tip doner prosciutto ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/14-270x270.jpg " title="Frankfer tri-tip doner prosciutto " class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-43"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                        <span class="label-product label-sale">
                                             -17%
                                        </span>
                                            </div>
                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('43', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('43');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=43 ">Frankfer tri-tip doner prosciutto </a></h4>
                                                <div class="price">
                                                    <span class="price-new">$92.00 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container">
                                                <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=49 "
                                                   title="Spicy jalapeno officia drumstick ">
                                                    <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/19-270x270.jpg " title="Spicy jalapeno officia drumstick "  class="img-1 img-responsive"/>
                                                </a>
                                            </div>
                                            <div class="countdown_box">
                                                <div class="countdown_inner">
                                                    <div class="defaultCountdown-49"></div>
                                                </div>
                                            </div>
                                            <div class="box-label">
                                            </div>

                                            <div class="button-group">
                                                <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('49', '1');"><i class="fa fa-shopping-cart"></i></button>
                                                <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('49');"><i class="fa fa-heart"></i></button>
                                                <button class="compare btn-button" type="button" title="Add to Compare" onclick="compare.add('49');"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=49 ">Spicy  jalapeno officia drumstick </a></h4>
                                                <div class="price">
                                                    <span class="price-new">$241.99 </span>
                                                </div>
                                                <div class="description hidden">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                        do eiusmod tempor incididunt ut labore.. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <aside class="col-md-3 col-sm-4 col-xs-12 content-aside right_column sidebar-offcanvas">
            <span id="close-sidebar" class="fa fa-times"></span>
            <div class="module category-style">
                <h3 class="modtitle"><span>MOBILE PHONES </span></h3>
                <div class="mod-content box-category">
                    <ul class="accordion" id="accordion-category">
                        <li class="panel">
                            <a href="#">Mom
                                &amp; Baby </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="module banner-left hidden-xs ">
                <h2></h2>
                <div class="banner-sidebar banners">
                    <div>
                        <a title="Banner Image" href="#">
                            <img src="image/catalog/demo/banners/banner-sidebar.jpg" alt="Banner Image">
                        </a>
                    </div>
                </div>
            </div>
        </aside>
    </div>
</div>
<div id="id01" class="w3-modal" style="padding-top: 30px">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom">
        <form action="">
            <header class="w3-container w3-blue" style="padding-top: 20px;padding-bottom: 10px;"> 
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-blue w3-xlarge w3-display-topright">&times;</span>
                <h2 style="text-align: center;font-size: 16px">Contact Messages</h2>
            </header>
            <div id="London" class="w3-container city" style="padding-right: 30px;padding-left: 30px">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first-name') ? ' has-error' : '' }}">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-controll" name="first-name" required="" placeholder="Enter first name..." style="width: 100%">
                            @if ($errors->has('first-name'))
                                <span class="help-block" style="color: 
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('first-name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last-name') ? ' has-error' : '' }}">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="form-controll" name="last-name" required="" placeholder="Enter last name..." style="width: 100%">
                            @if ($errors->has('last-name'))
                                <span class="help-block" style="color: 
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('last-name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-controll" name="email" required="" placeholder="Enter email address..." style="width: 100%">
                            @if ($errors->has('email'))
                                <span class="help-block" style="color: 
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-controll" name="subject" required="" placeholder="Enter subject name..." style="width: 100%">
                            @if ($errors->has('subject'))
                                <span class="help-block" style="color: 
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <label for="message">Messages</label>
                    <textarea name="message" class="form-controll" style="min-height: 200px;width: 100%" placeholder="Enter messages..."></textarea>
                    @if ($errors->has('message'))
                        <span class="help-block" style="color: 
                        white;font-family: sans-serif">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="w3-container w3-light-grey w3-padding">
                <button class="w3-btn w3-round-xxlarge" style="background: red;color: white;border:1px solid white">Send Message</button>
                <button class="w3-btn w3-round-xxlarge"  onclick="document.getElementById('id01').style.display='none'" style="background: orange;color: white;border:1px solid white">Close</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
@parent
@endsection