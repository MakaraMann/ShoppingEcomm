<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="http://opencart.opencartworks.com/themes/so_oneshop/"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Lorem ipsum dolor sit amet"/>
    @yield('head')
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/soconfig/css/lib.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/soconfig/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/ie9-and-up.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/layout1/red.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/header/header1.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/footer/footer1.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/responsive.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    <style type="text/css">body, #wrapper {font-family: 'Roboto', sans-serif}</style>
    <link href="web/image/catalog/favicon.png" rel="icon"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/animate.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/owl.carousel.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/assets/css/shortcodes.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_deals/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_deals/css/css3.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_extra_slider/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_extra_slider/css/css3.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_newletter_custom_popup/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_latest_blog/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style_render_33.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_tools/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_facebook_message/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_megamenu/so_megamenu.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_megamenu/wide-grid.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_searchpro/css/so_searchpro.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style_render_35.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_sociallogin/css/so_sociallogin.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/w3.css')}}">
</head>
<body class="product-product-30 ltr layout-1">
<div id="wrapper" class="wrapper-full banners-effect-10">
    <header id="header" class=" variant typeheader-1">
        <div class="header-top hidden-compact">
            <div class="container">
                <div class="row">
                    <div class="header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-2">
                        <ul class="top-link list-inline">
                            <li class="telephone hidden-xs hidden-sm">
                                <i class="fa fa-phone-square"></i> MyOnline: (+123)4 567 890
                            </li>
                            <li class="hidden-sm hidden-xs welcome-msg">
                                <i class="fa fa-envelope"></i> likdy@myonline.com
                            </li>
                        </ul>
                    </div>
                    <div class="header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-10">
                        <ul class="top-link list-inline">
                            <li class="language">
                                <div class="pull-left">
                                    <form action="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/index.php?route=common/language/language" method="post" enctype="multipart/form-data" id="form-language">
                                        <div class="btn-group">
                                            <button class="btn-link dropdown-toggle" data-toggle="dropdown">
                                                <img src="catalog/language/en-gb/en-gb.png" alt="English" title="English">
                                                <span class="hidden-xs hidden-sm hidden-md">English</span>
                                                <i class="fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <button class="btn-block language-select" type="button" name="ar-ar"><img src="catalog/language/ar-ar/ar-ar.png" alt="Arabic" title="Arabic"> Khmer</button>
                                                </li>
                                                <li>
                                                    <button class="btn-block language-select" type="button" name="en-gb"><img src="catalog/language/en-gb/en-gb.png" alt="English" title="English"> English</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <input type="hidden" name="code" value="">
                                        <input type="hidden" name="redirect" value="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/index.php?route=product/category&amp;path=33&amp;limit=50">
                                    </form>
                                </div>
                            </li>
                            <li class="account" id="my_account">
                                <a href="indexe223.html?route=account/account"
                                title="My Account " class="btn-xs dropdown-toggle"
                                data-toggle="dropdown"> <span class="hidden-xs">My Account </span>
                                <span class="fa fa-caret-down"></span></a>
                                <ul class="dropdown-menu ">
                                    <li><a href="{{URL::route('get_register')}}">Register</a></li>
                                    <li><a href="{{URL::route('login')}}">Login</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle ">
            <div class="container">
                <div class="row">
                    <div class="navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="logo" style="text-align: center;">
                            <a href="{{URL::route('home')}}">
                            <img src="{{URL::to('web/images/logo/1.png')}}" title="Your Store" alt="Your Store" width="40%" /></a>
                        </div>
                    </div>
                    <div class="navbar-logo col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="middle2 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="search-header-w">
                                    <div class="icon-search hidden-lg hidden-md"><i class="fa fa-search"></i></div>
                                    <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                        <form method="GET"
                                              action="http://opencart.opencartworks.com/themes/so_oneshop/index.php">
                                            <div id="search1" class="search input-group form-group">
                                                <div class="select_category filter_type  icon-select">
                                                    <select class="no-border" name="category_id">
                                                        <option value="0">All Categories</option>
                                                        @if(session('lan') == 'kh')
                                                            @foreach(App\MainCategories::all() as $main_category)
                                                            <option value="{{$main_category->id}}">{{json_decode($main_category->main_cat_name)->kh}}</option>
                                                            @endforeach
                                                        @else(session('lan') == 'en')
                                                            @foreach(App\MainCategories::all() as $main_category)
                                                            <option value="{{$main_category->id}}">{{json_decode($main_category->main_cat_name)->en}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <input class="autosearch-input form-control" type="text" value="" size="50"
                                                       autocomplete="off" placeholder="Keyword here..." name="search">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="button-search btn btn-default btn-lg" name="submit_search">Search<i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                            <input type="hidden" name="route" value="product/search"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="middle-right col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="shopping_cart">
                                    <div id="cart" class="btn-shopping-cart">
                                        <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                                           data-toggle="dropdown">
                                            <div class="shopcart">
                                                <span class="icon-c" style="text-align: center;padding-top: 5px"><img src="{{URL::to('web/image/sprites.png')}}" alt="" style="width: 80%;height: 80%"></span>
                                                <div class="shopcart-inner">
                                                    <p class="text-shopping-cart">
                                                        My cart
                                                    </p>
                                                    <span class="total-shopping-cart cart-total-full">{{Cart::count()}}
                                                        item(s) - ${{Cart::subtotal()}}</span>
                                                </div>
                                            </div>
                                        </a>
                                        <ul class="dropdown-menu pull-right shoppingcart-box">
                                            @if(Cart::count() == 0)
                                                <li>
                                                    <p class="text-center empty">Your shopping cart is empty!</p>
                                                </li>
                                            @else
                                                @foreach(Cart::content() as $item)
                                                    <li class="content-item">
                                                        <table class="table table-striped" style="margin-bottom:10px;">
                                                            <tbody>
                                                        <tr>
                                                            <td class="text-center size-img-cart">
                                                                <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=33">
                                                                    <img src="{{URL::to('web/image/cache/catalog/demo/product/1-200x200.jpg')}}" alt="Sunt beefrbs andlleui pignulla" title="Sunt beefrbs andlleui pignulla" class="img-thumbnail">
                                                                </a>
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="#">{{$item->name}}</a>
                                                            </td>
                                                            <td class="text-right"><input type="text" name="qty"
                                                                                          value="{{$item->qty}}"
                                                                                          style="width: 60px;text-align: center;">
                                                            </td>
                                                            <td class="text-right">{{$item->price * $item->qty}}</td>
                                                            <td class="text-center">
                                                                <button type="button" onclick="cart.remove('55');" title="Update" class="btn btn-danger btn-xs"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                                                <a href="{{URL::route('remove_item',$item->rowId)}}"
                                                                   class="btn btn-danger btn-xs"><i
                                                                            class="fa fa-trash-o"></i></a>
                                                            </td>
                                                        </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                @endforeach
                                                <li>
                                                <div class="checkout clearfix">
                                                    <a href="{{URL::route('view_cart')}}"
                                                       class="btn btn-view-cart inverse"> View Cart</a>
                                                    <a href="{{URL::route('check_out')}}"
                                                       class="btn btn-checkout pull-right">Checkout</a>
                                                </div>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom hidden-compact">
            <div class="container">
                <div class="row">
                    <div class="header-bottom-inner">
                        <div class="header-bottom-left menu-vertical col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="responsive megamenu-style-dev">
                                <div class="so-vertical-menu no-gutter">
                                    <nav class="navbar-default">
                                        <div class=" container-megamenu container vertical  ">
                                            <div id="menuHeading">
                                                <div class="megamenuToogle-wrapper">
                                                    <div class="megamenuToogle-pattern">
                                                        <div class="container">
                                                            <div><span></span><span></span><span></span></div>
                                                            Categories
                                                            <i class="fa fa-caret-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="navbar-header">
                                                <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                        class="navbar-toggle">
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <i class="fa fa-bars"></i>
                                                    <span class="hidden"> Categories </span>
                                                </button>
                                            </div>
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container">
                                                        <ul class="megamenu"
                                                            data-transition="slide" data-animationtime="300">
                                                            @if(session('lan') == 'kh')
                                                                @foreach(App\MainCategories::all() as $main_category)
                                                                    <li class="item-vertical  style1 with-sub-menu hover">
                                                                        <p class='close-menu'></p>
                                                                        <a href="index8122.html?route=product/category&amp;path=34"
                                                                           class="clearfix">
                                                                            <span>
                                                                                {{json_decode($main_category->main_cat_name)->kh}}</strong>
                                                                            </span>
                                                                            <b class='fa fa-angle-right'></b>
                                                                        </a>
                                                                        <div class="sub-menu" style="width: 5000px !important;">
                                                                            <div class="content">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="categories">
                                                                                            <div class="row">
                                                                                                @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                    <div class="col-sm-4 static-menu">
                                                                                                        <div class="menu">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                       class="main-menu" style="font-size: 14px">{{json_decode($parent_category->parent_cat_name)->kh}}</a>
                                                                                                                    <ul>
                                                                                                                        @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                            <li>
                                                                                                                                <a href="{{URL::route('get_product_by_child',$child_category->id)}}" style="color: black">{{json_decode($child_category->child_cat_name)->kh}}</a>
                                                                                                                            </li>
                                                                                                                        @endforeach 
                                                                                                                    </ul>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @else(session('lan') == 'en')
                                                                @foreach(App\MainCategories::all() as $main_category)
                                                                    <li class="item-vertical  style1 with-sub-menu hover">
                                                                        <p class='close-menu'></p>
                                                                        <a href="index8122.html?route=product/category&amp;path=34"
                                                                           class="clearfix">
                                                                            <span>
                                                                            {{json_decode($main_category->main_cat_name)->en}}</strong>
                                                                            </span>
                                                                            <b class='fa fa-angle-right'></b>
                                                                        </a>
                                                                        <div class="sub-menu" style="width:730px">
                                                                            <div class="content">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="categories ">
                                                                                            <div class="row">
                                                                                            @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                <div class="col-sm-4 static-menu">
                                                                                                    <div class="menu">
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                   class="main-menu" style="font-size: 14px">{{json_decode($parent_category->parent_cat_name)->en}}</a>
                                                                                                                <ul>
                                                                                                                @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                    <li>
                                                                                                                        <a href="{{URL::route('get_product_by_child',$child_category->id)}}" style="
                                                                                                                           color: black">{{json_decode($child_category->child_cat_name)->en}}</a>
                                                                                                                    </li>
                                                                                                                @endforeach 
                                                                                                                @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                    <li><a href="#" class="btn btn-success btn-sm"style="padding-right: 30px;padding-left: 30px;color: white;border-radius: 0px;border:none">More</a></li>
                                                                                                                @else
                                                                                                                @endif
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="header-bottom-right col-lg-9 col-md-9 col-sm-6 col-xs-6">
                            <div class="responsive megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu   horizontal ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container">
                                                    <ul class="megamenu"
                                                        data-transition="slide" data-animationtime="500">
                                                        @if(session('lan') == 'kh')
                                                            @foreach(App\MainCategories::all()->take(4) as $main_category)
                                                                <li class=" item-style2 with-sub-menu hover">
                                                                    <p class='close-menu'></p>
                                                                    <a href="#" class="clearfix">
                                                                        <strong>{{json_decode($main_category->main_cat_name)->kh}}
                                                                        </strong>
                                                                        <b class='caret'></b>
                                                                    </a>
                                                                    <div class="sub-menu" style="width: 100%">
                                                                        <div class="content">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="categories ">
                                                                                        <div class="row">
                                                                                        @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                            <div class="col-sm-4 static-menu">
                                                                                                <div class="menu">
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                               class="main-menu">{{json_decode($parent_category->parent_cat_name)->kh}}</a>
                                                                                                            <ul>
                                                                                                                @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                    <li>
                                                                                                                        <a href="{{URL::route('get_product_by_child',$child_category->id)}}">{{json_decode($child_category->child_cat_name)->kh}}</a>
                                                                                                                    </li>
                                                                                                                @endforeach 
                                                                                                                @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                    <li><a href="#" class="btn btn-success btn-sm" style="padding-right: 30px;padding-left: 30px;color: white;border-radius: 0px;border:none">More</a></li>
                                                                                                                @else
                                                                                                                @endif
                                                                                                            </ul>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        @else(session('lan') == 'en')
                                                            @foreach(App\MainCategories::all()->take(4) as $main_category)
                                                                <li class=" item-style2 with-sub-menu hover">
                                                                    <p class='close-menu'></p>
                                                                    <a href="#" class="clearfix">
                                                                        <strong>{{json_decode($main_category->main_cat_name)->en}}
                                                                        </strong>
                                                                        <b class='caret'></b>
                                                                    </a>
                                                                    <div class="sub-menu" style="width: 100%">
                                                                        <div class="content">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="categories ">
                                                                                        <div class="row">
                                                                                            @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                <div class="col-sm-4 static-menu">
                                                                                                    <div class="menu">
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}" class="main-menu">{{json_decode($parent_category->parent_cat_name)->en}}</a>
                                                                                                                <ul>
                                                                                                                    @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                        <li>
                                                                                                                            <a href="{{URL::route('get_product_by_child',$child_category->id)}}">{{json_decode($child_category->child_cat_name)->en}}</a>
                                                                                                                        </li>
                                                                                                                    @endforeach  
                                                                                                                    @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                        <li><a href="#" class="btn btn-success btn-sm" style="padding-right: 30px;padding-left: 30px;color: white;border-radius: 0px;border:none">More</a></li>
                                                                                                                    @else
                                                                                                                    @endif
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @yield('content')
    <div class="footer-newsletter">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <h5 class="newsletter-title">Sign up to Newsletter</h5><span class="error"></span>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7">
                    <form action="https://angkorstores.com/newsletter/index" name="newsletter" id="newsletter" method="post" accept-charset="utf-8">
                        <div class="input-group">
                            <input type="text" name="email" value="" id="email" class="form-control" placeholder="Enter your email address">
                            <span class="input-group-btn">
                            
                            <button name="btn_sign_up" type="submit" id="newsletter-signup" class="btn btn-secondary">Sign Up</button>
                                
                            </span>
                        </div>
                    </form>             
                </div>
            </div>
        </div>
    </div>
    <footer class="footer-container typefooter-1">
        <div class="footer-main collapse description-has-toggle" id="collapse-footer">
            <div class="so-page-builder">
                <div class="container page-builder-ltr">
                    <div class="row row_560y  row-style ">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_zk4z  col-style">

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_i76p  col-style">
                            <div class="box-contact box-footer">
                                <div class="modcontent">
                                    <img src="web/image/catalog/demo/logo/logo2.png" alt="logo footer">
                                    <p>They key is to have every key, the key to open every door. We don’t see them, we
                                        will never see them.</p>

                                    <ul class="infos">
                                        <li><i class="fa fa-map-marker"></i>100 S Manhattan Stress, Amarillo, TX 79104,
                                            North America
                                        </li>
                                        <li><i class="fa fa-phone"></i>( +123 )4 567 890 - ( +123 )4 567 899</li>
                                        <li><i class="fa fa-envelope"></i>support@sporttheme.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_njm1  col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Quick Links</h3>
                                    <div class="modcontent row">
                                        <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <li><a href="indexd773.html?route=product/manufacturer">Brands</a></li>
                                            <li><a href="#">Gift Certificates</a></li>
                                            <li><a href="index3d18.html?route=affiliate/login">Affiliates</a></li>
                                            <li><a href="indexf110-2.html?route=product/special">Specials</a></li>
                                            <li><a href="#">FAQs</a></li>
                                            <li><a href="#">Custom Link</a></li>
                                        </ul>
                                        <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <li>
                                                <a href="index8816.html?route=information/information&amp;information_id=4">About
                                                    Us</a></li>
                                            <li>
                                                <a href="index1766.html?route=information/information&amp;information_id=6">FAQ</a>
                                            </li>
                                            <li>
                                                <a href="index1679.html?route=information/information&amp;information_id=3">Warranty
                                                    And Services</a></li>
                                            <li>
                                                <a href="index11cd.html?route=information/information&amp;information_id=7">Support
                                                    24/7 page</a></li>
                                            <li><a href="#">Accessories</a></li>
                                            <li><a href="#">Trending Items</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_slxc  col-style">
                            <div class="box-community box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Join Our Community</h3>
                                    <div class="modcontent">
                                        <ul class="socials">
                                            <li class="facebook">
                                                <a class="_blank" href="https://www.facebook.com/MagenTech"
                                                target="_blank"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="twitter">
                                                <a class="_blank" href="https://twitter.com/smartaddons"
                                                target="_blank"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="rss">
                                                <a class="_blank" href="#" target="_blank"><i
                                                    class="fa fa-rss"></i></a></li>
                                            <li class="google_plus">
                                                <a class="_blank"href="https://plus.google.com/u/0/+Smartaddons/posts"
                                                target="_blank"><i class="fa fa-google-plus"></i></a>
                                            </li>
                                            <li class="pinterest">
                                                <a class="_blank"
                                                    href="https://www.pinterest.com/smartaddons/"
                                                    target="_blank"><i class="fa fa-pinterest"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="module module2 clearfix">
                                    <h3 class="modtitle">Download Apps</h3>
                                    <ul class="apps">
                                        <li><a href="#"><img src="web/image/catalog/demo/app1.png" alt="image"></a></li>
                                        <li><a href="#"><img src="web/image/catalog/demo/app2.png" alt="image"></a></li>
                                    </ul>
                                    
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="description-toggle hidden-lg hidden-md">
            <a class="showmore" data-toggle="collapse" href="#collapse-footer" aria-expanded="false"
               aria-controls="collapse-footer">
                <span class="toggle-more">Show More <i class="fa fa-angle-down"></i></span>
                <span class="toggle-less">Show Less <i class="fa fa-angle-up"></i></span>
            </a>
        </div>
        <div class="footer-bottom ">
            <div class="container">
                <div class="row">
                    <div class="copyright col-lg-6 col-xs-12">
                        DyMa © 2017 Demo Store. All Rights Reserved. Designed by <a
                            href="http://www.opencartworks.com/" target="_blank">Testing</a>
                    </div>
                    <div class="col-lg-6 col-xs-12 payment-w">
                        <img src="web/image/catalog/demo/payment/payment.png" alt="imgpayment">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
</div> 
<script src="{{asset('web/catalog/view/javascript/jquery/jquery-2.1.1.min.js')}}"></script>
<script src="{{asset('web/catalog/view/javascript/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('web/catalog/view/javascript/soconfig/js/libs.js')}}"></script>
<script src="{{asset('web/catalog/view/javascript/soconfig/js/owl.carousel.js')}}"></script>
<script src="{{asset('web/catalog/view/javascript/soconfig/js/so.system.js')}}"></script>
<script src="{{asset('web/catalog/view/theme/so-oneshop/js/so.custom.js')}}"></script>
<script src="{{asset('web/catalog/view/theme/so-oneshop/js/common.js')}}"></script>
<script src="{{asset('web/catalog/view/javascript/soconfig/js/jquery.elevateZoom-3.0.8.min.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/soconfig/js/lightslider.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js')}}"  type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_extra_slider/js/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_megamenu/so_megamenu.js')}}" type="text/javascript"></script>
<script src="{{asset('web/admin/view/template/extension/module/so_page_builder/assets/js/shortcodes.js')}}"  type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_page_builder/js/section.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_page_builder/js/modernizr.video.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_page_builder/js/swfobject.js')}}" type="text/javascript"></script>
<script src="{{asset('web/catalog/view/javascript/so_page_builder/js/video_background.js')}}" type="text/javascript"></script>
@yield('script')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        (function (element) {
            var $element = $(element),
                $extraslider = $(".extraslider-inner", $element),
                _delay = 500,
                _duration = 800,
                _effect = 'none ';

            $extraslider.on("initialized.owl.carousel2", function () {
                var $item_active = $(".owl2-item.active", $element);
                if ($item_active.length > 1 && _effect != "none") {
                    _getAnimate($item_active);
                }
                else {
                    var $item = $(".owl2-item", $element);
                    $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});
                }


                $(".owl2-controls", $element).insertBefore($extraslider);
                $(".owl2-dots", $element).insertAfter($(".owl2-prev", $element));

            });

            $extraslider.owlCarousel2({
                rtl: false,
                margin: 0,
                slideBy: 1,
                autoplay: 0,
                autoplayHoverPause: 0,
                autoplayTimeout: 0,
                autoplaySpeed: 1000,
                startPosition: 0,
                mouseDrag: 1,
                touchDrag: 1,
                autoWidth: false,
                responsive: {
                    0: {items: 1},
                    480: {items: 1},
                    768: {items: 1},
                    992: {items: 1},
                    1200: {items: 1}
                },
                dotClass: "owl2-dot",
                dotsClass: "owl2-dots",
                dots: false,
                dotsSpeed: 500,
                nav: false,
                loop: false,
                navSpeed: 500,
                navText: ["&#171 ", "&#187 "],
                navClass: ["owl2-prev", "owl2-next"]

            });

            $extraslider.on("translate.owl.carousel2", function (e) {


                var $item_active = $(".owl2-item.active", $element);
                _UngetAnimate($item_active);
                _getAnimate($item_active);
            });

            $extraslider.on("translated.owl.carousel2", function (e) {


                var $item_active = $(".owl2-item.active", $element);
                var $item = $(".owl2-item", $element);

                _UngetAnimate($item);

                if ($item_active.length > 1 && _effect != "none") {
                    _getAnimate($item_active);
                } else {

                    $item.css({"opacity": 1, "filter": "alpha(opacity = 100)"});

                }
            });

            function _getAnimate($el) {
                if (_effect == "none") return;
                //if ($.browser.msie && parseInt($.browser.version, 10) <= 9) return;
                $extraslider.removeClass("extra-animate");
                $el.each(function (i) {
                    var $_el = $(this);
                    $(this).css({
                        "-webkit-animation": _effect + " " + _duration + "ms ease both",
                        "-moz-animation": _effect + " " + _duration + "ms ease both",
                        "-o-animation": _effect + " " + _duration + "ms ease both",
                        "animation": _effect + " " + _duration + "ms ease both",
                        "-webkit-animation-delay": +i * _delay + "ms",
                        "-moz-animation-delay": +i * _delay + "ms",
                        "-o-animation-delay": +i * _delay + "ms",
                        "animation-delay": +i * _delay + "ms",
                        "opacity": 1
                    }).animate({
                        opacity: 1
                    });

                    if (i == $el.size() - 1) {
                        $extraslider.addClass("extra-animate");
                    }
                });
            }

            function _UngetAnimate($el) {
                $el.each(function (i) {
                    $(this).css({
                        "animation": "",
                        "-webkit-animation": "",
                        "-moz-animation": "",
                        "-o-animation": "",
                        "opacity": 1
                    });
                });
            }

        })("#so_extra_slider_12400785161508000057 ");
        <?php for ($i = 1; $i < 10; $i++){ ?>
        $('#upCart<?php echo $i; ?>').on('change', function () {
            var newqty = $('#upCart<?php echo $i; ?>').val();
            var rowId = $('#rowId<?php echo $i; ?>').val();
            var proId = $('#proId<?php echo $i; ?>').val();
            if (newqty <= 0) {
                alert('Please input valid number');
            } else {
                $.ajax({
                    url: '{{url('cart/update/')}}/' + rowId,
                    type: 'get',
                    data: "quantity=" + newqty,
                    success: function (data) {
                        console.log(data);
                        shoppingfn(data);
                    }
                });
            }
        });

        <?php } ?>
        var shopping = '';
        var total = '';

        function shoppingfn(data) {
            var num = 1;
            $.each(data, function (i, it) {
                shopping += '<tr><td class="text-center"><a href="#"><img src="http://opencart.opencartworks.com/themes/so_oneshop_marketplace/image/cache/catalog/demo/product/17-47x47.jpg" alt="Hello" class="img-thumbnail"></a></td>' +
                    '<td class="text-left"><a href="#">' + it.name + '</a></td>' +
                    '<td class="text-left">Product 2</td>' +
                    '<td class="text-left"><div class=" btn-block" style="max-width: 200px;">' +
                    '<input type="hidden" id="proId' + num + '"' + 'value="' + it.id + '">' +
                    '<input type="hidden" id="rowId' + num + '"' + 'value="' + it.rowId + '">' +
                    '<input type="text" name="quantity" id="upCart' + num + '"' + 'value="' + it.qty + '" size="1" class="form-control">' +
                    '<span class="input-group-btn"><a href="{{url('cart/remove_item/')}}/' + it.rowId + '" class="btn btn-danger" title="Delete" data-toggle="tooltip">' +
                    '<i class="fa fa-times-circle"></i></a></span>' +
                    '</div></td>' +
                    '<td class="text-right">$ ' + it.price + '</td>' +
                    '<td class="text-right">$ ' + (it.price * it.qty) + '</td>' +
                    '</tr>';
                num++;
            });
            total1 += '<tr>' +
                '<td class="text-right"><strong>Sub-Total:</strong></td>' +
                '<td class="text-right">' + data.subtotal + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td class="text-right"><strong>Shipping charge:</strong></td>' +
                '<td class="text-right">Free</td>' +
                '</tr>' +
                '<tr>' +
                '<td class="text-right"><strong>VAT (20%):</strong></td>' +
                '<td class="text-right">$' + data.tax + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td class="text-right"><strong>Total:</strong></td>' +
                '<td class="text-right">$' + data.total + '</td>' +
                '</tr>';
            $('#shopping').html(shopping);
            $('#total1').html(total1);
        }
    });
    //]]>
</script>
<script type="text/javascript">
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert-dismissible, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    }); 
</script>
<script type="text/javascript">
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=extension/soconfig/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert').remove();
                $('.form-group').removeClass('has-error');
                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {

                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="fa fa-close close" data-dismiss="alert"></button></div>');
                    $('#cart  .total-shopping-cart ').html(json['total']);
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    $('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');
                    $('.text-danger').remove();
                    timer = setTimeout(function () {
                        $('.alert').addClass('fadeOut');
                    }, 4000);
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        language: document.cookie.match(new RegExp('language=([^;]+)'))[1],
        pickTime: false
    });
    $('.datetime').datetimepicker({
        language: document.cookie.match(new RegExp('language=([^;]+)'))[1],
        pickDate: true,
        pickTime: true
    });
    $('.time').datetimepicker({
        language: document.cookie.match(new RegExp('language=([^;]+)'))[1],
        pickDate: false
    });
    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-upload input[name=\'file\']').trigger('click');
        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }
        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').val(json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
</script>
<script type="text/javascript">
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();
        $('#review').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');
    });
    $('#review').load('index.php?route=product/product/review&product_id=30');
    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=30',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-dismissible').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var zoomCollection = '.large-image img';
        $(zoomCollection).elevateZoom({
            zoomType: "inner",
            lensSize: 250,
            easing: true,
            scrollZoom: true,
            gallery: 'thumb-slider',
            cursor: 'pointer',
            galleryActiveClass: "active",
        });
        $('.large-image img').magnificPopup({
            items: [
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/2-600x600.jpg'},
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/7-600x600.jpg'},
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/6-600x600.jpg'},
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/5-600x600.jpg'},
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/3-600x600.jpg'},
                {src: 'http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/4-600x600.jpg'},
            ],
            gallery: {enabled: true, preload: [0, 2]},
            type: 'image',
            mainClass: 'mfp-fade',
            callbacks: {
                open: function () {
                    var activeIndex = parseInt($('#thumb-slider .img.active').attr('data-index'));
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.goTo(activeIndex);
                }
            }
        });
        $("#thumb-slider .image-additional").each(function () {
            $(this).find("[data-index='0']").addClass('active');
        });
        $('.product-options li.radio').click(function () {
            $(this).addClass(function () {
                if ($(this).hasClass("active")) return "";
                return "active";
            });
            $(this).siblings("li").removeClass("active");
            $(this).parent().find('.selected-option').html('<span class="label label-success">' + $(this).find('img').data('original-title') + '</span>');
        })
        $(".thumb-vertical-outer .next-thumb").click(function () {
            $(".thumb-vertical-outer .lSNext").trigger("click");
        });
        $(".thumb-vertical-outer .prev-thumb").click(function () {
            $(".thumb-vertical-outer .lSPrev").trigger("click");
        });
        $(".thumb-vertical-outer .thumb-vertical").lightSlider({
            item: 4,
            autoWidth: false,
            vertical: true,
            slideMargin: 10,
            verticalHeight: 410,
            pager: false,
            controls: true,
            prevHtml: '<i class="fa fa-angle-up"></i>',
            nextHtml: '<i class="fa fa-angle-down"></i>',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        verticalHeight: 300,
                        item: 3,
                    }
                }, {
                    breakpoint: 1024,
                    settings: {
                        verticalHeight: 300,
                        item: 2,
                        slideMargin: 5,
                    }
                }, {
                    breakpoint: 991,
                    settings: {
                        verticalHeight: 470,
                        item: 4,
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        verticalHeight: 300,
                        item: 2,
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        verticalHeight: 140,
                        item: 1,
                    }
                }
            ]
        });
    });
</script>
<script type="text/javascript">
    var ajax_price = function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?route=extension/soconfig/liveprice/index',
            data: $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\']:checked, .product-detail input[type=\'checkbox\']:checked, .product-detail select, .product-detail textarea'),
            dataType: 'json',
            success: function (json) {
                if (json.success) {
                    change_price('#price-special', json.new_price.special);
                    change_price('#price-tax', json.new_price.tax);
                    change_price('#price-old', json.new_price.price);
                }
            }
        });
    }
    var change_price = function (id, new_price) {
        $(id).html(new_price);
    }
    $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\'], .product-detail input[type=\'checkbox\'], .product-detail select, .product-detail textarea, .product-detail input[name=\'quantity\']').on('change', function () {
        ajax_price();
    });
</script>
</body>