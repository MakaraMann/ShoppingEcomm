@extends('frontent.layout.app')
@section('head')
    @parent
    <title>Product Detail</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://i.icomoon.io/public/c88de6d4a5/DWSiteGenesis/style.css'>
    <link rel='stylesheet prefetch' href='https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css'>
    <link rel='stylesheet prefetch' href='https://photoswipe.s3.amazonaws.com/pswp/dist/photoswipe.css'>
    <link rel='stylesheet prefetch' href='https://photoswipe.s3.amazonaws.com/pswp/dist/default-skin/default-skin.css'>
    <link rel="stylesheet" href="{{asset('web/detail/css/style.css')}}">
@endsection
@section('content')
<div class="content-main container product-detail" style="margin-top: 40px">
    <div class="row">
        <div id="content" class="col-md-12 col-sm-12 col-xs-12">
            @if(session('lan') == 'kh')
            <div class="row product-view product-detail">
                <div class="content-product-left  col-md-6 col-sm-12 col-xs-12">
                    <div class="wrapper">
                        <div class="product-images">
                            <div class="main-img-slider">
                                @foreach(App\Photos::where('product_id',$product->id)->get() as $photo)
                                    <figure>
                                        <a href="{{asset($photo->image)}}" data-size="1400x1400">
                                            <img src="{{asset($photo->image)}}" data-lazy="{{asset($photo->image)}}"  alt="" />
                                        </a>
                                    </figure>
                                @endforeach
                            </div>
                            <ul class="thumb-nav">
                                @foreach(App\Photos::where('product_id',$product->id)->get() as $photo)
                                    <li><img src="{{asset($photo->image)}}" /></li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="content-product-right col-md-6 col-sm-12 col-xs-12">
                    <div class="title-product" style="padding-bottom: 0px;padding-top: 20px">
                        <p style="font-size: 14px">{{json_decode($product->product_name)->kh}}</p>
                    </div>
                    <div class="product_page_price price" style="text-align: left;">
                        <span class="price-new"><span>${{$product->product_price}}.00</span></span>
                    </div>
                    <div class="product-box-desc">
                        <div class="inner-box-desc">
                            <div class="model"><span>Product Code: </span> Product 3</div>
                            <div class="reward"><span>Reward Points:</span> {{json_decode($product->product_code)->kh}}</div>
                            <div class="stock"><span>Availability:</span> <i class="fa fa-check-square-o"></i> In
                                Stock
                            </div>
                        </div>
                    </div>
                    <div class="short_description form-group" style="padding-bottom: 0px;padding-top: 10px">
                        <h3>Contact</h3>
                    </div>
                    <div class="short_description form-group" style="padding-top: 0px">
                        <div class="model"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:15px"></i> Call Us : 0964031171 / 0717028566 </div>
                        <div class="model"><i class="fa fa-envelope" aria-hidden="true" style="font-size:14px"></i> Email : <a href="#">likdy2015@gmail.com</a></div>
                        <div class="model"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size:15px"></i> Facebook : <a href="#">KhmerOnline</a></div>
                    </div>
                    <div id="product">
                        <div class="box-cart clearfix">
                            <div class="form-group box-info-product">
                                <div class="option quantity">
                                    <div class="input-group quantity-control">
                                        <span class="input-group-addon product_quantity_down fa fa-minus"></span>
                                        <input class="form-control" type="text" name="quantity" value="1"/>
                                        <input type="hidden" name="product_id" value="30"/>
                                        <span class="input-group-addon product_quantity_up fa fa-plus"></span>
                                    </div>
                                </div>
                                <div class="detail-action">
                                    <div class="cart">
                                        <input type="button" value="Add to Cart" data-loading-text="Loading..." id="button-cart" class="btn btn-mega btn-lg "/>
                                    </div>
                                    <div class="add-to-links wish_comp">
                                        <ul class="blank">
                                            <li class="wishlist">
                                                <a title="Add to Wish List" onclick="wishlist.add(30);"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li class="compare">
                                                <a title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            @else(session('lan') == 'en')
                <div class="row product-view product-detail">
                    <div class="content-product-left  col-md-6 col-sm-12 col-xs-12">
                        <div class="wrapper">
                            <div class="product-images">
                                <div class="main-img-slider">
                                    @foreach(App\Photos::where('product_id',$product->id)->get() as $photo)
                                        <figure>
                                            <a href="{{asset($photo->image)}}" data-size="1400x1400">
                                                <img src="{{asset($photo->image)}}" data-lazy="{{asset($photo->image)}}"  alt="" />
                                            </a>
                                        </figure>
                                    @endforeach
                                </div>
                                <ul class="thumb-nav">
                                    @foreach(App\Photos::where('product_id',$product->id)->get() as $photo)
                                        <li><img src="{{asset($photo->image)}}" /></li>
                                        @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="content-product-right col-md-6 col-sm-12 col-xs-12">
                        <div class="title-product" style="padding-bottom: 10px;padding-top: 0px">
                            <p style="font-size: 14px">{{json_decode($product->product_name)->en}}</p>
                        </div>
                        <div class="product_page_price price" style="text-align: left;">
                            <span class="price-new"><span>${{$product->product_price}}.00</span></span>
                        </div>
                        <div class="product-box-desc">
                            <div class="inner-box-desc">
                                <div class="model"><span>Product Code: </span> Product 3</div>
                                <div class="reward"><span>Reward Points:</span> {{json_decode($product->product_code)->en}}</div>
                                <div class="stock"><span>Availability:</span> <i class="fa fa-check-square-o"></i> In
                                    Stock
                                </div>
                            </div>
                        </div>
                        <div class="short_description form-group" style="padding-bottom: 0px;padding-top: 10px">
                            <h3>Contact</h3>
                        </div>
                        <div class="short_description form-group" style="padding-top: 0px">
                            <div class="model"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:15px"></i> Call Us : 0964031171 / 0717028566 </div>
                            <div class="model"><i class="fa fa-envelope" aria-hidden="true" style="font-size:14px"></i> Email : <a href="#">likdy2015@gmail.com</a></div>
                            <div class="model"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size:15px"></i> Facebook : <a href="#">KhmerOnline</a></div>
                        </div>
                        <div id="product">
                            <div class="box-cart clearfix">
                                <div class="form-group box-info-product">
                                    <div class="option quantity">
                                        <div class="input-group quantity-control">
                                            <span class="input-group-addon product_quantity_down fa fa-minus"></span>
                                            <input class="form-control" type="text" name="quantity" value="1"/>
                                            <input type="hidden" name="product_id" value="30"/>
                                            <span class="input-group-addon product_quantity_up fa fa-plus"></span>
                                        </div>
                                    </div>
                                    <div class="detail-action">
                                        <div class="cart">
                                            <input type="button" value="Add to Cart" data-loading-text="Loading..." id="button-cart" class="btn btn-mega btn-lg "/>
                                        </div>
                                        <div class="add-to-links wish_comp">
                                            <ul class="blank">
                                                <li class="wishlist">
                                                    <a title="Add to Wish List" onclick="wishlist.add(30);"><i class="fa fa-heart"></i></a>
                                                </li>
                                                <li class="compare">
                                                    <a title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a href="#"><img src="{{URL::to('web/images/sponsor/11.jpg')}}" alt="" width="100%"></a>
        </div>
        <div class="col-md-6">
            <a href="#"><img src="{{URL::to('web/images/sponsor/13.gif')}}" alt="" width="100%"></a>
        </div>
    </div>
    <div class="row">
        <div class="content-product-midde clearfix">
            <div class="col-xs-12">
                <div class="producttab ">
                    <div class="tabsslider   horizontal-tabs  col-xs-12">
                        <ul class="nav nav-tabs font-sn">
                            <li class="active"><a data-toggle="tab" href="#tab-description">Description</a>
                            </li>
                        </ul>
                        <div class="tab-content  col-xs-12">
                            <div class="tab-pane active" id="tab-description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste  natus error sit voluptatem accusantium doloremque laudantium.</p>
                                <p><strong>Nemo enim ipsam voluptatem</strong></p>
                                <ul>
                                    <li>100% Brand New.</li>
                                    <li>Contains 1 PCS</li>
                                    <li>Simple and easy</li>
                                </ul>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                    dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est
                                    laborum.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-product-bottom clearfix">
            <div class="clearfix module related-horizontal col-xs-12">
                <h3 class="modtitle"><span>Related Products</span></h3>
                <div class="products-category">
                    <div class="related-products products-list grid contentslider" data-rtl="no"
                        data-autoplay="no" data-pagination="no" data-delay="4" data-speed="0.6"
                        data-margin="30" data-items_column0="5" data-items_column1="5"
                        data-items_column2="2"
                        data-items_column3="1" data-items_column4="1" data-arrows="yes" data-lazyload="yes"
                        data-loop="no" data-hoverpause="yes">
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=29 "
                                           title=" Porkchop tongue cupim capicola ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/17-270x270.jpg " title=" Porkchop tongue cupim capicola " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-29"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('29', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('29');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=29 "> Porkchop tongue cupim capicola </a></h4>
                                        <div class="price">
                                            <span class="price-new">$337.99 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=30 "
                                           title="Alcatra sunt quispare ribs nulla ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/2-270x270.jpg " title="Alcatra sunt quispare ribs nulla " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-30"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                        <span class="label-product label-sale"> -20% </span>
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('30', '1');"><i  class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button"  title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4> <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=30 ">Alcatra sunt quispare ribs nulla </a></h4>
                                        <div class="price">
                                            <span class="price-new">$98.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=31 "
                                           title="Mapicola ullamco tempor jowlball ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/16-270x270.jpg " title="Mapicola ullamco tempor jowlball " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-31"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                        <span class="label-product label-sale"> -13% </span>
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('31', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('31');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4> <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=31 ">Mapicola  ullamco tempor jowlball </a></h4>
                                        <div class="price">
                                            <span class="price-new">$86.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=32 "
                                           title="Fatback turducken burgdoggen porche ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/13-270x270.jpg " title="Fatback turducken burgdoggen porche " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-32"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('32', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('32');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4>
                                            <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=32 ">Fatback turducken burgdoggen porche </a>
                                        </h4>
                                        <div class="price">
                                            <span class="price-new">$122.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=34 "
                                           title="Doner nisi estse strip steasgk ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/12-270x270.jpg " title="Doner nisi estse strip steasgk " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-34"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                        <span class="label-product label-sale">
                                             -20%
                                        </span>
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('34', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('34');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=34 ">Doner nisi estse strip steasgk </a>
                                        </h4>
                                        <div class="price">
                                            <span class="price-new">$98.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=42 "
                                           title=" Sausage aliqua prosciutto deserunt ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/1-270x270.jpg " title=" Sausage aliqua prosciutto deserunt " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-42"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                        <span class="label-product label-sale">
                                             -10%
                                        </span>
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('42', '2');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4> <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=42 "> Sausage aliqua prosciutto deserunt </a>
                                        </h4>
                                        <div class="price">
                                            <span class="price-new">$110.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=43 "
                                           title="Frankfer tri-tip doner prosciutto ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/14-270x270.jpg " title="Frankfer tri-tip doner prosciutto " class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-43"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                        <span class="label-product label-sale">
                                             -17%
                                        </span>
                                    </div>
                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('43', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('43');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Contact Message" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=43 ">Frankfer tri-tip doner prosciutto </a></h4>
                                        <div class="price">
                                            <span class="price-new">$92.00 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout">
                            <div class="product-item-container">
                                <div class="left-block">
                                    <div class="product-image-container">
                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=49 "
                                           title="Spicy jalapeno officia drumstick ">
                                            <img src="http://opencart.opencartworks.com/themes/so_oneshop/image/cache/catalog/demo/product/19-270x270.jpg " title="Spicy jalapeno officia drumstick "  class="img-1 img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="countdown_box">
                                        <div class="countdown_inner">
                                            <div class="defaultCountdown-49"></div>
                                        </div>
                                    </div>
                                    <div class="box-label">
                                    </div>

                                    <div class="button-group">
                                        <button class="btn-button" type="button" title="Add to Cart" onclick="cart.add('49', '1');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="wishlist btn-button" type="button" title="Add to Wish List" onclick="wishlist.add('49');"><i class="fa fa-heart"></i></button>
                                        <button class="compare btn-button" type="button" title="Add to Compare" onclick="compare.add('49');"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <div class="caption">
                                        <h4><a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=49 ">Spicy  jalapeno officia drumstick </a></h4>
                                        <div class="price">
                                            <span class="price-new">$241.99 </span>
                                        </div>
                                        <div class="description hidden">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                do eiusmod tempor incididunt ut labore.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div role="tabpanel" class="reviews-tabs padding-t-100">
            <div class="tab-content" style="border-top: 2px solid #e0e0e0;background:#e0e0e0">
              <div role="tabpanel" class="tab-pane active" id="review">
                <div class="fb-comments comment-blog" data-width="100%"  data-numposts="5"></div>
                <!-- End# Comments -->
              </div>
            </div>
        </div>
    </div>
</div>
<div id="id01" class="w3-modal" style="padding-top: 30px">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom">
        <form action="">
            <header class="w3-container w3-blue" style="padding-top: 20px;padding-bottom: 10px;">
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-blue w3-xlarge w3-display-topright">&times;</span>
                <h2 style="text-align: center;font-size: 16px">Contact Messages</h2>
            </header>
            <div id="London" class="w3-container city" style="padding-right: 30px;padding-left: 30px">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first-name') ? ' has-error' : '' }}">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-controll" name="first-name" required="" placeholder="Enter first name..." style="width: 100%">
                            @if ($errors->has('first-name'))
                                <span class="help-block" style="color:
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('first-name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last-name') ? ' has-error' : '' }}">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="form-controll" name="last-name" required="" placeholder="Enter last name..." style="width: 100%">
                            @if ($errors->has('last-name'))
                                <span class="help-block" style="color:
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('last-name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-controll" name="email" required="" placeholder="Enter email address..." style="width: 100%">
                            @if ($errors->has('email'))
                                <span class="help-block" style="color:
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-controll" name="subject" required="" placeholder="Enter subject name..." style="width: 100%">
                            @if ($errors->has('subject'))
                                <span class="help-block" style="color:
                                white;font-family: sans-serif">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <label for="message">Messages</label>
                    <textarea name="message" class="form-controll" style="min-height: 200px;width: 100%" placeholder="Enter messages..."></textarea>
                    @if ($errors->has('message'))
                        <span class="help-block" style="color:
                        white;font-family: sans-serif">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="w3-container w3-light-grey w3-padding">
                <button class="w3-btn w3-round-xxlarge" style="background: red;color: white;border:1px solid white">Send Message</button>
                <button class="w3-btn w3-round-xxlarge"  onclick="document.getElementById('id01').style.display='none'" style="background: orange;color: white;border:1px solid white">Close</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
@parent
<script src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
<script src='https://photoswipe.s3-eu-west-1.amazonaws.com/pswp/dist/photoswipe-ui-default.min.js'></script>
<script src='https://photoswipe.s3-eu-west-1.amazonaws.com/pswp/dist/photoswipe.min.js'></script>
<script src='https://kenwheeler.github.io/slick/slick/slick.js'></script>

<script  src="{{asset('web/detail/js/index.js')}}"></script>

    <script>
    (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1054001204745346";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '732895823516501',
                xfbml      : true,
                version    : 'v2.6',
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection