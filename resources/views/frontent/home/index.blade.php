<!DOCTYPE html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <base/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"  content="OneShop is"/>
    <meta name="keywords" content="clothing"/>
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/soconfig/css/lib.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/javascript/soconfig/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/ie9-and-up.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/layout1/red.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/header/header1.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/footer/footer1.css')}}">
    <link rel="stylesheet" href="{{asset('web/catalog/view/theme/so-oneshop/css/responsive.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    <style type="text/css">body, #wrapper {font-family: 'Roboto', sans-serif}</style>
    <link href="web/image/catalog/favicon.png" rel="icon"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/animate.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_home_slider/css/owl.carousel.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/assets/css/shortcodes.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_deals/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_deals/css/css3.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_extra_slider/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_extra_slider/css/css3.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_newletter_custom_popup/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_latest_blog/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style_render_33.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_tools/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_facebook_message/css/style.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_megamenu/so_megamenu.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_megamenu/wide-grid.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_searchpro/css/so_searchpro.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_page_builder/css/style_render_35.css')}}" type="text/css" rel="stylesheet"  media="screen"/>
    <link href="{{asset('web/catalog/view/javascript/so_sociallogin/css/so_sociallogin.css')}}" type="text/css" rel="stylesheet" media="screen"/>
    <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/w3.css')}}">

</head>
<body class="common-home ltr layout-1">
    <div id="wrapper" class="wrapper-full banners-effect-10">
        <header id="header" class=" variant typeheader-1">
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-2">
                            <ul class="top-link list-inline">
                                <li class="telephone hidden-xs hidden-sm">
                                    <i class="fa fa-phone-square"></i> MyOnline: (+123)4 567 890
                                </li>
                                <li class="hidden-sm hidden-xs welcome-msg">
                                    <i class="fa fa-envelope"></i> likdy@myonline.com
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-10">
                            <ul class="top-link list-inline">
                                <li id="my_account">
                                    <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                    data-toggle="dropdown"> <span class="hidden-xs">Languages</span>
                                    <span class="fa fa-caret-down"></span></a>
                                    <ul class="dropdown-menu " style="padding-right: 10px;padding-left: 10px;padding-top: 0px;padding-bottom: 0px">
                                        <li><a href="index5502.html?route=account/register"><img src="web/images/c.png" alt="Khmer" title="Khmer" width="20px" height="15px"> <b>Cambodia</b></a></li>
                                        <li><a href="index5502.html?route=account/register"><img src="web/images/e.png" alt="Khmer" title="Khmer" width="20px" height="15px"> <b>English</b></a></li>
                                    </ul>
                                </li>
                                <li class="account" id="my_account">
                                    <a href="indexe223.html?route=account/account"
                                       title="My Account " class="btn-xs dropdown-toggle"
                                       data-toggle="dropdown"> <span class="hidden-xs">My Account </span>
                                        <span class="fa fa-caret-down"></span></a>
                                    <ul class="dropdown-menu ">
                                        <li><a href="{{URL::route('get_register')}}">Register</a></li>
                                        <li><a href="{{URL::route('login')}}">Login</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-middle ">
                <div class="container">
                    <div class="row">
                        <div class="navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="logo" style="text-align: center;">
                                <a href="{{URL::route('home')}}">
                                <img src="web/images/logo/1.png" title="Your Store" alt="Your Store" width="40%" /></a>
                            </div>
                        </div>
                        <div class="navbar-logo col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="middle2 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="search-header-w">
                                        <div class="icon-search hidden-lg hidden-md"><i class="fa fa-search"></i></div>
                                        <div id="sosearchpro" class="sosearchpro-wrapper so-search ">
                                            <form method="GET"
                                                  action="http://opencart.opencartworks.com/themes/so_oneshop/index.php">
                                                <div id="search1" class="search input-group form-group">
                                                    <div class="select_category filter_type  icon-select">
                                                        <select class="no-border" name="category_id">
                                                            <option value="0">All Categories</option>
                                                            @if(session('lan') == 'kh')
                                                                @foreach(App\MainCategories::all() as $main_category)
                                                                <option value="{{$main_category->id}}">{{json_decode($main_category->main_cat_name)->kh}}</option>
                                                                @endforeach
                                                            @else(session('lan') == 'en')
                                                                @foreach(App\MainCategories::all() as $main_category)
                                                                <option value="{{$main_category->id}}">{{json_decode($main_category->main_cat_name)->en}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Keyword here..." name="search">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="button-search btn btn-default btn-lg" name="submit_search">Search<i class="fa fa-search"></i></button>
                                                    </span>
                                                </div>
                                                <input type="hidden" name="route" value="product/search"/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="middle-right col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="shopping_cart">
                                        <div id="cart" class="btn-shopping-cart">
                                            <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                                               data-toggle="dropdown">
                                                <div class="shopcart">
                                                    <span class="icon-c" style="text-align: center;padding-top: 5px"><img src="{{URL::to('web/image/sprites.png')}}" alt="" style="width: 80%;height: 80%"></span>
                                                    <div class="shopcart-inner">
                                                        <p class="text-shopping-cart">
                                                            My cart
                                                        </p>
                                                        <span class="total-shopping-cart cart-total-full">{{Cart::count()}}
                                                            item(s) - ${{Cart::subtotal()}}</span>
                                                    </div>
                                                </div>
                                            </a>
                                            <ul class="dropdown-menu pull-right shoppingcart-box">
                                                @if(Cart::count() == 0)
                                                    <li>
                                                        <p class="text-center empty">Your shopping cart is empty!</p>
                                                    </li>
                                                @else
                                                    @foreach(Cart::content() as $item)
                                                        <li class="content-item">
                                                            <table class="table table-striped"
                                                                   style="margin-bottom:10px;">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="text-center size-img-cart">
                                                                        <a href="http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=product/product&amp;product_id=33">
                                                                            <img src="{{URL::to('web/image/cache/catalog/demo/product/1-200x200.jpg')}}"
                                                                                 alt="Sunt beefrbs andlleui pignulla"
                                                                                 title="Sunt beefrbs andlleui pignulla"
                                                                                 class="img-thumbnail">
                                                                        </a>
                                                                    </td>
                                                                    <td class="text-left">
                                                                        <a href="#">{{$item->name}}</a>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <input type="text" name="qty" id="upCart"
                                                                               value="{{$item->qty}}"
                                                                               style="width: 60px;text-align: center;">
                                                                    </td>
                                                                    <td class="text-right">{{$item->price * $item->qty}}</td>
                                                                    <td class="text-center">
                                                                        {{--<button type="button" onclick="cart.remove('55');" title="Update" class="btn btn-danger btn-xs">--}}
                                                                        {{--<i class="fa fa-refresh" aria-hidden="true"></i>--}}
                                                                        {{--</button>--}}
                                                                        <a href="{{URL::route('remove_item',$item->rowId)}}"
                                                                           class="btn btn-danger btn-xs"><i
                                                                                    class="fa fa-trash-o"></i></a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </li>
                                                    @endforeach
                                                    <li>
                                                        <div class="checkout clearfix">
                                                            <a href="{{URL::route('view_cart')}}"
                                                               class="btn btn-view-cart inverse"> View Cart</a>
                                                            <a href="{{URL::route('check_out')}}"
                                                               class="btn btn-checkout pull-right">Checkout</a>
                                                        </div>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-bottom-inner">
                            <div class="header-bottom-left menu-vertical col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="responsive megamenu-style-dev">
                                    <div class="so-vertical-menu no-gutter">
                                        <nav class="navbar-default">
                                            <div class=" container-megamenu container vertical  ">
                                                <div id="menuHeading">
                                                    <div class="megamenuToogle-wrapper">
                                                        <div class="megamenuToogle-pattern">
                                                            <div class="container">
                                                                <div><span></span><span></span><span></span></div>
                                                                Categories
                                                                <i class="fa fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="navbar-header">
                                                    <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                            class="navbar-toggle">
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <i class="fa fa-bars"></i>
                                                        <span class="hidden"> Categories </span>
                                                    </button>
                                                </div>
                                                <div class="vertical-wrapper">
                                                    <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                    <div class="megamenu-pattern">
                                                        <div class="container">
                                                            <ul class="megamenu"
                                                                data-transition="slide" data-animationtime="300">
                                                                @if(session('lan') == 'kh')
                                                                    @foreach(App\MainCategories::all() as $main_category)
                                                                        <li class="item-vertical  style1 with-sub-menu hover">
                                                                            <p class='close-menu'></p>
                                                                            <a href="index8122.html?route=product/category&amp;path=34"
                                                                               class="clearfix">
                                                                                <span>
                                                                                    {{json_decode($main_category->main_cat_name)->kh}}</strong>
                                                                                </span>
                                                                                <b class='fa fa-angle-right'></b>
                                                                            </a>
                                                                            <div class="sub-menu" style="width: 5000px !important;">
                                                                                <div class="content">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="categories">
                                                                                                <div class="row">
                                                                                                    @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                        <div class="col-sm-4 static-menu">
                                                                                                            <div class="menu">
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                           class="main-menu" style="
                                                                                                                           font-size: 14px">{{json_decode($parent_category->parent_cat_name)->kh}}</a>
                                                                                                                        <ul>
                                                                                                                            @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                                <li>
                                                                                                                                    <a href="{{URL::route('get_product_by_child',$child_category->id)}}" style="color:black">{{json_decode($child_category->child_cat_name)->kh}}</a>
                                                                                                                                </li>
                                                                                                                            @endforeach 
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                @else(session('lan') == 'en')
                                                                    @foreach(App\MainCategories::all() as $main_category)
                                                                        <li class="item-vertical  style1 with-sub-menu hover">
                                                                            <p class='close-menu'></p>
                                                                            <a href="index8122.html?route=product/category&amp;path=34"
                                                                               class="clearfix">
                                                                                <span>
                                                                                {{json_decode($main_category->main_cat_name)->en}}</strong>
                                                                                </span>
                                                                                <b class='fa fa-angle-right'></b>
                                                                            </a>
                                                                            <div class="sub-menu" style="width:730px">
                                                                                <div class="content">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="categories ">
                                                                                                <div class="row">
                                                                                                @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                    <div class="col-sm-4 static-menu">
                                                                                                        <div class="menu">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                       class="main-menu" style="font-size: 14px">{{json_decode($parent_category->parent_cat_name)->en}}</a>
                                                                                                                    <ul>
                                                                                                                    @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                        <li>
                                                                                                                            <a href="{{URL::route('get_product_by_child',$child_category->id)}}" style="color: black">{{json_decode($child_category->child_cat_name)->en}}</a>
                                                                                                                        </li>
                                                                                                                    @endforeach 
                                                                                                                    @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                        <li><a href="#" class="btn btn-success btn-sm">More</a></li>
                                                                                                                    @else
                                                                                                                    @endif
                                                                                                                    </ul>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            <div class="header-bottom-right col-lg-9 col-md-9 col-sm-6 col-xs-6">
                                <div class="responsive megamenu-style-dev">
                                    <nav class="navbar-default">
                                        <div class=" container-megamenu   horizontal ">
                                            <div class="navbar-header">
                                                <button type="button" id="show-megamenu" data-toggle="collapse"
                                                        class="navbar-toggle">
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>
                                            <div class="megamenu-wrapper">
                                                <span id="remove-megamenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container">
                                                        <ul class="megamenu"
                                                            data-transition="slide" data-animationtime="500">
                                                            @if(session('lan') == 'kh')
                                                                @foreach(App\MainCategories::all()->take(4) as $main_category)
                                                                    <li class=" item-style2 with-sub-menu hover">
                                                                        <p class='close-menu'></p>
                                                                        <a href="#" class="clearfix">
                                                                            <strong>{{json_decode($main_category->main_cat_name)->kh}}
                                                                            </strong>
                                                                            <b class='caret'></b>
                                                                        </a>
                                                                        <div class="sub-menu" style="width: 100%">
                                                                            <div class="content">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="categories ">
                                                                                            <div class="row">
                                                                                            @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                <div class="col-sm-4 static-menu">
                                                                                                    <div class="menu">
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                   class="main-menu">{{json_decode($parent_category->parent_cat_name)->kh}}</a>
                                                                                                                <ul>
                                                                                                                    @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                        <li>
                                                                                                                            <a href="{{URL::route('get_product_by_child',$child_category->id)}}">{{json_decode($child_category->child_cat_name)->kh}}</a>
                                                                                                                        </li>
                                                                                                                    @endforeach 
                                                                                                                    @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                        <li><a href="#" class="btn btn-success btn-sm" style="padding-right: 30px;padding-left: 30px;color: white;border-radius: 0px;border:none">More</a></li>
                                                                                                                    @else
                                                                                                                    @endif
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @else(session('lan') == 'en')
                                                                @foreach(App\MainCategories::all()->take(4) as $main_category)
                                                                    <li class=" item-style2 with-sub-menu hover">
                                                                        <p class='close-menu'></p>
                                                                        <a href="#" class="clearfix">
                                                                            <strong>{{json_decode($main_category->main_cat_name)->en}}
                                                                            </strong>
                                                                            <b class='caret'></b>
                                                                        </a>
                                                                        <div class="sub-menu" style="width: 100%">
                                                                            <div class="content">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="categories ">
                                                                                            <div class="row">
                                                                                                @foreach(App\ParentCategories::where('main_cat_id',$main_category->id)->get() as $parent_category)
                                                                                                    <div class="col-sm-4 static-menu">
                                                                                                        <div class="menu">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a href="{{URL::route('get_product_by_parent',$parent_category->id)}}"
                                                                                                                       class="main-menu">{{json_decode($parent_category->parent_cat_name)->en}}</a>
                                                                                                                    <ul>
                                                                                                                        @foreach(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get()->take(6) as $child_category)
                                                                                                                            <li>
                                                                                                                                <a href="{{URL::route('get_product_by_child',$child_category->id)}}">{{json_decode($child_category->child_cat_name)->en}}</a>
                                                                                                                            </li>
                                                                                                                        @endforeach  
                                                                                                                        @if(count(App\ChildCategories::where('parent_cat_id',$parent_category->id)->get() )> 7)
                                                                                                                            <li><a href="#" class="btn btn-success btn-sm" style="padding-right: 30px;padding-left: 30px;color: white;border-radius: 0px;border:none">More</a></li>
                                                                                                                        @else
                                                                                                                        @endif
                                                                                                                    </ul>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content">
            <div class="so-page-builder">
                <div class="container page-builder-ltr">
                    <div class="row row_dofv  row-style ">
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 col_bj7o  custom-slider col-lg-offset-3">
                            <div class="module sohomepage-slider ">
                                <div class="modcontent">
                                    <div id="sohomepage-slider1">
                                        <div class="so-homeslider sohomeslider-inner-1">
                                            <div class="item">
                                                <a href="" title="slide 1" target="_self">
                                                    <img class="responsive"
                                                         src="web/images/slider/1.jpg"
                                                         alt="slide 1" style="width: 100%" />
                                                </a>
                                                <div class="sohomeslider-description">

                                                </div>
                                            </div>
                                            <div class="item">
                                                <a href="# " title="slide 2" target="_self">
                                                    <img class="responsive"
                                                         src="web/images/slider/2.jpg"
                                                         alt="slide 2" style="width: 100%" />
                                                </a>
                                                <div class="sohomeslider-description">

                                                </div>
                                            </div>
                                            <div class="item">
                                                <a href=" #" title="slide 3" target="_self">
                                                    <img class="responsive"
                                                         src="web/images/slider/3.jpg"
                                                         alt="slide 3" style="width: 100%" />
                                                </a>
                                                <div class="sohomeslider-description">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container page-builder-ltr" style="margin-top: 40px">
                    <div class="row row_il7b  row-style ">
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col_xoow  col-style">
                            <div class="row row_8qwb  row-style ">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_70jf col-style">
                                    <div class="banners banner1">
                                        <div>
                                            <a title="Banner" href="#"> <img src="web/image/catalog/demo/banners/banner1.jpg" alt="image"> 
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_hhvg col-style">
                                    <script>
                                        //<![CDATA[
                                        var listdeal1 = [];
                                        //]]>
                                    </script>
                                    <div class="module so-deals-ltr deals-custom1">
                                        <h3 class="modtitle">
                                            <span>New Products</span></h3>
                                        <div class="modcontent">
                                            <div id="so_deals_83259092109032017132217"
                                                 class="so-deal modcontent clearfix preset00-2 preset01-2 preset02-2 preset03-1 preset04-1  button-type1  style2">
                                                <div class="extraslider-inner products-list grid" data-effect="none">
                                                @if(session('lan') == 'kh')
                                                    @foreach($products as $product)
                                                        <div class="item">
                                                            <div class="product-thumb transition">
                                                                <div class="inner">
                                                                    <div class="item-left">
                                                                        <div class="image product-image-container ">
                                                                            <a class="img-link"
                                                                               href="{{URL::route('get_product_detail',$product->id)}}"
                                                                               target="_self">
                                                                                <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}" alt=" Sausage aliqua prosciutto deserunt" class="img-responsive2">
                                                                                <div class="box-label" style="position: absolute;top: 10px;left: 10px;"> <span style="background: #e93434;border-radius: 3px;padding-left: 10px;padding-right: 10px;padding-bottom: 5px;padding-top: 5px;color: white">New</span>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item-right">
                                                                        <div class="caption">
                                                                            <h4><a href="indexbb02.html?route=product/product&amp;product_id=42" target="_self" title=" Sausage aliqua prosciutto deserunt">{{json_decode($product->product_name)->kh}}</a>
                                                                            </h4> 
                                                                            <p class="price">
                                                                                <span class="price-new">${{$product->product_price}}</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="button-group">
                                                                            <button class="btn-button add-cart"
                                                                                    id="add-cart" title="Add to Cart"
                                                                                    type="button"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                <span class=""></span></button>
                                                                            <button class="btn-button wishlist" type="button" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                                            <a href="{{URL::route('get_product_detail',$product->id)}}" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                            <button class="btn-button compare" type="button" title="Contact Messaget" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else(session('lan') == 'en')
                                                    @foreach($products as $product)
                                                        <div class="item">
                                                            <div class="product-thumb transition">
                                                                <div class="inner">
                                                                    <div class="item-left">
                                                                        <div class="image product-image-container ">
                                                                            <a class="img-link"
                                                                               href="{{URL::route('get_product_detail',$product->id)}}"
                                                                               target="_self">
                                                                                <img src="{{asset(App\Photos::where('product_id',$product->id)->first()->image)}}"
                                                                                     alt=" Sausage aliqua prosciutto deserunt"
                                                                                     class="img-responsive2">
                                                                                <div class="box-label" style="position: absolute;top: 10px;left: 10px;">
                                                                                    <span style="background: #e93434;border-radius: 3px;padding-left: 10px;padding-right: 10px;padding-bottom: 5px;padding-top: 5px;color: white">New</span>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="item-right">
                                                                        <div class="caption">
                                                                            <h4>
                                                                                <a href="{{URL::route('get_product_detail',$product->id)}}"
                                                                                   target="_self"
                                                                                   title=" Sausage aliqua prosciutto deserunt">{{json_decode($product->product_name)->en}}</a></h4> 
                                                                            <p class="price">
                                                                                <span class="price-new">${{$product->product_price}}</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="button-group">
                                                                            {{--<button class="btn-button addcart" title="Add to Cart" type="button" onclick="addCart({{$product->id}})"><i class="fa fa-shopping-cart"></i> <span class=""></span></button>--}}
                                                                            <a href="{{URL::route('add_cart',$product->id)}}"
                                                                               class="btn-button addcart"
                                                                               title="Add to Cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                <span class=""></span></a>
                                                                            <button class="btn-button wishlist"
                                                                                    type="button"
                                                                                    title="Add to Wish List"><i
                                                                                        class="fa fa-heart"></i>
                                                                            </button>
                                                                            <a href="{{URL::route('get_product_detail',$product->id)}}" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                            <button class="btn-button compare" type="button" title="Contact Messaget" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_70jf col-style">
                                    <div class="banners banner1">
                                        <div>
                                            <a title="Banner" href="#"> <img src="web/image/catalog/demo/banners/banner1.jpg" alt="image"> 
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_vpro col-style">
                                    <!-- default Grid  -->
                                    <div class="module so-extraslider-ltr ">
                                        <h3 class="modtitle"><span>Wholesale Products</span></h3>
                                        <div class="modcontent">
                                            <div id="so_extra_slider_1062702841504444937"
                                                 class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                <!-- Begin extraslider-inner -->
                                                <div class="extraslider-inner products-list grid" data-effect="none">
                                                    <div class="item ">
                                                        <div class="product-layout style1 ">
                                                            <div class="product-item-container">
                                                                <div class="left-block">
                                                                    <div class="product-image-container">
                                                                        <a href="#"
                                                                           target="_self"
                                                                           title=" Porkchop tongue cupim capicola ">
                                                                            <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                 alt=" Porkchop tongue cupim capicola">
                                                                        </a>
                                                                    </div>
                                                                    <div class="button-group so-quickview">
                                                                        <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                    </div>
                                                                </div>
                                                                <div class="right-block">
                                                                    <div class="caption">
                                                                        <h4>
                                                                            <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                               target="_self"
                                                                               title=" Porkchop tongue cupim capicola ">
                                                                                Porkchop tongue cupim capicola
                                                                            </a>
                                                                        </h4>
                                                                        <div class="price">
                                                                            <span class="price-new">
                                                                                $337.99
                                                                            </span>
                                                                            <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="product-layout style1 ">
                                                            <div class="product-item-container">
                                                                <div class="left-block">
                                                                    <div class="product-image-container">
                                                                        <a href="#"
                                                                           target="_self"
                                                                           title="Aeserunt incididunt shank veniam  ">
                                                                            <img src="web/image/cache/catalog/demo/product/11-200x200.jpg"
                                                                                 alt="Aeserunt incididunt shank veniam ">
                                                                        </a> 
                                                                    </div>
                                                                    <div class="button-group so-quickview">
                                                                        <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('36 ');"><i  class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('36 ');"><i  class="fa fa-heart"></i></button>
                                                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        <button type="button" class="compare btn-button"  title="Contact Message "  onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>

                                                                <div class="right-block">
                                                                    <div class="caption">
                                                                        <h4>
                                                                            <a href="index5e0b.html?route=product/product&amp;product_id=36"
                                                                               target="_self"
                                                                               title="Aeserunt incididunt shank veniam  ">
                                                                                Aeserunt incididunt shank veniam
                                                                            </a>
                                                                        </h4> 
                                                                        <div class="price">
                                                                            <span class="price-new">$116.00 </span>&nbsp;&nbsp;
                                                                            <span class="hidden price-percent-reduction">Ex Tax:  $95.00 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="product-layout style1 ">
                                                            <div class="product-item-container">
                                                                <div class="left-block">
                                                                    <div class="product-image-container     ">
                                                                        <a href="#"
                                                                           target="_self"
                                                                           title="Chuck cow shoulder doloreipsum ">
                                                                            <img src="web/image/cache/catalog/demo/product/5-200x200.jpg"
                                                                                 alt="Chuck cow shoulder doloreipsum">
                                                                        </a>
                                                                    </div>
                                                                    <div class="button-group so-quickview">
                                                                        <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('41 ');"><i class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('41 ');"><i class="fa fa-heart"></i></button>
                                                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        <button type="button" class="compare btn-button" title="Contact Message " onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="right-block">
                                                                    <div class="caption">
                                                                        <h4>
                                                                            <a href="indexcae8.html?route=product/product&amp;product_id=41"
                                                                               target="_self"
                                                                               title="Chuck cow shoulder doloreipsum ">
                                                                                Chuck cow shoulder doloreipsum
                                                                            </a>
                                                                        </h4> 
                                                                        <div class="price">
                                                                            <span class="price-new">
                                                                                $122.00
                                                                            </span>
                                                                            <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="product-layout style1 ">
                                                            <div class="product-item-container">
                                                                <div class="left-block">
                                                                    <div class="product-image-container     ">
                                                                        <a href="#"
                                                                           target="_self"
                                                                           title="Cupim flank capcola sprribsde ">
                                                                            <img src="web/image/cache/catalog/demo/product/7-200x200.jpg"
                                                                                 alt="Cupim flank capcola sprribsde">
                                                                        </a>
                                                                    </div>
                                                                    <div class="box-label">
                                                                    </div>
                                                                    <div class="button-group so-quickview">
                                                                        <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('48 ');"><i  class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button type="button" class="wishlist btn-button"  title="Add to Wish List "  onclick="wishlist.add('48 ');"><i  class="fa fa-heart"></i></button>
                                                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        <button type="button" class="compare btn-button" title="Contact Message " onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="right-block">
                                                                    <div class="caption">
                                                                        <h4>
                                                                            <a href="indexb77e.html?route=product/product&amp;product_id=48"
                                                                               target="_self"
                                                                               title="Cupim flank capcola sprribsde ">
                                                                                Cupim flank capcola sprribsde
                                                                            </a>
                                                                        </h4> 
                                                                        <div class="price">
                                                                            <span class="price-new">
                                                                                $122.00
                                                                            </span>
                                                                            <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="product-layout style1 ">
                                                            <div class="product-item-container">
                                                                <div class="left-block">
                                                                    <div class="product-image-container     ">
                                                                        <a href="#"
                                                                           target="_self"
                                                                           title="Fatback turducken burgdoggen porche ">
                                                                            <img src="web/image/cache/catalog/demo/product/13-200x200.jpg"
                                                                                 alt="Fatback turducken burgdoggen porche">
                                                                        </a>
                                                                    </div>
                                                                    <div class="box-label">
                                                                    </div>
                                                                    <div class="button-group so-quickview">
                                                                        <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('32 ');"><i class="fa fa-shopping-cart"></i>
                                                                        </button>
                                                                        <button type="button" class="wishlist btn-button"
                                                                                title="Add to Wish List "
                                                                                onclick="wishlist.add('32 ');"><i
                                                                                class="fa fa-heart"></i></button>
                                                                        <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                        <button type="button" class="compare btn-button"
                                                                                title="Contact Message "
                                                                                onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                    </div>
                                                                </div>
                                                                <div class="right-block">
                                                                    <div class="caption">
                                                                        <h4>
                                                                            <a href="indexa17e.html?route=product/product&amp;product_id=32"
                                                                               target="_self"
                                                                               title="Fatback turducken burgdoggen porche ">
                                                                                Fatback turducken burgdoggen porche
                                                                            </a>
                                                                        </h4>
                                                                        
                                                                        <div class="price">
                                                                            <span class="price-new">
                                                                                $122.00
                                                                            </span>
                                                                            <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_70jf col-style">
                                    <div class="banners banner1">
                                        <div>
                                            <a title="Banner" href="#"> <img src="web/image/catalog/demo/banners/banner1.jpg" alt="image"> 
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_vpro col-style">
                                    <div class="module so-extraslider-ltr ">
                                        <h3 class="modtitle"><span>Wholesale Products</span></h3>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="modcontent">
                                                    <div  class="so-extraslider buttom-type1 preset00-4 preset01-3 preset02-3 preset03-2 preset04-1 button-type1">
                                                        <div class="extraslider-inner products-list grid" data-effect="none">
                                                            <div class="item ">
                                                                <div class="product-layout style1 ">
                                                                    <div class="product-item-container">
                                                                        <div class="left-block">
                                                                            <div class="product-image-container">
                                                                                <a href="#"
                                                                                   target="_self"
                                                                                   title=" Porkchop tongue cupim capicola ">
                                                                                    <img src="web/image/cache/catalog/demo/product/17-200x200.jpg"
                                                                                         alt=" Porkchop tongue cupim capicola">
                                                                                </a>
                                                                            </div>
                                                                            <div class="button-group so-quickview">
                                                                                <button type="button" class="btn-button" title="Add to Cart" onclick="cart.add('29 ');"><i  class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button type="button" class="wishlist btn-button" title="Add to Wish List " onclick="wishlist.add('29 ');"><i class="fa fa-heart"></i></button>
                                                                                <a href="#" class="compare btn-button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                                <button type="button" class="compare btn-button" title="Contact Message "  onclick="document.getElementById('id01').style.display='block'" ><i class="fa fa-phone" aria-hidden="true"></i></button>

                                                                            </div>
                                                                        </div>
                                                                        <div class="right-block">
                                                                            <div class="caption">
                                                                                <h4>
                                                                                    <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                                       target="_self"
                                                                                       title=" Porkchop tongue cupim capicola ">
                                                                                        Porkchop tongue cupim capicola
                                                                                    </a>
                                                                                </h4>
                                                                                <div class="price">
                                                                                    <span class="price-new">
                                                                                        $337.99
                                                                                    </span>
                                                                                    <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_igfk  col-style">
                            <div class="row row_is4j  row-style ">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_0b0n col-style">
                                    <script>
                                        //<![CDATA[
                                        var listdeal2 = [];
                                        //]]>
                                    </script>
                                    <div class="module so-deals-ltr deals-sidebar">
                                        <h3 class="modtitle2"><span>Hot Products</span></h3>
                                        <div class="modcontent">
                                            <div id="so_deals_150415776409032017132217"
                                                 class="so-deal modcontent clearfix preset00-1 preset01-1 preset02-2 preset03-1 preset04-1  button-type1  style2">
                                                <div class="products-list grid extraslider-inner" data-effect="none">
                                                    <div class="item">
                                                        <div class="product-thumb transition product-item-container">
                                                            <div class="left-block">
                                                                <div class="product-image-container ">
                                                                    <div class="image">
                                                                        <a href="indexbb02.html?route=product/product&amp;product_id=42"
                                                                           target="_self">
                                                                            <img src="web/image/cache/catalog/demo/product/1-300x300.jpg"
                                                                                 alt=" Sausage aliqua prosciutto deserunt"
                                                                                 class="img-responsive">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="button-group">
                                                                </div>
                                                            </div>
                                                            <div class="right-block">
                                                                <div class="caption">
                                                                    <h4>
                                                                        <a href="indexbb02.html?route=product/product&amp;product_id=42"
                                                                           target="_self"
                                                                           title=" Sausage aliqua prosciutto deserunt">
                                                                            Sausage aliqua prosciutto deserunt</a></h4>
                                                                    <p class="price">
                                                                        <span class="price-new">$110.00</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="product-thumb transition product-item-container">
                                                            <div class="left-block">
                                                                <div class="product-image-container ">
                                                                    <div class="image">
                                                                        <a href="index5e0b.html?route=product/product&amp;product_id=36"
                                                                           target="_self">
                                                                            <img src="web/image/cache/catalog/demo/product/11-300x300.jpg"
                                                                                 alt="Aeserunt incididunt shank veniam "
                                                                                 class="img-responsive">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="button-group">
                                                                </div>
                                                            </div>
                                                            <div class="right-block">
                                                                <div class="caption">
                                                                    <h4>
                                                                        <a href="index5e0b.html?route=product/product&amp;product_id=36"
                                                                           target="_self"
                                                                           title="Aeserunt incididunt shank veniam ">Aeserunt
                                                                            incididunt shank veniam </a></h4>
                                                                    <p class="price">
                                                                        <span class="price-new">$116.00</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="product-thumb transition product-item-container">
                                                            <div class="left-block">
                                                                <div class="product-image-container ">
                                                                    <div class="image">
                                                                        <a href="indexf073.html?route=product/product&amp;product_id=30"
                                                                           target="_self">
                                                                            <img src="web/image/cache/catalog/demo/product/2-300x300.jpg"
                                                                                 alt="Alcatra sunt quispare ribs nulla"
                                                                                 class="img-responsive">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="button-group">
                                                                </div>
                                                            </div>
                                                            <div class="right-block">
                                                                <div class="caption">
                                                                    <h4>
                                                                        <a href="indexf073.html?route=product/product&amp;product_id=30"
                                                                           target="_self"
                                                                           title="Alcatra sunt quispare ribs nulla">Alcatra
                                                                            sunt quispare ribs nulla</a></h4>
                                                                    <p class="price">
                                                                        <span class="price-new">$98.00</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="product-thumb transition product-item-container">
                                                            <div class="left-block">
                                                                <div class="product-image-container ">
                                                                    <div class="image">
                                                                        <a href="index6320.html?route=product/product&amp;product_id=28"
                                                                           target="_self">
                                                                            <img src="web/image/cache/catalog/demo/product/4-300x300.jpg"
                                                                                 alt="Biltong drumtick burgen sirloin"
                                                                                 class="img-responsive">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="button-group">
                                                                </div>
                                                            </div>
                                                            <div class="right-block">
                                                                <div class="caption">
                                                                    <h4><a href="index6320.html?route=product/product&amp;product_id=28"
                                                                           target="_self"
                                                                           title="Biltong drumtick burgen sirloin">Biltong
                                                                            drumtick burgen sirloin</a></h4>
                                                                    <p class="price">
                                                                        <span class="price-new">$104.00</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="module banner-left hidden-xs" style="margin-top: 10px">
                                    <div class="banner-sidebar banners">
                                        <div>
                                            <a title="Banner Image" href="#">
                                                <img src="web/image/catalog/demo/banners/banner-sidebar.jpg" alt="Banner Image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="module so-extraslider-ltr product-simple" style="margin-top: 10px">
                                    <h3 class="modtitle2"><span>Hot Products</span></h3>
                                    <div class="modcontent">
                                        <div id="so_extra_slider_20014185151504444938"
                                             class="so-extraslider buttom-type1 preset00-1 preset01-1 preset02-1 preset03-1 preset04-1 button-type1">
                                            <!-- Begin extraslider-inner -->
                                            <div class="extraslider-inner" data-effect="none">
                                                <div class="item ">
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="index6320.html?route=product/product&amp;product_id=28"
                                                                   target="_self"
                                                                   title="Biltong drumtick burgen sirloin ">
                                                                    <img src="web/image/cache/catalog/demo/product/4-100x100.jpg"
                                                                         alt="Biltong drumtick burgen sirloin">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="index6320.html?route=product/product&amp;product_id=28"
                                                                   target="_self"
                                                                   title="Biltong drumtick burgen sirloin ">
                                                                    Biltong drumtick burgen sirloin
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$104.00 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $85.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                   target="_self"
                                                                   title=" Porkchop tongue cupim capicola ">
                                                                    <img src="web/image/cache/catalog/demo/product/17-100x100.jpg"
                                                                         alt=" Porkchop tongue cupim capicola">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="index0f47.html?route=product/product&amp;product_id=29"
                                                                   target="_self"
                                                                   title=" Porkchop tongue cupim capicola ">
                                                                    Porkchop tongue cupim capicola
                                                                </a>
                                                            </div>


                                                            <div class="content_price price">
                                                                <span class="price product-price">
                                                                    $337.99
                                                                </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $279.99 </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexf073.html?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title="Alcatra sunt quispare ribs nulla ">
                                                                    <img src="web/image/cache/catalog/demo/product/2-100x100.jpg"
                                                                         alt="Alcatra sunt quispare ribs nulla">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexf073.html?route=product/product&amp;product_id=30"
                                                                   target="_self"
                                                                   title="Alcatra sunt quispare ribs nulla ">
                                                                    Alcatra sunt quispare ribs nulla
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$98.00 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $80.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexbfcf.html?route=product/product&amp;product_id=31"
                                                                   target="_self"
                                                                   title="Mapicola ullamco tempor jowlball ">
                                                                    <img src="web/image/cache/catalog/demo/product/16-100x100.jpg"
                                                                         alt="Mapicola ullamco tempor jowlball">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexbfcf.html?route=product/product&amp;product_id=31"
                                                                   target="_self"
                                                                   title="Mapicola ullamco tempor jowlball ">
                                                                    Mapicola ullamco tempor jowlball
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$86.00 </span>&nbsp;&nbsp;
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $70.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexa17e.html?route=product/product&amp;product_id=32"
                                                                   target="_self"
                                                                   title="Fatback turducken burgdoggen porche ">
                                                                    <img src="web/image/cache/catalog/demo/product/13-100x100.jpg"
                                                                         alt="Fatback turducken burgdoggen porche">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexa17e.html?route=product/product&amp;product_id=32"
                                                                   target="_self"
                                                                   title="Fatback turducken burgdoggen porche ">
                                                                    Fatback turducken burgdoggen porche
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price product-price">
                                                                    $122.00
                                                                </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End item-wrap -->
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="index6a4d.html?route=product/product&amp;product_id=33"
                                                                   target="_self"
                                                                   title="Sunt beefrbs andlleui pignulla ">
                                                                    <img src="web/image/cache/catalog/demo/product/20-100x100.jpg"
                                                                         alt="Sunt beefrbs andlleui pignulla">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="index6a4d.html?route=product/product&amp;product_id=33"
                                                                   target="_self"
                                                                   title="Sunt beefrbs andlleui pignulla ">
                                                                    Sunt beefrbs andlleui pignulla
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price product-price">
                                                                    $242.00
                                                                </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $200.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="item ">
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexc21e.html?route=product/product&amp;product_id=34"
                                                                   target="_self"
                                                                   title="Doner nisi estse strip steasgk ">
                                                                    <img src="web/image/cache/catalog/demo/product/12-100x100.jpg"
                                                                         alt="Doner nisi estse strip steasgk">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexc21e.html?route=product/product&amp;product_id=34"
                                                                   target="_self"
                                                                   title="Doner nisi estse strip steasgk ">
                                                                    Doner nisi estse strip steasgk
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$98.00 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $80.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End item-wrap -->
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexfac8.html?route=product/product&amp;product_id=35"
                                                                   target="_self" title="Ribeye capicola ground round ">
                                                                    <img src="web/image/cache/catalog/demo/product/18-100x100.jpg"
                                                                         alt="Ribeye capicola ground round">
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexfac8.html?route=product/product&amp;product_id=35"
                                                                   target="_self" title="Ribeye capicola ground round ">
                                                                    Ribeye capicola ground round
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price product-price">
                                                                    $122.00
                                                                </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">

                                                                <a href="index5e0b.html?route=product/product&amp;product_id=36"
                                                                   target="_self"
                                                                   title="Aeserunt incididunt shank veniam  ">

                                                                    <img src="web/image/cache/catalog/demo/product/11-100x100.jpg"
                                                                         alt="Aeserunt incididunt shank veniam ">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="index5e0b.html?route=product/product&amp;product_id=36"
                                                                   target="_self"
                                                                   title="Aeserunt incididunt shank veniam  ">
                                                                    Aeserunt incididunt shank veniam
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$116.00 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $95.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="index9144.html?route=product/product&amp;product_id=40"
                                                                   target="_self"
                                                                   title="Cow porchetta pork chop kielbasa ">

                                                                    <img src="web/image/cache/catalog/demo/product/6-100x100.jpg"
                                                                         alt="Cow porchetta pork chop kielbasa">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="index9144.html?route=product/product&amp;product_id=40"
                                                                   target="_self"
                                                                   title="Cow porchetta pork chop kielbasa ">
                                                                    Cow porchetta pork chop kielbasa
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$111.20 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $91.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexcae8.html?route=product/product&amp;product_id=41"
                                                                   target="_self"
                                                                   title="Chuck cow shoulder doloreipsum ">
                                                                    <img src="web/image/cache/catalog/demo/product/5-100x100.jpg"
                                                                         alt="Chuck cow shoulder doloreipsum">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexcae8.html?route=product/product&amp;product_id=41"
                                                                   target="_self"
                                                                   title="Chuck cow shoulder doloreipsum ">
                                                                    Chuck cow shoulder doloreipsum
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price product-price">
                                                                    $122.00
                                                                </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $100.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-inner style1 ">
                                                        <div class="item-image">
                                                            <div class="item-img-info">
                                                                <a href="indexbb02.html?route=product/product&amp;product_id=42"
                                                                   target="_self"
                                                                   title=" Sausage aliqua prosciutto deserunt ">
                                                                    <img src="web/image/cache/catalog/demo/product/1-100x100.jpg"
                                                                         alt=" Sausage aliqua prosciutto deserunt">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="item-title">
                                                                <a href="indexbb02.html?route=product/product&amp;product_id=42"
                                                                   target="_self"
                                                                   title=" Sausage aliqua prosciutto deserunt ">
                                                                    Sausage aliqua prosciutto deserunt
                                                                </a>
                                                            </div>
                                                            <div class="content_price price">
                                                                <span class="price-new product-price">$110.00 </span>
                                                                <span class="hidden price-percent-reduction">Ex Tax:  $90.00 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="module banner-left hidden-xs" style="margin-top: 10px">
                                    <div class="banner-sidebar banners">
                                        <div>
                                            <a title="Banner Image" href="#">
                                                <img src="web/image/catalog/demo/banners/banner-sidebar.jpg" alt="Banner Image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-newsletter" style="margin-top: 30px">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <h5 class="newsletter-title">Sign up to Newsletter</h5><span class="error"></span>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <form action="https://angkorstores.com/newsletter/index" name="newsletter" id="newsletter" method="post" accept-charset="utf-8">
                            <div class="input-group">
                                <input type="text" name="email" value="" id="email" class="form-control" placeholder="Enter your email address">
                                <span class="input-group-btn">
                                
                                <button name="btn_sign_up" type="submit" id="newsletter-signup" class="btn btn-secondary">Sign Up</button>
                                    
                                </span>
                            </div>
                        </form>             
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer-container typefooter-1">
            <div class="footer-main collapse description-has-toggle" id="collapse-footer">
                <div class="so-page-builder">
                    <div class="container page-builder-ltr">
                        <div class="row row_560y  row-style ">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_zk4z  col-style">

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_i76p  col-style">
                                <div class="box-contact box-footer">
                                    <div class="modcontent">
                                        <img src="web/image/catalog/demo/logo/logo2.png" alt="logo footer">
                                        <p>They key is to have every key, the key to open every door. We don’t see them, we
                                            will never see them.</p>

                                        <ul class="infos">
                                            <li><i class="fa fa-map-marker"></i>100 S Manhattan Stress, Amarillo, TX 79104,
                                                North America
                                            </li>
                                            <li><i class="fa fa-phone"></i>( +123 )4 567 890 - ( +123 )4 567 899</li>
                                            <li><i class="fa fa-envelope"></i>support@sporttheme.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_njm1  col-style">
                                <div class="box-account box-footer">
                                    <div class="module clearfix">
                                        <h3 class="modtitle">Quick Links</h3>
                                        <div class="modcontent row">
                                            <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <li><a href="indexd773.html?route=product/manufacturer">Brands</a></li>
                                                <li><a href="#">Gift Certificates</a></li>
                                                <li><a href="index3d18.html?route=affiliate/login">Affiliates</a></li>
                                                <li><a href="indexf110-2.html?route=product/special">Specials</a></li>
                                                <li><a href="#">FAQs</a></li>
                                                <li><a href="#">Custom Link</a></li>
                                            </ul>
                                            <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <li>
                                                    <a href="index8816.html?route=information/information&amp;information_id=4">About
                                                        Us</a></li>
                                                <li>
                                                    <a href="index1766.html?route=information/information&amp;information_id=6">FAQ</a>
                                                </li>
                                                <li>
                                                    <a href="index1679.html?route=information/information&amp;information_id=3">Warranty
                                                        And Services</a></li>
                                                <li>
                                                    <a href="index11cd.html?route=information/information&amp;information_id=7">Support
                                                        24/7 page</a></li>
                                                <li><a href="#">Accessories</a></li>
                                                <li><a href="#">Trending Items</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col_slxc  col-style">
                                <div class="box-community box-footer">
                                    <div class="module clearfix">
                                        <h3 class="modtitle">Join Our Community</h3>
                                        <div class="modcontent">
                                            <ul class="socials">
                                                <li class="facebook">
                                                    <a class="_blank" href="https://www.facebook.com/MagenTech"
                                                    target="_blank"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li class="twitter">
                                                    <a class="_blank" href="https://twitter.com/smartaddons"
                                                    target="_blank"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li class="rss">
                                                    <a class="_blank" href="#" target="_blank"><i
                                                        class="fa fa-rss"></i></a></li>
                                                <li class="google_plus">
                                                    <a class="_blank"href="https://plus.google.com/u/0/+Smartaddons/posts"
                                                    target="_blank"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                                <li class="pinterest">
                                                    <a class="_blank"
                                                        href="https://www.pinterest.com/smartaddons/"
                                                        target="_blank"><i class="fa fa-pinterest"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="module module2 clearfix">
                                        <h3 class="modtitle">Download Apps</h3>
                                        <ul class="apps">
                                            <li><a href="#"><img src="web/image/catalog/demo/app1.png" alt="image"></a></li>
                                            <li><a href="#"><img src="web/image/catalog/demo/app2.png" alt="image"></a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="description-toggle hidden-lg hidden-md">
                <a class="showmore" data-toggle="collapse" href="#collapse-footer" aria-expanded="false"
                   aria-controls="collapse-footer">
                    <span class="toggle-more">Show More <i class="fa fa-angle-down"></i></span>
                    <span class="toggle-less">Show Less <i class="fa fa-angle-up"></i></span>
                </a>
            </div>
            <div class="footer-bottom ">
                <div class="container">
                    <div class="row">
                        <div class="copyright col-lg-6 col-xs-12">
                            dymafashion © 2017 Demo Store. All Rights Reserved. Designed by <a
                                href="http://www.opencartworks.com/" target="_blank">DyMa</a>
                        </div>
                        <div class="col-lg-6 col-xs-12 payment-w">
                            <img src="web/image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
    </div>
    <div id="id01" class="w3-modal" style="padding-top: 30px">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom">
            <form action="">
                <header class="w3-container w3-blue" style="padding-top: 20px;padding-bottom: 10px;"> 
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-blue w3-xlarge w3-display-topright">&times;</span>
                    <h2 style="text-align: center;font-size: 16px">Contact Messages</h2>
                </header>
                <div id="London" class="w3-container city" style="padding-right: 30px;padding-left: 30px">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('first-name') ? ' has-error' : '' }}">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-controll" name="first-name" required="" placeholder="Enter first name..." style="width: 100%">
                                @if ($errors->has('first-name'))
                                    <span class="help-block" style="color: 
                                    white;font-family: sans-serif">
                                        <strong>{{ $errors->first('first-name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('last-name') ? ' has-error' : '' }}">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-controll" name="last-name" required="" placeholder="Enter last name..." style="width: 100%">
                                @if ($errors->has('last-name'))
                                    <span class="help-block" style="color: 
                                    white;font-family: sans-serif">
                                        <strong>{{ $errors->first('last-name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-controll" name="email" required="" placeholder="Enter email address..." style="width: 100%">
                                @if ($errors->has('email'))
                                    <span class="help-block" style="color: 
                                    white;font-family: sans-serif">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-controll" name="subject" required="" placeholder="Enter subject name..." style="width: 100%">
                                @if ($errors->has('subject'))
                                    <span class="help-block" style="color: 
                                    white;font-family: sans-serif">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label for="message">Messages</label>
                        <textarea name="message" class="form-controll" style="min-height: 200px;width: 100%" placeholder="Enter messages..."></textarea>
                        @if ($errors->has('message'))
                            <span class="help-block" style="color: 
                            white;font-family: sans-serif">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="w3-container w3-light-grey w3-padding">
                    <button class="w3-btn w3-round-xxlarge" style="background: red;color: white;border:1px solid white">Send Message</button>
                    <button class="w3-btn w3-round-xxlarge"  onclick="document.getElementById('id01').style.display='none'" style="background: orange;color: white;border:1px solid white">Close</button>
                </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="{{asset('web/catalog/view/javascript/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('web/catalog/view/javascript/soconfig/js/libs.js')}}"></script>
    <script src="{{asset('web/catalog/view/javascript/soconfig/js/owl.carousel.js')}}"></script>
    <script src="{{asset('web/catalog/view/javascript/soconfig/js/so.system.js')}}"></script>
    <script src="{{asset('web/catalog/view/theme/so-oneshop/js/so.custom.js')}}"></script>
    <script src="{{asset('web/catalog/view/theme/so-oneshop/js/common.js')}}"></script>
    <script src="{{asset('web/catalog/view/javascript/so_home_slider/js/owl.carousel.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/assets/js/shortcodes.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/view/javascript/so_page_builder/js/section.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/view/javascript/so_page_builder/js/modernizr.video.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/view/javascript/so_page_builder/js/swfobject.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/view/javascript/so_page_builder/js/video_background.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/catalog/view/javascript/so_megamenu/so_megamenu.js')}}" type="text/javascript"></script>
    <script src="{{asset('web/js/product.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var itemver =  7;
            if(itemver <= $( ".vertical ul.megamenu >li" ).length)
                $('.vertical ul.megamenu').append('<li class="loadmore"><i class="fa fa-plus-square-o"></i><span class="more-view"> More Categories</span></li>');
            $('.horizontal ul.megamenu li.loadmore').remove();

            var show_itemver = itemver-1 ;
            $('ul.megamenu > li.item-vertical').each(function(i){
                if(i>show_itemver){
                        $(this).css('display', 'none');
                }
            });
            $(".megamenu .loadmore").click(function(){
                if($(this).hasClass('open')){
                    $('ul.megamenu li.item-vertical').each(function(i){
                        if(i>show_itemver){
                            $(this).slideUp(200);
                            $(this).css('display', 'none');
                        }
                    });
                    $(this).removeClass('open');
                    $('.loadmore').html('<i class="fa fa-plus-square-o"></i><span class="more-view">More Categories</span>');
                }else{
                    $('ul.megamenu li.item-vertical').each(function(i){
                        if(i>show_itemver){
                            $(this).slideDown(200);
                        }
                    });
                    $(this).addClass('open');
                    $('.loadmore').html('<i class="fa fa-minus-square-o"></i><span class="more-view">Close Categories</span>');
                }
            });
            $(".add-cart").click(function () {
                alert('Hello');
                var id = $(this).data('id') - 0;
                var name = $(this).data('name');
                var price = $(this).data('price') - 0;
                var images = $(this).data('images');

                ///alert(images);

                $.ajax({
                    url: '#',
                    type: 'GET',
                    dataType: 'json',
                    async: false,
                    data: {
                        id: id,
                        name: name,
                        price: price,
                        images: images
                    },
                    success: function (data) {
                        //alert(data);
                        updateCat(data);
                    },
                    error: function () {
                        alert('error');
                    }
                });
            });
        });

        function addCart(id) {
            alert('Hello' + id);
            var name = $(this).data('name');
            var price = $(this).data('price') - 0;
            var images = $(this).data('images');

        }

        function updateCat(data) {
            var cart = '';
            var total_qty = 0;
            var shop_checkout_price = 0;
            $.each(data, function (i, it) {
                total_qty += it.qty - 0;
                shop_checkout_price += (it.price * it.qty);
                {{--var img='{{URL::to(it.image)}}';--}}
                    cart += '<div class="item clearfix">' +
                    '<a href="home_style/#">' +
                    '<img src="' + it.options.image + '" alt="">' +
                    '</a>' +
                    '<div class="item_desc">' +
                    '<a href="http://ppcoffeecorner.com/items_detail/' + it.id + '">' + it.name + '</a>' +
                    '<span class="item_price">' + it.qty + ' x $' + it.price + '</span>' +
                    '<div class="pp"><span class="item_quantity"><a href="#" data-rowid="' + it.rowid + '" class="btn btn-primary btn-xs del">X</a></span></div>' +
                    '</div>' +
                    '</div>';
            });
            $('.cart').html(cart);
            $('.total_qty').html(total_qty);
            $('.shop_checkout_price').html(shop_checkout_price);
        }
        function _SoQuickView() {
            var windowWidth = window.innerWidth || document.documentElement.clientWidth;
            if (windowWidth > 1200) {
                var $item_class = $('.so-quickview');
                if ($item_class.length > 0) {
                    for (var i = 0; i < $item_class.length; i++) {
                        if ($($item_class[i]).find('.quickview_handler').length <= 0) {
                            var $product_id = $($item_class[i]).find('a', $(this)).attr('data-product');
                            if ($.isNumeric($product_id)) {
                                var _quickviewbutton = "<a class='btn-quickview quickview quickview_handler' href='http://opencart.opencartworks.com/themes/so_oneshop/index.php?route=extension/soconfig/quickview&amp;product_id=" + $product_id + "' title=\"Quick view\" data-title =\"Quick view\" data-fancybox-type=\"iframe\" ><i class=\"fa fa-eye\"></i></a>";
                                $($item_class[i]).append(_quickviewbutton);
                            }
                        }
                    }
                }
            }
        }
        jQuery(document).ready(function ($) {
            _SoQuickView();
            // Hide tooltip when clicking on it
            var hasTooltip = $("[data-toggle='tooltip']").tooltip({container: 'body'});
            hasTooltip.on('click', function () {
                $(this).tooltip('hide')
            });
            $('#upCart').on('change', function () {
                alert('I am here');
            });
        });
    </script>
</body>